/**
 * @type {import('gatsby').GatsbyConfig}
 */

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    title: `Мой парус`,
    description: `Сайт парусного клуба "Мой парус"`,
  },

  plugins: [
    "gatsby-plugin-apollo",
    // gatsby-config.js

    // {
    //   resolve: `gatsby-source-wordpress`,
    //   options: {
    //     url: process.env.GATSBY_WP_URL,
    //     logLevel: "info",
    //   },
    // },
    // !!!!!!!!!!!!!!!!!!!!!! НЕ УДАЛЯТЬ
    // {
    //   resolve: "@pasdo501/gatsby-source-woocommerce",
    //   options: {
    //     // Base URL of WordPress site
    //     api: process.env.GATSBY_WC_URL,
    //     // true if using https. false otherwise.
    //     https: Boolean(Number.parseInt(process.env.GATSBY_WC_HTTPS)),
    //     api_keys: {
    //       consumer_key: process.env.GATSBY_CONSUMER_KEY,
    //       consumer_secret: process.env.GATSBY_CONSUMER_SECRET,
    //     },
    //     // Array of strings with fields you'd like to create nodes for...
    //     fields: ["products", "products/categories", "products/attributes"],
    //   },
    // },
    // {
    //   resolve: "gatsby-source-wordpress",
    //   options: {
    //     url: process.env.GATSBY_WP_URL,
    //   },
    // },
    "gatsby-plugin-svgr",
    "gatsby-plugin-image",

    "gatsby-transformer-sharp",
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        icon: `src/media/icons/favicon.svg`,
      },
    },
    {
      resolve: "gatsby-plugin-sharp",
      options: {
        defaults: {
          quality: 100, // Значение качества изображения от 0 до 100
        },
      },
    },
  ],
};
