// const client = new ApolloClient({
//   link: new HttpLink({
//     uri: "https://my-sail.ru/wordpress_admin/graphql",
//     fetch,
//   }),
//   cache: new InMemoryCache(),
// });

// exports.createPages = async ({ graphQL, actions }) => {
//   const shopCategories = ["clothes", "accessories"];
//   const result = await client.query({
//     query: gql`
//       query ProductsAll {
//         products {
//           found
//           nodes {
//             databaseId
//             image {
//               sourceUrl
//             }
//             name
//             id
//             type
//             ... on SimpleProduct {
//               enclosure
//               price(format: RAW)
//               regularPrice(format: RAW)
//               salePrice
//               id
//               productId
//               onSale
//             }
//             ... on VariableProduct {
//               price(format: RAW)
//               onSale
//               regularPrice(format: RAW)
//               productId
//             }
//           }
//         }
//       }
//     }
//   `);

//   data.allWpPost.edges.node.forEach((element) => {
//     const { sourceUrl } = element.fieldGroupName.photosgallery.sourceUrl;
//     actions.createPage({
//       component: path.resolve("./srс/templated/galleryPost.jsx"),
//       context: { sourceUrl },
//     });
//   });
// };
