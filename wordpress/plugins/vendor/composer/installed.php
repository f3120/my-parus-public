<?php return array(
    'root' => array(
        'name' => 'wp-graphql/wp-graphql-woocommerce',
        'pretty_version' => 'v0.18.2',
        'version' => '0.18.2.0',
        'reference' => '63a27276817edf2d38ea0752ded07a0816f22acc',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'firebase/php-jwt' => array(
            'pretty_version' => 'v6.9.0',
            'version' => '6.9.0.0',
            'reference' => 'f03270e63eaccf3019ef0f32849c497385774e11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../firebase/php-jwt',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'wp-graphql/wp-graphql-woocommerce' => array(
            'pretty_version' => 'v0.18.2',
            'version' => '0.18.2.0',
            'reference' => '63a27276817edf2d38ea0752ded07a0816f22acc',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
