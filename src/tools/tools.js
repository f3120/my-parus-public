import { id } from "../../wordpress/plugins/updraftplus/includes/handlebars/handlebars";

export const convertSrcSet = (srcSet) => {
  const id = crypto.randomUUID();
  if (!srcSet) {
    return null;
  }
  return srcSet
    .split(",")
    .map((url) => ({
      sourceUrl: url.trim().split(" ")[0],
      width: Number(url.trim().split(" ")[1].slice(0, -1)),
      id: id,
    }))
    .sort((a, b) => a.width - b.width);
};
