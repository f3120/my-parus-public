import axios from "axios";
export const dadataQuery = async (query, successCallback, errorCallback) => {
  const token = "3b10b4e7fb549892c86bd896f04930039360d488";
  try {
    const response = await axios.post(
      "http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
      { query: query },
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Token " + token,
        },
      }
    );
    successCallback(response?.data);
  } catch (e) {
    console.error(e);
    errorCallback && errorCallback(e.response.data);
  }
};
