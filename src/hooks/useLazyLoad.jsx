import React, { useEffect, useState } from "react";

export const useLazyLoad = (preview, image) => {
  const [currentImage, setCurrentImage] = useState(preview);
  const [loading, setLoading] = useState(true);

  const fetchImage = (src) => {
    const loadingImage = new Image();
    loadingImage.src = src;
    loadingImage.onload = () => {
      setCurrentImage(loadingImage.src);
      setLoading(false);
    };
  };

  useEffect(() => {
    setCurrentImage(preview);
    image && fetchImage(image);
  }, [preview, image]);

  return [currentImage, loading];
};
