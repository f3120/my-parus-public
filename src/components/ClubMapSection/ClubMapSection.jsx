import { Box, Container, Stack, Typography } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React, { useState } from "react";
import car from "../../media/icons/car.svg";
import ModalMap from "./ModalMap/ModalMap";
import human from "../../media/icons/human.svg";
import { YMaps, Map, Placemark } from "@pbe/react-yandex-maps";
import { citiesData } from "../../config/data/citiesData";
export default function ClubMapSection({ clubId }) {
  const [selectedService, setSelectedService] = useState(undefined);

  const dataWay = citiesData.find((el) => el.id === clubId);

  return (
    <Box>
      <Container>
        <Grid2
          container
          backgroundColor={"secondary.main"}
          borderRadius={"30px"}
          overflow={"hidden"}
        >
          <Grid2 xs={12} lg={5}>
            <Stack padding={"20px"}>
              <Typography variant="h4">Как добраться</Typography>
              <Stack marginTop={"15px"} direction="row" flexWrap="wrap">
                <Stack
                  borderRadius={"100px"}
                  alignItems={"center"}
                  gap={{ xs: "5px", md: "8px" }}
                  sx={{ backgroundColor: "primary.white" }}
                  direction={"row"}
                  p={{ xs: "2px 10px 2px 2px", md: "4px 20px 4px 4px" }}
                >
                  <Box
                    display={"flex"}
                    alignItems={"center"}
                    justifyContent={"center"}
                    width={{ xs: "24px", md: "40px" }}
                    height={{ xs: "24px", md: "40px" }}
                    borderRadius={"100px"}
                    backgroundColor="secondary.main"
                  >
                    <img width="70%" src={car} alt="clubEvents" />
                  </Box>
                  <Typography color={"primary.main"}>На машине</Typography>
                </Stack>
              </Stack>
              <Typography
                marginTop={"5px"}
                color={"primary.main"}
                variant="body1"
              >
                {dataWay.car}
              </Typography>
              {dataWay.go ? (
                <>
                  <Stack marginTop={"15px"} direction="row" flexWrap="wrap">
                    <Stack
                      borderRadius={"100px"}
                      alignItems={"center"}
                      gap={{ xs: "5px", md: "8px" }}
                      sx={{ backgroundColor: "primary.white" }}
                      direction={"row"}
                      p={{ xs: "2px 10px 2px 2px", md: "4px 20px 4px 4px" }}
                    >
                      <Box
                        display={"flex"}
                        alignItems={"center"}
                        justifyContent={"center"}
                        width={{ xs: "24px", md: "40px" }}
                        height={{ xs: "24px", md: "40px" }}
                        borderRadius={"100px"}
                        backgroundColor="secondary.main"
                      >
                        <img width="70%" src={human} alt="clubEvents" />
                      </Box>
                      <Typography color={"primary.main"}>
                        Своим ходом
                      </Typography>
                    </Stack>
                  </Stack>
                  <Typography marginTop={"5px"} color={"primary.main"}>
                    {dataWay.go}
                  </Typography>
                </>
              ) : (
                <Box mt={"50px"} />
              )}

              <Box
                onClick={() => setSelectedService("someValue")}
                marginTop={"15px"}
                borderRadius={"30px"}
                sx={{ cursor: "pointer", backgroundColor: "primary.white" }}
                p={{ xs: "8px 47px", md: "4px 20px 4px 4px" }}
              >
                <Typography textAlign={"center"} color={"primary.main"}>
                  Подробный маршрут
                </Typography>
              </Box>
            </Stack>
          </Grid2>
          <Grid2 xs={12} lg={7}>
            <Box width={"100%"} height={"100%"}>
              <YMaps>
                <Map
                  defaultState={{ center: [0, 0], zoom: 1 }}
                  width={"100%"}
                  height={"100%"}
                  state={{
                    center: dataWay.coordinate,
                    zoom: 15,
                    behaviors: ["drag"],
                  }}
                >
                  {<Placemark geometry={dataWay.coordinate} />}
                </Map>
              </YMaps>
            </Box>
          </Grid2>
        </Grid2>
      </Container>
      <ModalMap
        dataWay={dataWay}
        selectedService={selectedService}
        setSelectedService={setSelectedService}
      />
    </Box>
  );
}
