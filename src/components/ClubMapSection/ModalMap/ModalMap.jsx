import { Box, Dialog, Stack, Typography } from "@mui/material";
import React from "react";
import human from "../../../media/icons/human.svg";
import car from "../../../media/icons/car.svg";
import train from "../../../media/icons/train.svg";
import aircraft from "../../../media/icons/aircraft.svg";
import telefon from "../../../media/icons/telefon.svg";
import colocol from "../../../media/icons/colocol.svg";
import { Link } from "gatsby";
export default function ModalMap({
  selectedService,
  setSelectedService,
  dataWay,
}) {
  return (
    <Dialog
      open={typeof selectedService !== "undefined"}
      onClose={() => setSelectedService(undefined)}
      PaperProps={{
        sx: {
          borderRadius: "15px",
          width: { xs: "100%", sm: "540px", md: "800px" },
          m: 0,
        },
      }}
    >
      <Stack>
        <Stack padding={"5px"}>
          <Stack
            borderRadius={"18px"}
            direction={"column"}
            justifyContent={"center"}
            alignItems={"center"}
            padding={"10px 16.5px"}
            backgroundColor={"secondary.main"}
          >
            <Typography variant="h4">Как добраться</Typography>
            <Typography
              textAlign={"center"}
              variant="h6"
              color={"primary.main"}
            >
              {dataWay.adress}
            </Typography>
          </Stack>
        </Stack>
        <Stack
          padding={{ xs: "10px 10px 15px 10px", md: "20px 30px 30px 30px" }}
        >
          {dataWay.carModal && (
            <>
              <Stack direction="row">
                <Stack
                  borderRadius={"100px"}
                  alignItems={"center"}
                  gap={{ xs: "5px", md: "8px" }}
                  sx={{ backgroundColor: "secondary.main" }}
                  direction={"row"}
                  p={{ xs: "2px 10px 2px 2px", md: "4px 20px 4px 4px" }}
                >
                  <Box
                    display={"flex"}
                    justifyContent={"center"}
                    alignItems={"center"}
                    width={{ xs: "24px", md: "40px" }}
                    height={{ xs: "24px", md: "40px" }}
                    borderRadius={"100px"}
                    backgroundColor="primary.white"
                  >
                    <img width="70%" src={car} alt="clubEvents" />
                  </Box>
                  <Typography color={"primary.main"}>На машине</Typography>
                </Stack>
              </Stack>
              <Typography
                marginTop={{ xs: "5px", md: "10px" }}
                color={"primary.main"}
              >
                {dataWay.carModal}
              </Typography>
            </>
          )}
          {dataWay.goModal && (
            <>
              <Stack marginTop={{ xs: "15px", sm: "20px" }} direction="row">
                <Stack
                  borderRadius={"100px"}
                  alignItems={"center"}
                  gap={{ xs: "5px", md: "8px" }}
                  sx={{ backgroundColor: "secondary.main" }}
                  direction={"row"}
                  p={{ xs: "2px 10px 2px 2px", md: "4px 20px 4px 4px" }}
                >
                  <Box
                    display={"flex"}
                    justifyContent={"center"}
                    alignItems={"center"}
                    width={{ xs: "24px", md: "40px" }}
                    height={{ xs: "24px", md: "40px" }}
                    borderRadius={"100px"}
                    backgroundColor="primary.white"
                  >
                    <img width="70%" src={human} alt="clubEvents" />
                  </Box>
                  <Typography color={"primary.main"}>Своим ходом</Typography>
                </Stack>
              </Stack>
              <Typography
                marginTop={{ xs: "5px", md: "10px" }}
                color={"primary.main"}
              >
                {dataWay.goModal}
              </Typography>
            </>
          )}

          {dataWay.train && (
            <>
              <Stack marginTop={{ xs: "15px", sm: "20px" }} direction="row">
                <Stack
                  borderRadius={"100px"}
                  alignItems={"center"}
                  gap={{ xs: "5px", md: "8px" }}
                  sx={{ backgroundColor: "secondary.main" }}
                  direction={"row"}
                  p={{ xs: "2px 10px 2px 2px", md: "4px 20px 4px 4px" }}
                >
                  <Box
                    display={"flex"}
                    justifyContent={"center"}
                    alignItems={"center"}
                    width={{ xs: "24px", md: "40px" }}
                    height={{ xs: "24px", md: "40px" }}
                    borderRadius={"100px"}
                    backgroundColor="primary.white"
                  >
                    <img width="70%" src={train} alt="clubEvents" />
                  </Box>
                  <Typography color={"primary.main"}>На поезде</Typography>
                </Stack>
              </Stack>
              <Typography
                marginTop={{ xs: "5px", md: "10px" }}
                color={"primary.main"}
              >
                {dataWay.train}
              </Typography>
            </>
          )}
          {dataWay.aircraft && (
            <>
              <Stack marginTop={{ xs: "15px", sm: "20px" }} direction="row">
                <Stack
                  borderRadius={"100px"}
                  alignItems={"center"}
                  gap={{ xs: "5px", md: "8px" }}
                  sx={{ backgroundColor: "secondary.main" }}
                  direction={"row"}
                  p={{ xs: "2px 10px 2px 2px", md: "4px 20px 4px 4px" }}
                >
                  <Box
                    display={"flex"}
                    justifyContent={"center"}
                    alignItems={"center"}
                    width={{ xs: "24px", md: "40px" }}
                    height={{ xs: "24px", md: "40px" }}
                    borderRadius={"100px"}
                    backgroundColor="primary.white"
                  >
                    <img width="70%" src={aircraft} alt="clubEvents" />
                  </Box>
                  <Typography color={"primary.main"}>На самолете</Typography>
                </Stack>
              </Stack>
              <Typography
                marginTop={{ xs: "5px", md: "10px" }}
                color={"primary.main"}
              >
                {dataWay.aircraft}
              </Typography>
            </>
          )}

          {/* <Stack
            flexWrap={"wrap"}
            marginTop={{ xs: "5px", sm: "8px", md: "12px" }}
            direction={"row"}
            gap={"5px"}
          > */}
            <Stack
              gap={"5px"}
              flexWrap={"wrap"}
              direction={"row"}
              alignItems={"center"}
            >
            {dataWay?.apropos && (
              <>
                <Stack marginTop={{ xs: "15px", sm: "20px" }} direction="row">
                <Stack
                  borderRadius={"100px"}
                  alignItems={"center"}
                  gap={{ xs: "5px", md: "8px" }}
                  sx={{ backgroundColor: "secondary.main" }}
                  direction={"row"}
                  p={{ xs: "2px 10px 2px 2px", md: "4px 20px 4px 4px" }}
                >
                  <Box
                    display={"flex"}
                    justifyContent={"center"}
                    alignItems={"center"}
                    width={{ xs: "24px", md: "40px" }}
                    height={{ xs: "24px", md: "40px" }}
                    borderRadius={"100px"}
                    backgroundColor="primary.white"
                  >
                    <Typography color={"primary.main"}>🚪</Typography>
                  </Box>
                  <Typography color={"primary.main"}>Для прохода</Typography>
                </Stack>
              </Stack>
            
              <Typography color={"primary.main"}>{dataWay?.apropos}</Typography>
              </>)}
            </Stack>
          </Stack>
        </Stack>
      {/* </Stack> */}
    </Dialog>
  );
}
