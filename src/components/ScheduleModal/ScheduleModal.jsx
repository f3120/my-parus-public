import React, { useContext } from "react";
import { ModalContext } from "../../context/ModalContext";
import {
  Box,
  Button,
  Dialog,
  FormControl,
  MenuItem,
  Select,
  Stack,
  Typography,
} from "@mui/material";
import scheduleIcon from "../../media/icons/Calendar.svg";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { navigate } from "gatsby";
import { citiesData } from "../../config/data/citiesData";

const modalID = "scheduleModal";
export default function ScheduleModal() {
  const { isModalActive, setModalInactive } = useContext(ModalContext);
  let clubRegion =
    typeof window !== "undefined" && localStorage.getItem("clubRegion");
  return (
    <Dialog
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "15px",
        },
      }}
    >
      <Stack
        alignItems={"center"}
        direction={{ xs: "column" }}
        padding={{ xs: "15px", md: "20px", lg: "30px" }}
        justifyContent={"space-between"}
      >
        <Box
          sx={{
            width: "40px",
            height: "40px",
            padding: "10px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            bgcolor: "secondary.main",
            borderRadius: "10px",
          }}
        >
          <img width={"100%"} src={scheduleIcon} alt="schedule" />
        </Box>
        <Typography
          variant="h5"
          textAlign={"center"}
          color={"primary.main"}
          mt={"10px"}
        >
          Хотите перейти
          <br />в расписание клуба
          <br />
          {clubRegion
            ? citiesData.find((el) => el.id === clubRegion)?.nameWhere
            : "в Москве"}
          ?
        </Typography>
        <Button
          variant="contained"
          sx={{ mt: "10px" }}
          onClick={() => {
            navigate(
              clubRegion
                ? "/schedule" +
                    citiesData.find((el) => el.id === clubRegion)?.url
                : "/schedule/moscow"
            );
            setModalInactive(modalID);
          }}
        >
          <Typography variant="body1">Да, перейти</Typography>
        </Button>
        <Box
          width={{ xs: 145, md: "auto" }}
          sx={{
            mt: "10px",
          }}
        >
          <FormControl fullWidth variant={"standard"}>
            <Select
              disableUnderline={true}
              defaultValue={-1}
              MenuProps={{ PaperProps: { sx: { borderRadius: "15px" } } }}
              IconComponent={KeyboardArrowDownIcon}
              onChange={(e) => {
                e.target.value === "/sochi"
                  ? window.open("https://sputnikresort.ru/", "_blank")
                  : navigate(`/schedule${e.target.value}`);
                setModalInactive(modalID);
              }}
            >
              <MenuItem value={-1} disabled>
                <Typography
                  variant="body1"
                  color={"primary.main"}
                  textAlign={"left"}
                >
                  Выберите регион
                </Typography>
              </MenuItem>
              {citiesData.map((city) => (
                <MenuItem value={city.url}>
                  <Typography
                    variant="body1"
                    color={"primary.main"}
                    textAlign={"center"}
                  >
                    {city.name}
                  </Typography>
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
      </Stack>
    </Dialog>
  );
}
