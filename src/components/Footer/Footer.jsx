import { Box, Container, Link, Stack, Typography } from "@mui/material";
import React, { useContext, useEffect } from "react";
import vkicon, { ReactComponent as VkIcon } from "../../media/icons/VKIcon.svg";
import tgIcon, { ReactComponent as TgIcon } from "../../media/icons/TGIcon.svg";
import waIcon, { ReactComponent as WaIcon } from "../../media/icons/WAIcon.svg";
import ytIcon, { ReactComponent as YtIcon } from "../../media/icons/YTIcon.svg";
import wave, { ReactComponent as Wave } from "../../media/icons/Wave.svg";
import { navigate } from "gatsby";
import moiparusIcon, {
  ReactComponent as MoiparusIcon,
} from "../../media/icons/MoiParus.svg";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import { ModalContext } from "../../context/ModalContext";
import { gql, useQuery } from "@apollo/client";
import UserAgreementModal from "./UserAgreementModal/UserAgreementModal";
import PrivacyPolicyModal from "./PrivacyPolicyModal/PrivacyPolicyModal";

const APOLLO_QUERY = gql`
  query LegalDocs {
    posts(where: { categoryName: "legal_documents" }) {
      edges {
        node {
          id
          categories {
            nodes {
              slug
            }
          }
          content
          title
        }
      }
    }
  }
`;
const APOLLO_QUERY2 = gql`
  query PriceDocs {
    posts(where: { categoryName: "price_file" }) {
      edges {
        node {
          id
          clubfield {
            clubname
          }
          filefields {
            file {
              mediaItemUrl
            }
            namefile
          }
        }
      }
    }
  }
`;

export default function Footer({
  clubRegionState = "moscow",
  setClubRegionState,
}) {
  const location = typeof window !== "undefined" && window.location.pathname;

  const [priceData, setPriceData] = React.useState({});

  const { data, loading, error } = useQuery(APOLLO_QUERY);

  const {
    data: data2,
    loading: loading2,
    error: error2,
  } = useQuery(APOLLO_QUERY2);

  useEffect(() => {
    setPriceData(
      data2?.posts?.edges &&
        data2?.posts?.edges.find(
          (edge) => edge?.node?.clubfield?.clubname === clubRegionState
        )
    );
  }, [data2, loading2, clubRegionState]);

  const privacyPolicyData =
    data?.posts?.edges &&
    data?.posts?.edges.find((edge) =>
      edge?.node?.categories?.nodes.some(
        (node) => node?.slug === "privacy_policy"
      )
    );

  const userAgreementData =
    data?.posts?.edges &&
    data?.posts?.edges.find((edge) =>
      edge?.node?.categories?.nodes.some((node) => node?.slug === "offer")
    );

  const { setModalActive } = useContext(ModalContext);
  return (
    <Box
      bgcolor={"primary.main"}
      sx={{
        width: "100%",
        borderRadius: "20px 20px 0 0",
        position: "relative",
        paddingBottom: { xs: "40px", sm: "40px", md: "60px" },
      }}
    >
      <Container
        sx={{
          paddingTop: { xs: "15px", sm: "20px", md: "34px" },
        }}
      >
        <Grid2 container spacing={{ xs: 2, sm: 0 }}>
          <Grid2 xs={12} sm={6}>
            <Stack direction={"column"}>
              <Typography
                display={{ xs: "block", sm: "block", md: "none" }}
                variant="h5"
              >
                Контакты
              </Typography>
              <Typography
                display={{ xs: "none", sm: "none", md: "block" }}
                direction={"column"}
                variant="h5"
              >
                Телефон
              </Typography>
              <Typography
                mt={"5px"}
                variant="subtitle1"
                component={Link}
                href="tel:88005503006"
              >
                8 800 550 3006
              </Typography>
              <Typography
                marginTop={{ md: "43px" }}
                variant="h5"
                display={{ xs: "none", sm: "none", md: "block" }}
                direction={"column"}
              >
                Почтовый адрес
              </Typography>
              <Typography
                id="contacts"
                mt={"5px"}
                variant="subtitle1"
                component={Link}
                href="mailto:info@my-sail.ru"
              >
                info@my-sail.ru
              </Typography>
            </Stack>
          </Grid2>

          <Grid2 xs={12} sm={6}>
            <Stack
              direction={"column"}
              justifyContent={"space-between"}
              height={"100%"}
            >
              <Stack
                direction={"column"}
                rowGap={{ xs: "10px", md: "10px" }}
                marginTop={{ xs: "10px", md: "0px" }}
                textAlign={{ sm: "right" }}
              >
                <Typography
                  component={Link}
                  href={priceData?.node?.filefields?.file?.mediaItemUrl}
                  target="_blank"
                  variant="body1"
                  display={
                    location &&
                    location
                      .split("/")
                      .some((item) => item === "moscow" || item === "mria")
                      ? "block"
                      : "none"
                  }
                >
                  {priceData?.node?.filefields?.namefile}
                </Typography>
              </Stack>
              <Stack
                direction={"column"}
                alignItems={{ xs: "center", sm: "flex-end" }}
              >
                <Typography
                  variant="body1"
                  display={{ xs: "none", sm: "none", md: "block" }}
                  direction={"column"}
                  textAlign={"end"}
                >
                  Наши соцсети
                </Typography>
                <Stack
                  direction={"row"}
                  sx={{ gap: "15px" }}
                  marginTop={{ xs: "20px", sm: "10px" }}
                  justifyContent={{ xs: "center", md: "right" }}
                >
                  <a
                    href="https://vk.com/my_sail"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <VkIcon />
                  </a>
                  <a
                    href="http://t.me/mysail_club"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <TgIcon />
                  </a>
                  <a
                    href="https://wa.me/79384727938"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <WaIcon />
                  </a>
                  <a
                    href="https://youtube.com/channel/UC90zn1qc-MjU6FsjFK98TRA"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <YtIcon />
                  </a>
                </Stack>
              </Stack>
            </Stack>
          </Grid2>
        </Grid2>

        <Box
          borderBottom={5}
          // TODO: заменить на белый + прозрачность 50%
          borderColor="#FFFFFF"
          sx={{ opacity: 0.5 }}
          borderRadius={"20px"}
          marginTop={{ xs: "20px", sm: "30px" }}
        />

        <Grid2 container rowGap={"20px"}>
          <Grid2
            xs={12}
            sm={12}
            md={12}
            lg={4}
            xl={4}
            order={{ xs: 0, sm: 0, md: 0, lg: 1 }}
          >
            <Stack
              onClick={() => navigate("/")}
              marginTop={{ xs: "20px", sm: "40px", md: "40px" }}
              sx={{ alignItems: "center", cursor: "pointer" }}
            >
              <MoiparusIcon />
            </Stack>
          </Grid2>

          <Grid2
            xs={12}
            sm={6}
            md={6}
            lg={4}
            xl={4}
            order={{ xs: 1, sm: 1, md: 1, lg: 0 }}
          >
            <Stack
              alignItems={{ xs: "center", sm: "flex-start", md: "flex-start" }}
              direction={{ xs: "column", md: "column" }}
            >
              <Typography
                onClick={() => setModalActive("privacyPolicy")}
                component={Link}
                marginTop={{ xs: "50px", sm: "20px", md: "9px", lg: "20px" }}
                variant="body1"
              >
                {privacyPolicyData?.node?.title}
              </Typography>
              <Typography
                onClick={() => setModalActive("userAgreement")}
                component={Link}
                marginTop={"5px"}
                variant="body1"
                textAlign={"center"}
              >
                {userAgreementData?.node?.title}
              </Typography>
            </Stack>
          </Grid2>
          <Grid2
            xs={12}
            sm={6}
            md={6}
            lg={4}
            xl={4}
            order={{ xs: 2, sm: 2, md: 2, lg: 2 }}
          >
            <Stack
              alignItems={{ xs: "center", sm: "flex-end", md: "flex-end" }}
              direction={{ xs: "column", md: "column" }}
            >
              <Typography
                sx={{ mt: "20px" }}
                marginTop={{ xs: "20px", sm: "20px", md: "4px", lg: "20px" }}
                variant="body1"
                textAlign={"center"}
              >
                Все права защищены
              </Typography>
              <Typography
                variant="body1"
                textAlign={{ xs: "center", sm: "end" }}
                marginTop={"5px"}
              >
                2020-2024 © ООО "Парусный Клуб"
              </Typography>
            </Stack>
          </Grid2>
        </Grid2>
      </Container>
      <Box
        sx={{
          position: "absolute",
          bottom: 0,
          left: 0,
          width: "100%",
          height: "24px",
          backgroundImage: `url(${wave})`,
          backgroundRepeat: "repeat-x",
        }}
      />
      <UserAgreementModal data={userAgreementData?.node} />
      <PrivacyPolicyModal data={privacyPolicyData?.node} />
    </Box>
  );
}
