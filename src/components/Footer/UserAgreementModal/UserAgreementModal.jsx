import React, { useContext } from "react";
import { ModalContext } from "../../../context/ModalContext";
import { Dialog, Stack, Typography } from "@mui/material";
import parse from "html-react-parser";

const modalID = "userAgreement";
export default function UserAgreementModal({ data = { content: "" } }) {
  const { isModalActive, setModalInactive } = useContext(ModalContext);
  return (
    <Dialog
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      maxWidth={"lg"}
      PaperProps={{
        sx: {
          borderRadius: "15px",
        },
      }}
    >
      <Stack
        direction={"column"}
        padding={"40px"}
        gap={"20px"}
        alignItems={"flex-start"}
      >
        <Typography
          variant="h5"
          color={"primary.main"}
          width={"100%"}
          textAlign={"center"}
        >
          {data?.title}
        </Typography>
        <Typography variant="body1" color={"primary.main"} width={"100%"}>
          {parse(data?.content)}
        </Typography>
      </Stack>
    </Dialog>
  );
}
