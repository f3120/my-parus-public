import React from "react";
import { useSiteMetadata } from "../../hooks/useSiteMetadata";
import { gql, useQuery } from "@apollo/client";

const APOLLO_QUERY = gql`
  query Keywords {
    posts(where: { categoryName: "keywords" }) {
      edges {
        node {
          id
          keywords {
            words
          }
        }
      }
    }
  }
`;

export const SEO = ({ title, description, children }) => {
  const { loading, error, data } = useQuery(APOLLO_QUERY);
  const keywords =
    data && data?.posts.edges[0].node.keywords.words.replaceAll(";", ",");

  const { title: defaultTitle, description: defaultDescription } =
    useSiteMetadata();

  const seo = {
    title: title || defaultTitle,
    description: description || defaultDescription,
  };

  return (
    <>
      <title>{seo.title}</title>
      <meta name="keywords" content={keywords} />
      <meta name="description" content={seo.description} />
      {children}
    </>
  );
};
