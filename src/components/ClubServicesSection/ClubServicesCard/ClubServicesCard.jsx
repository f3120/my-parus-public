import { Box, Button, Stack, Typography } from "@mui/material";

import React from "react";
import jackdaw, {
  ReactComponent as Jackdaw,
} from "../../../media/icons/Jackdaw.svg";
import { useContext } from "react";
import { ModalContext } from "../../../context/ModalContext";

export default function ClubServicesCard({ item, sx }) {
  console.log({ item });
  const { setModalActive } = useContext(ModalContext);

  // let strock = item.content.split(";");
  // console.log({ strock });

  return (
    <Stack
      justifyContent="space-between"
      padding={{ xs: "10px", lg: "20px" }}
      sx={{
        bgcolor: "primary.white",
        height: "100%",
        borderRadius: {
          xs: "15px",
          sm: "20px ",
          lg: "30px ",
        },
        ...sx,
      }}
    >
      <Stack gap={{ xs: "10px" }}>
        <Typography variant="h5" color={"primary.main"}>
          {item.title}
        </Typography>

        {Array.isArray(item.content) &&
          item.content.map((contentItem, index) => (
            <Stack direction="row">
              <Box
                marginRight={"10px"}
                sx={{
                  minWidth: "24px", // Добавлено
                  minHeight: "24px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  width: { xs: "14px", sm: "24px" },
                  height: { xs: "14px", sm: "24px" },
                  borderRadius: "50%",
                  border: "1px solid",
                  borderColor: "primary.main",
                }}
              >
                <Jackdaw width={"70%"} height={"70%"} stroke="#324F7B" />
              </Box>
              <Typography color={"primary.main"} key={index}>
                {contentItem}
              </Typography>
            </Stack>
          ))}
      </Stack>
      <Stack>
        <Stack direction={"column"} alignItems={"center"}>
          <Box width={"fit-content"}>
            <Typography
              width={"100%"}
              textAlign={"center"}
              variant="h4"
              marginTop={{ xs: "32px", md: "35px", lg: "30px", xl: "50px" }}
              color={"primary.main"}
            >
              {`${item.price}₽`}
            </Typography>
            <Typography
              width={"100%"}
              textAlign={"right"}
              color={"primary.main"}
            >
              {item?.countService || "Кол-во занятий неопределено"}
            </Typography>
          </Box>
        </Stack>
        <Button
          bgColor={"secondary.main"}
          sx={{
            padding: "0px !important",
            borderRadius: "30px",
            backgroundColor: "secondary.main",
            height: { xs: "35px !important", md: "50px !important" },
            marginTop: { xs: "15px" },
          }}
          onClick={() => setModalActive("scheduleModal")}
        >
          <Typography color={"primary.main"}>Записаться</Typography>
        </Button>
      </Stack>
    </Stack>
  );
}
