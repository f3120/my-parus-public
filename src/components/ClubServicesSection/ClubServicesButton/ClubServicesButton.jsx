import { Box, Button, Stack, Typography } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React, { useState } from "react";
import { ReactComponent as UpDown } from "../../../media/icons/upDown.svg";
export default function ClubServiceButton({
  icon,
  color,
  text,
  onClick,
  sx,
  isActive,
}) {
  const handleClick = () => {
    onClick();
  };

  const imageStyle = {
    filter: isActive ? "brightness(0) invert(1)" : "none",
  };

  return (
    <Stack
      bgcolor={isActive ? color : "primary.contrastText"}
      borderRadius={"40px"}
      direction={{ xs: "row" }}
      padding={"4px"}
      sx={{
        cursor: "pointer",
        transition: "all 0.4s ease",
        alignItems: "center",
        ...sx,
      }}
      onClick={handleClick}
    >
      <Box
        width={{ xs: "50px", md: "58px", lg: "70px" }}
        height={{ xs: "50px", md: "58px", lg: "70px" }}
        sx={{
          bgcolor: "#FFFFFF",
          borderRadius: "100px",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <img src={icon} width={"100%"} alt="location" />
      </Box>
      <Box
        // marginTop={{
        //   xs: "14px",
        //   md: "18px",
        //   lg: "21px",
        // }}
        marginLeft={{
          xs: "35px",
          sm: "17px",
          md: "32px",
          lg: "54px",
          xl: "90px",
        }}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
        width={{
          xs: "90px",
          sm: "90px",
          md: "106px",
          lg: "132px",
          xl: "142px",
        }}
        height={{
          xs: "20px",
          sm: "20px",
          md: "24px",
          lg: "32px",
          xl: "32px",
        }}
      >
        <img
          src={text}
          width={"100%"}
          height={"100%"}
          alt="location"
          style={{ ...imageStyle }}
        />
      </Box>
    </Stack>
  );
}
