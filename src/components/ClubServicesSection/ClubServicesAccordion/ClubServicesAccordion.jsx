import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Stack,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import ClubServicesButton from "../ClubServicesButton/ClubServicesButton";
import ClubServicesCard from "../ClubServicesCard/ClubServicesCard";
import { ReactComponent as UpDown } from "../../../media/icons/upDown.svg";
import { service } from "../../../../wordpress/plugins/woocommerce/assets/js/sourcebuster/sourcebuster";

export default function ClubServicesAccordion({
  clubServicesDataButton,
  services,
}) {
  const [selectedServiceId, setSelectedServiceId] = useState(
    clubServicesDataButton[0]?.id || null
  );

  useEffect(() => {
    setSelectedServiceId(clubServicesDataButton?.[0]?.id || null);
  }, [clubServicesDataButton]);

  const handleButtonClick = (serviceId) => {
    setSelectedServiceId((prevId) => (prevId === serviceId ? null : serviceId));
  };

  const getServiceCards = (serviceId) => {
    console.log({
      serviceId,
      servicesId: services.map((service) => service.id),
      cards: services.find((service) => service.id === serviceId)?.cards,
    });
    return services.find((service) => service.id === serviceId)?.cards || [];
  };
  return (
    <Stack width={"100%"} gap={"20px"}>
      {clubServicesDataButton &&
        clubServicesDataButton.map((serviceBlock) => (
          <Accordion
            key={serviceBlock.id}
            width={"100%"}
            expanded={selectedServiceId === serviceBlock.id}
            sx={{
              backgroundColor: "transparent !important",
              borderRadius: "40px !important ",
              "&:before": {
                display: "none",
              },

              "& .MuiAccordionSummary-root": {
                borderRadius: "40px !important",
              },
              "& .MuiAccordionSummary-content": {
                margin: 0,
                "&.Mui-expanded": {
                  margin: 0,
                },
              },
              "& .MuiCollapse-root": {
                overflow: "hidden",
                width: {
                  xs: "100%",
                },
              },
            }}
            elevation={0}
          >
            <AccordionSummary>
              <ClubServicesButton
                sx={{ width: "100%", height: "100%" }}
                icon={serviceBlock?.icon}
                color={serviceBlock?.color}
                text={serviceBlock?.text}
                onClick={() => handleButtonClick(serviceBlock.id)}
                isActive={selectedServiceId === serviceBlock.id}
                // icon={<Box sx={{transform:{rotete:}}}><UpDown/></Box>}
              />
            </AccordionSummary>
            <AccordionDetails>
              {Array.isArray(getServiceCards(serviceBlock.id)) &&
              getServiceCards(serviceBlock.id).length > 0 ? (
                <Stack direction={"column"} gap={"10px"}>
                  {getServiceCards(serviceBlock.id).map((serviceCard) => (
                    <ClubServicesCard key={serviceCard.id} item={serviceCard} />
                  ))}
                </Stack>
              ) : (
                <Typography
                  variant="h6"
                  color={"primary.main"}
                  sx={{ mt: "20px" }}
                  textAlign={"center"}
                >
                  Предложения скоро
                  <br />
                  появятся
                </Typography>
              )}
            </AccordionDetails>
          </Accordion>
        ))}
    </Stack>
  );
}
