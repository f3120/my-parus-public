import React, { useState } from "react";

import textOptiSAIL, {
  ReactComponent as TextOptiSAIL,
} from "../../media/icons/TextOptiSAIL.svg";
import textOptiRACE, {
  ReactComponent as TextOptiRACE,
} from "../../media/icons/TextOptiRACE.svg";
import textOptiFUN, {
  ReactComponent as TextOptiFUN,
} from "../../media/icons/TextOptiFUN.svg";
import textLaserSAIL, {
  ReactComponent as TextLaserSAIL,
} from "../../media/icons/TextLaserSAIL.svg";
import textLaserRACE, {
  ReactComponent as TextLaserRACE,
} from "../../media/icons/TextLaserRACE.svg";
import textLaserFUN, {
  ReactComponent as TextLaserFUN,
} from "../../media/icons/TextLaserFUN.svg";

import optiFUN from "../../media/icons/OptiFUN.svg";
import optiSAIL from "../../media/icons/OptiSAIL.svg";
import optiRACE from "../../media/icons/OptiRACE.svg";
import laserFUN from "../../media/icons/LaserFUN.svg";
import laserSAIL from "../../media/icons/LaserSAIL.svg";
import laserRACE from "../../media/icons/LaserRACE.svg";

import {
  Box,
  Container,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";

import { mainTheme } from "../../config/MUI/mainTheme";

import ClubServicesAccordion from "./ClubServicesAccordion/ClubServicesAccordion";
import ClubServicesStack from "./ClubServicesStack/ClubServicesStack";
import { graphql, useStaticQuery } from "gatsby";

import { gql, useQuery } from "@apollo/client";
import ClubServicesSelector from "./ClubServicesSelector/ClubServicesSelector";

const OptiServicesDataButton = [
  {
    id: "optiFUN",
    icon: optiFUN,
    text: textOptiFUN,
    color: "secondary.orange",
  },
  {
    id: "optiSAIL",
    icon: optiSAIL,
    text: textOptiSAIL,
    color: "primary.main",
  },
  {
    id: "optiRACE",
    icon: optiRACE,
    text: textOptiRACE,
    color: "secondary.red",
  },
];

const LazerServicesDataButton = [
  {
    id: "laserFUN",
    icon: laserFUN,
    text: textLaserFUN,
    color: "secondary.orange",
  },
  {
    id: "laserSAIL",
    icon: laserSAIL,
    text: textLaserSAIL,
    color: "primary.main",
  },
  {
    id: "laserRACE",
    icon: laserRACE,
    text: textLaserRACE,
    color: "secondary.red",
  },
];

const APOLLO_QUERY = gql`
  query QueryClubServices($categoryName: String!) {
    posts(where: { categoryName: $categoryName }, first: 1000) {
      edges {
        node {
          id
          clubfield {
            clubname
          }
          servicefield {
            servicedescription
            servicecountservice
            servicename
            serviceprice
            servicetype
          }
        }
      }
    }
  }
`;

export default function ClubServicesSection({ clubId }) {
  const { loading, error, data } = useQuery(APOLLO_QUERY, {
    variables: { categoryName: "service" },
  });

  const services = !loading
    ? data.posts.edges
        .filter((node) => node?.node?.clubfield?.clubname === clubId)
        //reduce берет массив data.allWpPost.edges и уменьшает его до одного значения
        //prev - начинается как пустой массив [] и будет накапливать услуги
        //item - текущий элемент, обрабатываемый в массиве edges
        .reduce((prev, item) => {
          //строка проверят существует ли уже услуга с тем же servicetype в prev
          const existingService = prev.find(
            (items) => items.id === item?.node?.servicefield?.servicetype
          );
          //если existingService найден, добавляется новый объект карточки в массив cards существующей услуги.
          if (existingService) {
            existingService.cards.push({
              title: item?.node?.servicefield?.servicename,
              countService: item?.node?.servicefield?.servicecountservice,
              price: item?.node?.servicefield?.serviceprice,
              content: item?.node?.servicefield?.servicedescription.split(";"),
              id: item?.node?.servicefield?.servicetype,
            });
            //если existingService не найден, создается новый объект услуги с новым servicetype и добавляется объект карточки в его массив cards.
          } else {
            prev.push({
              id: item?.node?.servicefield?.servicetype,
              cards: [
                {
                  title: item?.node?.servicefield?.servicename,
                  price: item?.node?.servicefield?.serviceprice,
                  countService: item?.node?.servicefield?.servicecountservice,
                  content:
                    item?.node?.servicefield?.servicedescription.split(";"),
                  id: item?.node?.servicefield?.servicetype,
                },
              ],
            });
          }
          return prev;
        }, [])
    : [];

  console.log({ services });
  const upSm = useMediaQuery(mainTheme.breakpoints.up("sm"));

  const [selectedType, setSelectedType] = React.useState("Optimist");

  return (
    <Stack
      direction={"column"}
      justifyContent={"center"}
      alignItems={"center"}
      paddingTop={"30px"}
      sx={{
        width: "100%",
      }}
    >
      <Typography variant="h3">Услуги клуба</Typography>
      <ClubServicesSelector
        selectedType={selectedType}
        setSelectedType={setSelectedType}
      />
      <Box
        sx={{
          width: "100%",
          padding: "30px",
          borderRadius: { xs: "20px", md: "30px" },
        }}
        bgcolor={{ xs: "secondary.main" }}
      >
        <Container>
          {upSm ? (
            <ClubServicesStack
              clubServicesDataButton={
                selectedType === "Optimist"
                  ? OptiServicesDataButton
                  : LazerServicesDataButton
              }
              services={services}
            />
          ) : (
            <ClubServicesAccordion
              clubServicesDataButton={
                selectedType === "Optimist"
                  ? OptiServicesDataButton
                  : LazerServicesDataButton
              }
              services={services}
            />
          )}
        </Container>
      </Box>
    </Stack>
  );
}
