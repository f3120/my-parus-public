import { Box, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import ClubServicesCard from "../ClubServicesCard/ClubServicesCard";
import ClubServicesButton from "../ClubServicesButton/ClubServicesButton";

import Grid2 from "@mui/material/Unstable_Grid2/Grid2";

export default function ClubServicesStack({
  clubServicesDataButton,
  services,
}) {
  const [selectedServiceId, setSelectedServiceId] = useState(
    clubServicesDataButton?.[0]?.id || null
  );

  useEffect(() => {
    setSelectedServiceId(clubServicesDataButton?.[0]?.id || null);
  }, [clubServicesDataButton]);
  const handleButtonClick = (serviceId) => {
    setSelectedServiceId(serviceId);
  };

  const selectedService = services.find(
    (service) => service.id === selectedServiceId
  );
  return (
    <Stack gap={"20px"} direction={{ xs: "column" }} width={"100%"}>
      <Grid2 container spacing={{ sm: "10px", md: "20px" }}>
        {clubServicesDataButton &&
          clubServicesDataButton.map((serviceBlock) => (
            <Grid2 sm>
              <ClubServicesButton
                icon={serviceBlock?.icon}
                color={serviceBlock?.color}
                text={serviceBlock?.text}
                onClick={() => handleButtonClick(serviceBlock.id)}
                isActive={selectedServiceId === serviceBlock.id}
              />
            </Grid2>
          ))}
      </Grid2>
      <Box>
        {selectedService && selectedService.cards.length > 0 ? (
          <Grid2 container spacing={{ sm: "10px", md: "20px" }} sx={{}}>
            {selectedService.cards.map((serviceCard) => (
              <Grid2 xs={6} md={4}>
                <ClubServicesCard item={serviceCard} />
              </Grid2>
            ))}
          </Grid2>
        ) : (
          <Typography
            variant="h6"
            color={"primary.main"}
            sx={{ mt: "20px" }}
            textAlign={"center"}
          >
            Предложения скоро
            <br />
            появятся
          </Typography>
        )}
      </Box>
    </Stack>
  );
}
