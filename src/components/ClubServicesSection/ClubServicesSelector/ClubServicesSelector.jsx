import { Container, Stack, Typography, useMediaQuery } from "@mui/material";
import React from "react";
import ChipWithIcon from "../../ChipWithIcon/ChipWithIcon";
import OptimistIcon from "../../../media/icons/OptimistIcon.png";
import LaserIcon from "../../../media/icons/LaserIcon.png";
import { mainTheme } from "../../../config/MUI/mainTheme";
import lineyka from "../../../media/icons/lineyka.svg";
import vesi from "../../../media/icons/vesi.svg";

export default function ClubServicesSelector({
  selectedType,
  setSelectedType,
}) {
  const upSm = useMediaQuery(mainTheme.breakpoints.up("sm"));

  return (
    <Container>
      <Stack
        direction={"row"}
        gap={{ xs: "20px", md: "30px" }}
        justifyContent={"center"}
        alignItems={"flex-end"}
        mt={"5px"}
      >
        <SelectorItem
          selected={selectedType === "Optimist"}
          setSelectedType={setSelectedType}
          icon={OptimistIcon}
          name={"Optimist"}
          descr={"для детей"}
          height={upSm ? "рост < 160 см" : "< 160 см"}
          weight={upSm ? "вес < 55 кг" : "< 55 кг"}
        />
        <SelectorItem
          selected={selectedType === "Laser"}
          setSelectedType={setSelectedType}
          icon={LaserIcon}
          name={"Laser"}
          descr={upSm ? "для подростоков и взрослых" : "для взрослых"}
          height={upSm ? "рост > 160 см" : "> 160 см"}
          weight={upSm ? "вес > 55 кг" : "> 55 кг"}
        />
      </Stack>
    </Container>
  );
}

const SelectorItem = ({
  icon,
  descr,
  name,
  height,
  weight,
  selected,
  setSelectedType,
}) => {
  return (
    <Stack
      sx={{
        flex: 1,
        borderTopLeftRadius: { xs: "20px", md: "30px" },
        borderTopRightRadius: { xs: "20px", md: "30px" },
        borderBottomLeftRadius: selected ? 0 : { xs: "20px", md: "30px" },
        borderBottomRightRadius: selected ? 0 : { xs: "20px", md: "30px" },
        backgroundColor: "secondary.main",
        padding: "20px",
        transition:
          "border-bottom-left-radius 0.3s ease, border-bottom-right-radius 0.3s ease, filter 0.3s ease",
        height: selected ? "220px" : "210px",
        marginBottom: selected ? 0 : "10px",
        ":hover": {
          filter: !selected && "brightness(0.95)",
        },
        cursor: selected ? "auto" : "pointer",
      }}
      alignItems={"center"}
      justifyContent={"center"}
      direction={"column"}
      onClick={() => setSelectedType(name)}
    >
      <img width={"40px"} src={icon} alt="serviceIcon" />
      <Typography variant="h5" color={"primary.main"} mt={"5px"}>
        {name}
      </Typography>
      <Typography
        variant="body1"
        color={"primary.main"}
        mt={"5px"}
        textAlign={"center"}
      >
        {descr}
      </Typography>
      <Stack
        direction={{ xs: "column", sm: "row" }}
        gap={"5px"}
        mt={"10px"}
        justifyContent={"center"}
        alignItems={"center"}
        width={"100%"}
      >
        <ChipWithIcon
          invertColor
          text={height}
          icon={lineyka}
          iconStyle={{ width: "70%", height: "70%" }}
        />
        <ChipWithIcon
          invertColor
          text={weight}
          icon={vesi}
          iconStyle={{ width: "70%", height: "70%" }}
        />
      </Stack>
    </Stack>
  );
};
