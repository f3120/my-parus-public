import { Box, Typography } from "@mui/material";
import React from "react";
import parse from "html-react-parser";
export default function NewsContent({ content }) {
  if (typeof content !== "string") {
    return <Typography>Invalid content format</Typography>;
  }
  return (
    <Typography marginTop={"30px"} variant="body1" color={"primary.main"}>
      {parse(content)}
    </Typography>
  );
}
