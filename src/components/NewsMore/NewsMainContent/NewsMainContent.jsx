import { Block } from "@mui/icons-material";
import { Box, Stack, Typography } from "@mui/material";
import React from "react";

export default function NewsMainContent({ background, title, date, author }) {
  console.log({ date });
  return (
    <Stack
      direction={{ xs: "column", sm: "row" }}
      gap={"30px"}
      marginTop={{ xs: "0px", sm: "80px" }}
      alignItems={"center"}
    >
      <Stack
        sx={{
          marginTop: "15px",
          minWidth: { xs: "320px", lg: "400px" },
          minHeight: { xs: "200px", lg: "250px" },
          maxWidth: { xs: "320px", lg: "400px" },
          maxHeight: { xs: "200px", lg: "250px" },
          backgroundImage: `url(${background})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          borderRadius: "20px",
          position: { xs: "relative", sm: "static" },
          padding: "10px 20px 10px 20px",
          justifyContent: "space-between",
        }}
      >
        <Stack
          justifyContent="flex-end"
          direction="row"
          flexWrap="wrap"
          display={{ sx: "flex", sm: "none" }}
        >
          <Stack
            backgroundColor={"primary.main"}
            p={"8px 12px 8px 12px"}
            borderRadius={"30px"}
          >
            <Typography color={"primary.white"}>{date}</Typography>
          </Stack>
        </Stack>
        <Stack
          display={{ xs: "flex", sm: "none" }}
          justifyContent="flex-start"
          direction="row"
          flexWrap="wrap"
        >
          <Stack
            backgroundColor={"primary.main"}
            p={"8px 12px 8px 12px"}
            borderRadius={"30px"}
          >
            <Typography color={"primary.white"}>Автор: {author}</Typography>
          </Stack>
        </Stack>
      </Stack>
      <Typography
        textAlign={"center"}
        display={{ xs: "block", sm: "none" }}
        variant="h4"
        color={"primary.main"}
      >
        {title}
      </Typography>

      <Stack
        direction={"column"}
        justifyContent={"space-between"}
        display={{ xs: "none", sm: "flex" }}
      >
        <Stack direction="row" flexWrap="wrap">
          <Stack
            backgroundColor={"primary.main"}
            p={"8px 12px 8px 12px"}
            borderRadius={"30px"}
          >
            <Typography color={"primary.white"}>{date}</Typography>
          </Stack>
        </Stack>
        <Typography
          marginBottom={{ xs: "100px", lg: "150px" }}
          variant="h4"
          color={"primary.main"}
        >
          {title}
        </Typography>
        <Stack direction="row" flexWrap="wrap">
          <Stack
            backgroundColor={"primary.main"}
            p={"8px 12px 8px 12px"}
            borderRadius={"30px"}
          >
            <Typography color={"primary.white"}>Автор: {author}</Typography>
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
}
