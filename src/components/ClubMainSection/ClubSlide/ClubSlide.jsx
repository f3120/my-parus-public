import {
  Box,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import React, { useContext } from "react";
import { ModalContext } from "../../../context/ModalContext";
import vkicon from "../../../media/icons/VKIcon.svg";
import tgIcon from "../../../media/icons/TGIcon.svg";
import waIcon from "../../../media/icons/WAIcon.svg";
import ytIcon from "../../../media/icons/YTIcon.svg";
import mobila from "../../../media/icons/Mobila.svg";
import { useLazyLoad } from "../../../hooks/useLazyLoad";

export const clubSlideStyle = {
  width: "100%",
  height: "100vh",
  borderBottomLeftRadius: "35px",
  borderBottomRightRadius: "35px",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  position: "relative",

  overflow: "hidden",
};

export default function ClubSlide({ slideData, index, clubId }) {
  const { id, title, description, setting, srcSet, linkbanner, loadingSlide } =
    slideData || {};

  const { setModalActive } = useContext(ModalContext);

  console.log("srcSet", srcSet);

  const [currentImage, loadingImageStatus] = useLazyLoad(
    Array.isArray(srcSet) && srcSet[0].sourceUrl,
    Array.isArray(srcSet) && srcSet[srcSet.length - 1].sourceUrl
  );

  if (loadingSlide) {
    return (
      <Box
        sx={{
          ...clubSlideStyle,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <CircularProgress size={70} />
      </Box>
    );
  }

  return (
    <Box
      sx={{
        ...clubSlideStyle,
      }}
    >
      <img
        src={currentImage}
        alt="Background"
        style={{
          objectFit: "cover",
          filter: loadingImageStatus ? "blur(10px)" : "none",
          width: "100%",
          height: "100%",
          position: "absolute",
        }}
      />
      <Box
        sx={{
          position: "absolute",
          width: "100%",
          height: "100%",
          background:
            "linear-gradient(180deg, rgba(6, 11, 17, 0) 0%, rgba(6, 11, 17, 0.125) 50%, rgba(6, 11, 17, 0.7) 100%)",
        }}
      ></Box>
      {/* <StaticImage
        src="../media/images/MainSliderMoscow_1.png"
        alt="Background"
        formats={["auto", "webp", "avif"]}
        placeholder="blurred"
        transformOptions={{ fit: "cover" }}
        style={{
          width: "100%",
          height: "100%",
          position: "absolute",
        }}
      /> */}
      <Container sx={{ height: "100%", position: "relative", zIndex: 1 }}>
        <Stack
          height={"100%"}
          position={"relative"}
          sx={{ paddingBottom: "70px" }}
          direction={"column"}
          justifyContent={index === 0 ? "space-between" : "flex-end"}
        >
          <Stack alignItems={"end"} display={index === 0 ? "flex" : "none"}>
            <Box
              display={{ xs: "none", sm: "block" }}
              sx={{
                backdropFilter: "blur(10px)",
                background: "rgba(255, 255, 255, 0.1)",
              }}
              borderRadius={"100px"}
              padding={"10px"}
              width={{ xl: "240px" }}
              height={{ xl: "60px" }}
              marginTop={"136px"}
            >
              <Stack direction={"row"} gap={"20px"}>
                <Box>
                  <a
                    href="https://vk.com/my_sail"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img width="100%" src={vkicon} alt="clubEvents" />
                  </a>
                </Box>
                <Box>
                  <a
                    href="http://t.me/mysail_club"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img width="100%" src={tgIcon} alt="clubEvents" />
                  </a>
                </Box>
                <Box>
                  <a
                    href="https://wa.me/79384727938"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img width="100%" src={waIcon} alt="clubEvents" />
                  </a>
                </Box>
                <Box>
                  <a
                    href="https://youtube.com/channel/UC90zn1qc-MjU6FsjFK98TRA"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img width="100%" src={ytIcon} alt="clubEvents" />
                  </a>
                </Box>
              </Stack>
            </Box>
            <Box
              display={{ xs: "none", md: "block" }}
              marginTop={"15px"}
              sx={{
                backdropFilter: "blur(10px)",
                background: "rgba(255, 255, 255, 0.1)",
              }}
              borderRadius={"100px"}
              padding={"10px"}
              width={{ xl: "280px" }}
              height={{ xl: "60px" }}
            >
              <Stack direction={"row"} alignItems={"center"} gap={"10px"}>
                <Box>
                  <img width="100%" src={mobila} alt="clubEvents" />
                </Box>
                <Typography variant="h5">8 800 550 3006</Typography>
              </Stack>
            </Box>
          </Stack>
          <Stack width={"100%"}>
            <Stack width={{ xs: "90%", sm: "70%" }}>
              <Typography
                marginBottom={{ xs: "5px", md: "10px" }}
                color={"primary.white"}
                variant="h3"
              >
                {title}
              </Typography>
              <Typography color={"primary.white"} variant="body1">
                {description}
              </Typography>

              <Box
                onClick={() =>
                  clubId === "sochi"
                    ? window.open("https://sputnikresort.ru/", "_blank")
                    : setModalActive("scheduleModal")
                }
                marginTop={{ xs: "15px", md: "25px" }}
                display={index === 0 ? "flex" : "none"}
                justifyContent={"center"}
                sx={{
                  cursor: "pointer",
                  alignItems: "center",
                  borderRadius: "30px",
                  border: "1.80px solid #FFFFFF",
                }}
                width={{ xs: "115px", md: "180px" }}
                height={{ xs: "35px", md: "50px" }}
              >
                <Typography>Записаться</Typography>
              </Box>
            </Stack>
          </Stack>
        </Stack>
      </Container>
    </Box>
  );
}
