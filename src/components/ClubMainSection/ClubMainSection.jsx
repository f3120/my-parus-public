import { Box, Container, Stack, Typography } from "@mui/material";
import React, { useEffect, useRef } from "react";
import Slider from "react-slick";
import ClubSlide from "./ClubSlide/ClubSlide";
import { gql, useQuery } from "@apollo/client";
import { clubSlideStyle } from "./ClubSlide/ClubSlide";
import { convertSrcSet } from "../../tools/tools";

const APOLLO_QUERY = gql`
  query ManyEvents {
    posts(where: { categoryName: "bannerclub" }, first: 1000) {
      edges {
        node {
          id
          clubfield {
            clubname
          }
          bannerfield {
            description
            setting
            title
            linkbanner
            photo {
              sourceUrl
              srcSet
            }
          }
          categories {
            nodes {
              slug
            }
          }
        }
      }
    }
  }
`;

export default function ClubMainSection({ clubId }) {
  const { data, loading, error } = useQuery(APOLLO_QUERY);

  const [slides, setSlides] = React.useState([
    { id: "loading", loadingSlide: true },
  ]);

  const dotsStyle = {
    width: "100%",
    height: "4px",
    borderRadius: "20px",
    transition: "all 0.2s ease",
  };

  const dotsWrapperStyle = {
    width: 65 / slides.length + "%",
    maxWidth: "15%",
    p: "5px 0",
    cursor: "pointer",
  };

  useEffect(() => {
    if (!loading && !error && Array.isArray(data?.posts?.edges)) {
      setSlides(
        data?.posts?.edges
          .filter(
            (node) =>
              node.node.clubfield.clubname === clubId &&
              node.node.categories.nodes.some(
                (node) => node.slug === "bannerclub"
              )
          )
          .map((node) => ({
            id: node.node.id,
            title: node.node.bannerfield.title,
            description: node.node.bannerfield.description,
            setting: node.node.bannerfield.setting,
            srcSet: convertSrcSet(node.node.bannerfield.photo.srcSet),
            linkbanner: node.node.bannerfield.linkbanner,
          }))
      );
    }
  }, [data, clubId, loading, error]);

  const [currentSlide, setCurrentSlide] = React.useState(0);

  let sliderRef = useRef(null);

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    className: "main-slider",
    afterChange: (curSlide) => {
      setCurrentSlide(curSlide);
    },
  };

  return (
    <Box sx={{ position: "relative", top: "-68px", marginBottom: "-68px" }}>
      <Slider
        {...settings}
        ref={(slider) => {
          sliderRef = slider;
        }}
      >
        {slides.length > 0 &&
          slides.map((item, index) => (
            <ClubSlide
              slideData={item}
              index={index}
              key={item?.id}
              clubId={clubId}
            />
          ))}
      </Slider>
      <Box
        position={"absolute"}
        sx={{
          bottom: { xs: "5px", md: "10px" },
          width: "100%",
          // display: { xs: "flex", md: "none" },
          justifyContent: "center",
          zIndex: 1,
        }}
      >
        <Stack
          direction={"row"}
          justifyContent={"center"}
          gap={"7px"}
          width={"100%"}
        >
          {slides.length > 0 &&
            slides.map((item, index) => (
              <Box
                onClick={() => {
                  sliderRef.slickGoTo(index);
                }}
                sx={{
                  ...dotsWrapperStyle,
                }}
              >
                <Box
                  sx={{
                    ...dotsStyle,
                    backgroundColor:
                      currentSlide === index ? "primary.main" : "primary.white",
                  }}
                ></Box>
              </Box>
            ))}
        </Stack>
      </Box>
    </Box>
  );
}
