import { Stack, Typography } from "@mui/material";
import React from "react";

export default function GalleryTitle() {
  return (
    <Stack
      marginTop={"20px"}
      alignItems={"center"}
      justifyContent={"center"}
      width={"100% "}
      backgroundColor={"secondary.main"}
      borderRadius={"40px"}
      padding={{ xs: "15px", lg: "30px" }}
      p={{
        xs: "10px 20px 10px 20px",
        md: "20px 0px 20px 0px",
        lg: "30px 81px 30px 40px",
        xl: "30px 370px 30px 40px",
      }}
      direction={{ lg: "row" }}
      gap={{ lg: "120px", xl: "160px" }}
    >
      <Typography variant="h3">Галерея</Typography>
      <Typography
        color={"primary.main"}
        display={{ xs: "none", lg: "block" }}
        variant="h6"
      >
        Здесь собраны фотографии с наших событий. Найди на них себя и своих друзей!
      </Typography>
    </Stack>
  );
}
