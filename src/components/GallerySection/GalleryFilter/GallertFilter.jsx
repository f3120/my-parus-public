import {
  Box,
  Button,
  FormControl,
  InputAdornment,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import { DateRangePicker } from "react-date-range";
import { addDays } from "date-fns";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { DateRange } from "react-date-range";
import { ru } from "date-fns/locale";

import { format } from "../../../../wordpress/plugins/woocommerce/assets/js/accounting/accounting";
import { citiesData } from "../../../config/data/citiesData";

function useOutsideClick(ref, callback) {
  const listenHandler = (e) => {
    if (ref.current && !ref.current.contains(e.target)) {
      callback();
    }
  };
  useEffect(() => {
    document.addEventListener("mousedown", listenHandler);
    return () => {
      document.removeEventListener("mousedown", listenHandler);
    };
  });
}

export default function GallertFilter({
  setSearchText,
  setClubRegionState,
  setStartDate,
  setEndDate,
}) {
  const [isCalendarOpen, setCalendarOpen] = useState(false);
  const [state, setState] = useState([
    {
      startDate: undefined,
      endDate: undefined,
      key: "selection",
    },
  ]);

  const daysOfWeek = ["вс", "пн", "вт", "ср", "чт", "пт", "сб"];

  const ruLocale = {
    ...ru,
    formatDistance: (token) => ({
      ...ru.formatDistance(token),
      day: "dd",
    }),
    localize: {
      ...ru.localize,
      day: (date) => daysOfWeek[format(date, "E")],
    },
  };

  const handleDateChange = (ranges) => {
    setState([ranges.selection]);
    setStartDate(ranges.selection.startDate || null);
    setEndDate(new Date(ranges.selection.endDate).setHours(23, 59, 59) || null);
  };

  const calendarRef = useRef(null);
  useOutsideClick(calendarRef, () => {
    if (isCalendarOpen) setTimeout(() => setCalendarOpen(false), 150);
  });
  const arrowTransform = {
    transform: isCalendarOpen ? "rotate(180deg)" : "rotate(0)",
    transition: "transform 0s ease",
  };
  const hasFilter = state[0].startDate && state[0].endDate;
  return (
    <Stack
      spacing={{ xs: "20px", sm: "10px", md: "20px", lg: "35px", xl: "50px" }}
      direction={{ xs: "column", sm: "row" }}
      sx={{
        width: "100%",
      }}
    >
      <Stack>
        <TextField
          // variant={"filled"}

          sx={{
            "& .MuiInputBase-root": {
              backgroundColor: "secondary.main",
              alignItems: "center",
              paddingTop: "10px",
              paddingBottom: "10px",
              borderRadius: "100px",
              width: { xs: "100%", sm: "180px", md: "300px", lg: "440px" },
            },
            "& .MuiOutlinedInput-notchedOutline": {
              border: "none",
            },
            "& .MuiInputBase-input": {
              padding: "0px 12px",
              borderRadius: "100px",
              backgroundColor: "secondary.main",

              color: "primary.main",
            },
            backgroundColor: "secondary.main",
            borderRadius: "100px",
          }}
          placeholder="Название мероприятия"
          type="text"
          onChange={(event) => setSearchText(event.target.value)}
          InputProps={{
            endAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        ></TextField>
      </Stack>
      <Stack>
        <Box
          ref={calendarRef}
          sx={{
            width: { xs: "100%", sm: "200px", md: "270px", lg: "325px" },

            height: "42px",
            cursor: "pointer",
            borderRadius: "100px",
            backgroundColor: "secondary.main",
            position: "relative",
            alignItems: "center",
            display: "flex",
            justifyContent: "space-between",
            paddingBottom: "12px",
            p: "12px 12px 12px 20px",
          }}
          onClick={() => setCalendarOpen(!isCalendarOpen)}
        >
          <Typography color={"primary.main"}>Дата проведения</Typography>
          <Box display="flex" alignItems="center">
            <KeyboardArrowDownIcon color="primary" sx={arrowTransform} />
            {hasFilter && (
              <Box
                sx={{
                  width: 8,
                  height: 8,
                  borderRadius: "50%",
                  backgroundColor: "primary.main",
                  position: "absolute",
                  right: { xs: 40, sm: 45, md: 90, lg: 150 },
                  top: 18,
                }}
              />
            )}
          </Box>
        </Box>
        <Box
          ref={calendarRef}
          sx={{
            position: "absolute",
            zIndex: 1,
            display: isCalendarOpen ? "block" : "none",
            "& .rdrCalendarWrapper": {
              borderRadius: "10px",
            },
            "& .rdrDateRangeWrapper": {
              backgroundColor: "secondary.main",
              borderRadius: "10px",
            },
            "& .rdrDateDisplayWrapper": {
              backgroundColor: "secondary.main",
              borderRadius: "10px",
            },
          }}
          marginTop={"55px"}
        >
          <DateRange
            style={{
              fontFamily: "'Inter', sans-serif",
            }}
            editableDateInputs={true}
            onChange={handleDateChange}
            moveRangeOnFirstSelection={false}
            ranges={state}
            locale={ruLocale}
            startDatePlaceholder={"Начало"}
            endDatePlaceholder={"Конец"}
            dateDisplayFormat="dd MMMM yyyy"
            monthDisplayFormat="MMMM yyyy"
          />
        </Box>
      </Stack>
      <FormControl
        variant={"filled"}
        sx={{
          width: { xs: "100%", sm: "180px", md: "250px" },

          "& .MuiTypography-root": {
            textAlign: "left",
            color: "primary.main",
          },
        }}
      >
        <Select
          MenuProps={{
            PaperProps: {
              sx: {
                borderRadius: "15px",
                marginTop: "12px",
                boxShadow: "none",
              },
            },
          }}
          sx={{
            "& .MuiSelect-select": {
              p: "12px 20px 12px 20px",
              backgroundColor: "secondary.main",
              borderRadius: "100px",
            },
          }}
          disableUnderline={true}
          defaultValue={0}
          onChange={(event) => setClubRegionState(event.target.value)}
          IconComponent={KeyboardArrowDownIcon}
        >
          <MenuItem value={0}>
            <Typography variant="body1" color={"primary.main"}>
              Выберите место
            </Typography>
          </MenuItem>
          {citiesData.map((city) => (
            <MenuItem value={city.id}>
              <Typography
                variant="body1"
                color={"primary.main"}
                textAlign={"center"}
              >
                {city.name}
              </Typography>
            </MenuItem>
          ))}
          <MenuItem value={"sirius"}>
            <Typography
              variant="body1"
              color={"primary.main"}
              textAlign={"center"}
            >
              Сириус
            </Typography>
          </MenuItem>
        </Select>
      </FormControl>
    </Stack>
  );
}
