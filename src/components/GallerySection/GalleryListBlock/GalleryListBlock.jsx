import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React from "react";
import GallaryBlock from "../GalleryBlock/GalleryBlock";
import Loader from "../../Loader/Loader";

export default function GalleryListBlock({ filteredData, loading }) {
  if (loading) {
    return <Loader />;
  }
  return (
    <Grid2 spacing={"20px"} container>
      {filteredData?.map((item) => (
        <Grid2 xs={12} sm={6} md={4} xl={3}>
          <GallaryBlock loading={loading} galleryData={item} />
        </Grid2>
      ))}
    </Grid2>
  );
}
