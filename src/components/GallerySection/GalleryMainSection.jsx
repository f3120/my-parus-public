import { Box, Container, Stack } from "@mui/material";

import React, { useEffect, useMemo, useState } from "react";

import { gql, useQuery } from "@apollo/client";

import Loader from "../Loader/Loader";
import GalleryTitle from "./GalleryTitle/GalleryTitle";
import GallertFilter from "./GalleryFilter/GallertFilter";
import GalleryListBlock from "./GalleryListBlock/GalleryListBlock";
import { parse } from "date-fns";
import { ru } from "date-fns/locale";
import { isBefore, isAfter, isEqual } from "date-fns";
import dayjs from "dayjs";
const APOLLO_QUERY = gql`
  query QueryGallery($categoryName: String!) {
    posts(where: { categoryName: $categoryName }, first: 1000) {
      edges {
        node {
          id
          title
          galleryfield {
            dategallery
            fieldGroupName
            pivotphoto {
              sourceUrl
            }
          }
          clubfield {
            clubname
          }
        }
      }
    }
  }
`;

export default function GalleryMainSection() {
  const { data, loading } = useQuery(APOLLO_QUERY, {
    variables: { categoryName: "gallery" },
  });

  const gallery = useMemo(() => {
    return data?.posts?.edges.map((node) => ({
      id: node?.node?.id,
      label: node?.node?.title,
      date: node?.node?.galleryfield?.dategallery,
      region: node?.node?.clubfield?.clubname,
      photos: node?.node?.galleryfield?.pivotphoto?.sourceUrl,
    }));
  }, [data]);

  const [clubRegion, setClubRegionState] = useState("");
  const [searchText, setSearchText] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  // console.log({ galleryData });

  useEffect(() => {
    setFilteredData(
      gallery
        ?.filter((item) => {
          const itemDate = parse(item.date, "dd.MM.yyyy", new Date(), {
            locale: ru,
          });

          return (
            (!clubRegion || item.region === clubRegion) &&
            (!startDate ||
              !endDate ||
              (isBefore(itemDate, endDate) && isAfter(itemDate, startDate)) ||
              isEqual(itemDate, startDate))
          );
        })
        .sort((a, b) => {
          const aDate = parse(a.date, "dd.MM.yyyy", new Date(), {
            locale: ru,
          });
          const bDate = parse(b.date, "dd.MM.yyyy", new Date(), {
            locale: ru,
          });

          return dayjs(bDate).unix() - dayjs(aDate).unix();
        })
    );
  }, [clubRegion, startDate, endDate, gallery]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setFilteredData(
        gallery
          ?.filter((item) => {
            return item?.label
              ?.toLowerCase()
              .includes(searchText.toLowerCase());
          })
          .sort((a, b) => {
            const aDate = parse(a.date, "dd.MM.yyyy", new Date(), {
              locale: ru,
            });
            const bDate = parse(b.date, "dd.MM.yyyy", new Date(), {
              locale: ru,
            });

            return dayjs(bDate).unix() - dayjs(aDate).unix();
          })
      );
    }, 500);

    return () => clearTimeout(timer);
  }, [searchText, gallery]);

  return (
    <Box>
      <Container>
        <Stack gap={"40px"}>
          <GalleryTitle />
          <GallertFilter
            setStartDate={setStartDate}
            setEndDate={setEndDate}
            setSearchText={setSearchText}
            setClubRegionState={setClubRegionState}
          />
          <GalleryListBlock loading={loading} filteredData={filteredData} />
        </Stack>
      </Container>
    </Box>
  );
}
