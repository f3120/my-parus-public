import { Box, Stack, Typography } from "@mui/material";
import React from "react";
import Gallery from "../../../media/images/Gallery.png";
import dayjs from "dayjs";
import Loader from "../../Loader/Loader";
import { navigate } from "gatsby";

import "dayjs/locale/ru";
export default function GallaryBlock({ galleryData, loading }) {
  dayjs.locale("ru");
  const { date, label, id, photos } = galleryData;
  if (loading) {
    return <Loader />;
  }
  return (
    <Stack
      sx={{
        height: { xs: "200px", md: "240px", lg: "320px" },
        backgroundImage: `url(${photos})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        borderRadius: "25px",
      }}
    >
      <Stack
        width={"100%"}
        height={"100%"}
        direction={"column"}
        padding={"15px"}
        sx={{
          justifyContent: "space-between",
          borderRadius: "25px",
          background:
            "linear-gradient(180deg, rgba(6, 11, 17, 0) 0%, rgba(6, 11, 17, 0.125) 50%, rgba(6, 11, 17, 0.7) 100%)",
        }}
      >
        <Stack justifyContent={"end"} direction="row" flexWrap="wrap">
          <Stack
            backgroundColor={"primary.white"}
            p={"8px 12px 8px 12px"}
            borderRadius={"30px"}
          >
            <Typography color={"primary.main"}>{date}</Typography>
          </Stack>
        </Stack>
        <Stack direction={"column"}>
          <Typography color={"primary.white"} variant="h5">
            {label}
          </Typography>

          <Box
            onClick={() => {
              const url = `/photoSheet/?ID=${id}`;
              navigate(url, { state: { id } });
            }}
            sx={{
              marginTop: "10px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "43px",
              borderRadius: "30px",
              border: "1px solid #FFFFFF ",
              cursor: "pointer",
            }}
          >
            <Typography>Все фотографии</Typography>
          </Box>
        </Stack>
      </Stack>
    </Stack>
  );
}
