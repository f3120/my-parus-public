import { Button } from "@mui/material";
import React from "react";
import arrow45, {
  ReactComponent as Arrow45,
} from "../../media/icons/Arrow45deg.svg";

export default function ArrowButton({
  sx,
  onClick,
  variant = "contained",
  svgProps = { width: "40%", height: "40%", stroke: "#FFFFFF" },
}) {
  return (
    <Button
      onClick={onClick}
      borderRadius={"100%"}
      sx={{
        p: "0",
        width: { xs: "30px", md: "40px", lg: "40px" },
        height: { xs: "30px", md: "40px", lg: "40px" },
        lineHeight: 0,
        ...sx,
      }}
      variant={variant}
    >
      <Arrow45
        width={svgProps?.width || "40%"}
        height={svgProps?.height || "40%"}
        stroke={svgProps?.stroke || "#FFFFFF"}
      />
    </Button>
  );
}
