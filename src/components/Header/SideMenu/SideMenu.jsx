import { Box, Container, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import handWithPen from "../../../media/images/HandWithPen.png";
import closeIcon, {
  ReactComponent as CloseIcon,
} from "../../../media/icons/CloseIcon.svg";
import vkicon, {
  ReactComponent as VkIcon,
} from "../../../media/icons/VKIcon.svg";
import tgIcon, {
  ReactComponent as TgIcon,
} from "../../../media/icons/TGIcon.svg";
import waIcon, {
  ReactComponent as WaIcon,
} from "../../../media/icons/WAIcon.svg";
import ytIcon, {
  ReactComponent as YtIcon,
} from "../../../media/icons/YTIcon.svg";
import { ModalContext } from "../../../context/ModalContext";
import { navigate } from "gatsby";
import Link from "@mui/material/Link";
import { citiesData } from "../../../config/data/citiesData";

export default function SideMenu({
  setSideMenuOpen,
  clubRegion,
  setClubRegionState,
}) {
  const { setModalActive } = useContext(ModalContext);
  return (
    <Box
      sx={{
        width: "100%",
        backgroundColor: "primary.white",
        position: "relative",
        pt: "60px",
        pb: "27px",
        borderRadius: "30px 0 0 30px",
      }}
    >
      <Box
        sx={{
          width: "40px",
          height: "40px",
          border: "1px solid",
          borderColor: "primary.main",
          borderRadius: "100%",
          position: "absolute",
          right: "20px",
          top: "10px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          cursor: "pointer",
        }}
        onClick={() => setSideMenuOpen(false)}
      >
        <CloseIcon width="14px" height="14px" stroke={"#324F7B"} />
      </Box>
      <Container>
        <Stack direction={"column"} gap={"56px"}>
          <Stack direction={"column"} gap={"15px"}>
            {/* <Typography
              onClick={() => setModalActive("developCard")}
              variant="body1"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
            >
              О нас
            </Typography> */}
            {citiesData.map((city) => (
              <Typography
                onClick={() => {
                  setClubRegionState(city.id);
                  navigate(city.url);
                }}
                variant="body1"
                color={"primary.main"}
                sx={{ cursor: "pointer" }}
              >
                Мой парус {city.nameWhere}
              </Typography>
            ))}

            <Typography
              onClick={() => navigate(`/shop`)}
              variant="body1"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
            >
              Магазин
            </Typography>
            <Typography
              onClick={() => navigate(`/photo`)}
              variant="body1"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
            >
              Галерея
            </Typography>
            {/* <Typography
              onClick={() => navigate(`/events`)}
              variant="body1"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
            >
              Мероприятия
            </Typography> */}
            <Typography
              onClick={() => navigate(`/news`)}
              variant="body1"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
            >
              Новости
            </Typography>
            {/* <Typography
              onClick={() => setModalActive("developCard")}
              variant="body1"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
            >
              Cоревнования
            </Typography> */}
            <Link href="#contacts">
              <Typography
                onClick={() => setSideMenuOpen(false)}
                variant="body1"
                color={"primary.main"}
                sx={{ cursor: "pointer", textDecoration: "none" }}
              >
                Контакты
              </Typography>
            </Link>
          </Stack>
          <Stack flexDirection={"column"} gap={"22px"} alignItems={"flex-end"}>
            <Typography
              mr={{ xs: "15px" }}
              variant="subtitle1"
              color={"primary.main"}
              component={Link}
              href="tel:88005503006"
            >
              8(800)550-30-06
            </Typography>
            <Box
              sx={{
                backgroundColor: "secondary.main",
                p: "5px",
                borderRadius: "25px",
                pr: "20px",
                cursor: "pointer",
              }}
              onClick={() => setModalActive("feedback")}
            >
              <Stack direction={"row"} alignItems={"center"} gap={"10px"}>
                <Box
                  sx={{
                    width: "40px",
                    height: "40px",
                    borderRadius: "100%",
                    backgroundColor: "primary.white",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <img src={handWithPen} alt="handWithPen" />
                </Box>
                <Typography variant="body1" color={"primary.main"}>
                  Напишите нам
                </Typography>
              </Stack>
            </Box>
            <Stack
              direction={"row"}
              sx={{ gap: "15px" }}
              justifyContent={{ xs: "center" }}
            >
              <Box
                sx={{
                  height: "40px",
                  width: "40px",
                  cursor: "pointer",
                }}
              >
                <a
                  href="https://vk.com/my_sail"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img width={"100%"} src={vkicon} alt="arrow" />
                </a>
              </Box>
              <Box
                sx={{
                  height: "40px",
                  width: "40px",
                  cursor: "pointer",
                }}
              >
                <a
                  href="http://t.me/mysail_club"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img width={"100%"} src={tgIcon} alt="arrow" />
                </a>
              </Box>
              <Box
                sx={{
                  height: "40px",
                  width: "40px",
                  cursor: "pointer",
                }}
              >
                <a
                  href="https://wa.me/79384727938"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img width={"100%"} src={waIcon} alt="arrow" />
                </a>
              </Box>
              <Box
                sx={{
                  height: "40px",
                  width: "40px",
                  cursor: "pointer",
                }}
              >
                <a
                  href="https://youtube.com/channel/UC90zn1qc-MjU6FsjFK98TRA"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img width={"100%"} src={ytIcon} alt="arrow" />
                </a>
              </Box>
            </Stack>
          </Stack>
        </Stack>
      </Container>
    </Box>
  );
}
