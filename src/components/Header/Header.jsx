import {
  Box,
  Container,
  FormControl,
  MenuItem,
  Select,
  Stack,
  Typography,
  useMediaQuery,
  Link,
} from "@mui/material";
import React, { useContext, useEffect, useState } from "react";
import moiParus_logo, {
  ReactComponent as MoiParusLogo,
} from "../../media/icons/MoiParus_logo.svg";
import MoiParus_logo_big, {
  ReactComponent as MoiParusLogoBig,
} from "../../media/icons/MoiParus_logo_big.svg";
import burgerIcon, {
  ReactComponent as BurgerIcon,
} from "../../media/icons/BurgerIcon.svg";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import SideMenu from "./SideMenu/SideMenu";
import { mainTheme } from "../../config/MUI/mainTheme";
import { ModalContext } from "../../context/ModalContext";
import { navigate } from "gatsby";
import { citiesData } from "../../config/data/citiesData";

export default function Header({ clubRegion, setClubRegionState }) {
  const [sideMenuOpen, setSideMenuOpen] = React.useState(false);

  useEffect(() => {
    if (typeof document !== "undefined") {
      document.body.style.overflow = sideMenuOpen ? "hidden" : "auto";
    }
  }, [sideMenuOpen]);

  const upMd = useMediaQuery(mainTheme.breakpoints.up("md"));
  const { setModalActive } = useContext(ModalContext);
  return (
    <Box
      bgcolor={mainTheme.palette.secondary.main + "e6"}
      sx={{
        width: "100%",
        borderRadius: "0 0 30px 30px",
        position: "sticky",
        top: 0,
        height: "68px",
        zIndex: 10,
      }}
    >
      <Container sx={{ height: "100%" }}>
        <Stack
          direction={"row"}
          height={"100%"}
          width={"100%"}
          alignItems={"center"}
          justifyContent={{ xs: "space-between", md: "flex-start" }}
        >
          <Box sx={{ cursor: "pointer" }} onClick={() => navigate("/")}>
            {upMd ? <MoiParusLogoBig /> : <MoiParusLogo />}
          </Box>
          <Stack
            display={{ xs: "none", md: "flex" }}
            direction={"row"}
            gap={"40px"}
            flexGrow={{ md: 1 }}
            ml={{ md: "50px" }}
          >
            <Typography
              onClick={() =>
                navigate(
                  clubRegion
                    ? citiesData.find((el) => el.id === clubRegion)?.url
                    : "/moscow"
                )
              }
              variant="body1"
              color={"primary.main"}
              display={{ xs: "none", md: "block" }}
              sx={{ cursor: "pointer" }}
            >
              Мой парус{" "}
              {clubRegion
                ? citiesData.find((el) => el.id === clubRegion)?.nameWhere
                : "в Москве"}
            </Typography>
            <Typography
              onClick={() => navigate(`/shop`)}
              variant="body1"
              color={"primary.main"}
              display={{ xs: "none", md: "block" }}
              sx={{ cursor: "pointer" }}
            >
              Магазин
            </Typography>
            <Typography
              onClick={() => navigate(`/photo`)}
              variant="body1"
              color={"primary.main"}
              display={{ xs: "none", lg: "block" }}
              sx={{ cursor: "pointer" }}
            >
              Галерея
            </Typography>
            {/* <Typography
              onClick={() => navigate(`/events`)}
              variant="body1"
              color={"primary.main"}
              display={{ xs: "none", lg: "block" }}
              sx={{ cursor: "pointer" }}
            >
              Мероприятия
            </Typography> */}
            <Typography
              variant="body1"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
              onClick={() => setSideMenuOpen((sideMenuOpen) => !sideMenuOpen)}
            >
              Ещё
            </Typography>
          </Stack>
          <Typography
            display={{ xs: "none", md: "block" }}
            mr={{ xs: "15px" }}
            variant="subtitle1"
            color={"primary.main"}
            component={Link}
            href="tel:88005503006"
          >
            8(800)550-30-06
          </Typography>
          <Box width={{ xs: 145, md: "auto" }}>
            <FormControl fullWidth variant={upMd ? "outlined" : "standard"}>
              <Select
                disableUnderline={true}
                defaultValue={clubRegion || -1}
                MenuProps={{ PaperProps: { sx: { borderRadius: "15px" } } }}
                IconComponent={KeyboardArrowDownIcon}
                onChange={(e) => {
                  setClubRegionState(e.target.value);
                  navigate(
                    citiesData.find((el) => el.id === e.target.value)?.url
                  );
                }}
              >
                <MenuItem value={-1} disabled>
                  <Typography
                    variant="body1"
                    color={"primary.main"}
                    textAlign={"left"}
                  >
                    Выберите регион
                  </Typography>
                </MenuItem>
                {citiesData.map((city) => (
                  <MenuItem
                    value={city.id}
                    onClick={() => navigate(city.url)}
                    key={city.id}
                  >
                    <Typography
                      variant="body1"
                      color={"primary.main"}
                      textAlign={"center"}
                    >
                      {city.name}
                    </Typography>
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
          <Box
            sx={{ display: { xs: "block", md: "none" }, cursor: "pointer" }}
            onClick={() => setSideMenuOpen((sideMenuOpen) => !sideMenuOpen)}
          >
            <BurgerIcon />
          </Box>
        </Stack>
      </Container>
      <Box
        sx={{
          position: "fixed",
          top: 0,
          right: 0,
          width: { xs: "100%", sm: "350px" },
          transform: sideMenuOpen ? "translateX(0)" : "translateX(100%)",
          transition: "transform 0.3s",
          zIndex: 1001,
        }}
      >
        <SideMenu
          clubRegion={clubRegion}
          setSideMenuOpen={setSideMenuOpen}
          setClubRegionState={setClubRegionState}
        />
      </Box>
      <Box
        sx={{
          pointerEvents: sideMenuOpen ? "auto" : "none",
          position: "fixed",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          background: "#00000080",
          opacity: sideMenuOpen ? 1 : 0,
          zIndex: 1000,
        }}
        onClick={() => setSideMenuOpen(false)}
      ></Box>
    </Box>
  );
}
