import { Stack, Typography, Box } from "@mui/material";
import React from "react";

export default function ChipWithIcon({
  icon,
  text,
  onClick,
  sx,
  textEmoji,
  invertColor,
  iconStyle = {},
}) {
  return (
    <Stack
      direction="row"
      bgcolor={invertColor ? "primary.white" : "secondary.main"}
      sx={{
        borderRadius: "100px",
        cursor: onClick ? "pointer" : "auto",
        ...sx,
      }}
      alignItems={"center"}
      p={{ xs: "5px 10px 5px 2px", sm: "5px 10px 5px 4px" }}
      onClick={onClick}
    >
      {icon || textEmoji ? (
        <Box
          sx={{
            minWidth: { xs: "24px", sm: "30px" },
            minHeight: { xs: "24px", sm: "30px" },
            maxWidth: { xs: "24px", sm: "30px" },
            maxHeight: { xs: "24px", sm: "30px" },
            borderRadius: "100px",
            backgroundColor: invertColor ? "secondary.main" : "primary.white",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {icon ? (
            <img src={icon} alt="clubEvents" style={{ ...iconStyle }} />
          ) : (
            <Typography>{textEmoji}</Typography>
          )}
        </Box>
      ) : (
        <Box sx={{ width: "10px" }}></Box>
      )}

      <Typography marginLeft={"5px"} color={"primary.main"} flexGrow={"2"}>
        {text}
      </Typography>
    </Stack>
  );
}
