import { Box, Container, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import { ReactComponent as ShoppingCart } from "../../../media/icons/shopping-cart.svg";
import { mainTheme } from "../../../config/MUI/mainTheme";
import { navigate } from "gatsby";
import { CartContext } from "../../../context/CartContext";

export default function ShopHeader({ hideCart }) {
  const { cartItems } = useContext(CartContext);

  return (
    <Container>
      <Stack
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"center"}
        p={{ xs: "12px 0", md: "20px 0" }}
      >
        <Stack
          direction={"row"}
          justifyContent={"flex-start"}
          alignItems={"center"}
          gap={{ xs: "10px", md: "20px" }}
        >
          <Typography
            onClick={() => {
              navigate("/shop");
            }}
            variant="body1"
            color={"primary.main"}
            p={"1px"}
            sx={{ cursor: "pointer" }}
            display={{ xs: "none", sm: "block" }}
          >
            Все товары
          </Typography>
          <Typography
            onClick={() => {
              navigate("/shop/clothes");
            }}
            variant="body1"
            color={"primary.main"}
            p={"1px"}
            sx={{ cursor: "pointer" }}
          >
            Одежда
          </Typography>
          <Typography
            onClick={() => {
              navigate("/shop/accessories");
            }}
            variant="body1"
            color={"primary.main"}
            p={"1px"}
            sx={{ cursor: "pointer" }}
          >
            Аксессуары
          </Typography>
        </Stack>
        <Stack
          direction={"row"}
          justifyContent={"flex-end"}
          alignItems={"center"}
          gap={"2px"}
          sx={{ cursor: "pointer" }}
          onClick={() => navigate("/cart")}
          display={hideCart ? "none" : "flex"}
        >
          <ShoppingCart
            width={"24px"}
            height={"24px"}
            stroke={mainTheme.palette.primary.main}
          />
          <Box
            borderRadius={"100%"}
            width={"24px"}
            height={"24px"}
            display={"flex"}
            justifyContent={"center"}
            alignItems={"center"}
            bgcolor={"primary.main"}
          >
            <Typography variant="caption" color={"primary.white"}>
              {
                cartItems.filter((cartItem) => cartItem && cartItem?.quantity)
                  .length
              }
            </Typography>
          </Box>
        </Stack>
      </Stack>
    </Container>
  );
}
