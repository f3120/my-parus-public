import {
  Box,
  Container,
  FormControl,
  MenuItem,
  Select,
  Stack,
  Typography,
} from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { navigate } from "gatsby";
import React from "react";
import { ReactComponent as Sort } from "../../../media/icons/sort.svg";
import { ReactComponent as SortUp } from "../../../media/icons/sortUp.svg";
import { ReactComponent as SortDown } from "../../../media/icons/sortDown.svg";
import { gql } from "@apollo/client";
import ShopBreadcrumbs from "../ShopBreadcrumbs/ShopBreadcrumbs";

export default function ShopFilterSortBlock({
  sortParam = "default",
  setSortParam = () => {},
}) {
  return (
    <Container
      sx={{
        mt: { xs: "-30px", md: "-50px" },
        mb: { xs: "-30px", md: "-50px" },
      }}
    >
      <Stack
        direction={{ sx: "column", sm: "row" }}
        justifyContent={"space-between"}
        alignItems={{ xs: "flex-start", sm: "center" }}
      >
        <ShopBreadcrumbs />
        <Box width={{ xs: "100%", sm: "auto" }}>
          <FormControl fullWidth variant={"filled"}>
            <Select
              disableUnderline={true}
              MenuProps={{ PaperProps: { sx: { borderRadius: "15px" } } }}
              IconComponent={KeyboardArrowDownIcon}
              defaultValue={sortParam}
              onChange={(e) => setSortParam(e.target.value)}
              sx={{
                "& .MuiSelect-select": {
                  paddingTop: { xs: "10px", sm: "15px" },
                  paddingBottom: { xs: "10px", sm: "15px" },
                },
              }}
            >
              <MenuItem value={"default"}>
                <Stack
                  direction={"row"}
                  justifyContent={"center"}
                  alignItems={"center"}
                  gap={"5px"}
                >
                  <Sort width={"20px"} height={"20px"} />
                  <Typography variant="body1" color={"primary.main"}>
                    Сортировать
                  </Typography>
                </Stack>
              </MenuItem>
              <MenuItem value={"asc"}>
                <Stack
                  direction={"row"}
                  justifyContent={"center"}
                  alignItems={"center"}
                  gap={"5px"}
                >
                  <SortUp width={"20px"} height={"20px"} />
                  <Typography variant="body1" color={"primary.main"}>
                    По возрастанию цены
                  </Typography>
                </Stack>
              </MenuItem>
              <MenuItem value={"desc"}>
                <Stack
                  direction={"row"}
                  justifyContent={"center"}
                  alignItems={"center"}
                  gap={"5px"}
                >
                  <SortDown width={"20px"} height={"20px"} />
                  <Typography variant="body1" color={"primary.main"}>
                    По убыванию цены
                  </Typography>
                </Stack>
              </MenuItem>
            </Select>
          </FormControl>
        </Box>
      </Stack>
    </Container>
  );
}
