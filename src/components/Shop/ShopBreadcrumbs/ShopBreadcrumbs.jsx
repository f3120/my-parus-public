import { Stack, Typography } from "@mui/material";
import { navigate } from "gatsby";
import React from "react";

export default function ShopBreadcrumbs({ productName, productCategory }) {
  const location = typeof window !== "undefined" && window.location.pathname;

  return (
    <Stack
      direction={"row"}
      gap={"10px"}
      justifyContent={"flex-start"}
      alignItems={"center"}
      p={{ xs: "10px 0", md: "20px 0" }}
      flexWrap={"wrap"}
    >
      {location &&
        location
          .split("/")
          .filter((item) => breadCrumbsMap.get(item))
          .map((item, index) => {
            return (
              <>
                <Typography
                  variant="caption"
                  color={"primary.main"}
                  sx={{ cursor: index === 0 ? "pointer" : "auto" }}
                  onClick={() => navigate(`/${item}`)}
                >
                  {breadCrumbsMap.get(item)}
                </Typography>
                {index !==
                  location.split("/").filter((item) => item).length - 1 && (
                  <Typography variant="caption" color={"primary.main"}>
                    {">"}
                  </Typography>
                )}
              </>
            );
          })}
      {productCategory && (
        <>
          <Typography
            variant="caption"
            color={"primary.main"}
            sx={{ cursor: "pointer" }}
            onClick={() => navigate(`/shop/${productCategory}`)}
          >
            {breadCrumbsMap.get(productCategory)}
          </Typography>
          <Typography variant="caption" color={"primary.main"}>
            {">"}
          </Typography>
        </>
      )}
      {productName && (
        <Typography variant="caption" color={"primary.main"}>
          {productName}
        </Typography>
      )}
    </Stack>
  );
}

const breadCrumbsMap = new Map([
  ["shop", "Все товары"],
  ["clothes", "Одежда"],
  ["accessories", "Аксессуары"],
]);
