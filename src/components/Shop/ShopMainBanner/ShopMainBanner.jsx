import { Container, Stack, Typography, useMediaQuery } from "@mui/material";
import React from "react";
import verticalBanner from "../../../media/images/AbstractShopVertical.png";
import horizontalBanner from "../../../media/images/AbstractShopHorizontal.png";
import clothes from "../../../media/images/clothes.png";
import accessories from "../../../media/images/accessories.png";
import ShopMainBannerCard from "./ShopMainBannerCard/ShopMainBannerCard";
import { mainTheme } from "../../../config/MUI/mainTheme";

export default function ShopMainBanner() {
  const upSm = useMediaQuery(mainTheme.breakpoints.up("sm"));
  return (
    <Container>
      <Stack
        direction={{ xs: "column", sm: "row" }}
        justifyContent={"space-between"}
        alignItems={"center"}
        sx={{
          background: `url(${upSm ? horizontalBanner : verticalBanner})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          borderRadius: "20px",

          p: {
            xs: "15px",
          },
        }}
        gap={"10px"}
      >
        <Stack
          ml={{ xs: "0", sm: "5px", md: "10px", lg: "20px" }}
          p={"10px 0"}
          direction={"column"}
          justifyContent={"center"}
          alignItems={"flex-start"}
        >
          <Typography variant="h2">Магазин</Typography>
          <Typography variant="body1" display={{ xs: "none", md: "inline" }}>
            Погрузитесь в мир морских фантазий
            <br />с нашей обновленной линейкой одежды для маленьких капитанов
          </Typography>
        </Stack>

        <Stack
          width={{ xs: "100%" }}
          direction={{ xs: "column", sm: "row" }}
          justifyContent={"flex-end"}
          alignItems={"center"}
          gap={"10px"}
        >
          <ShopMainBannerCard
            image={accessories}
            title={"К аксессуарам"}
            link={"/accessories"}
            textEmoji={"👓"}
          />
          <ShopMainBannerCard
            image={clothes}
            title={"К одежде"}
            link={"/clothes"}
            textEmoji={"👕"}
          />
        </Stack>
      </Stack>
    </Container>
  );
}
