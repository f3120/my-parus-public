import { Box, Stack } from "@mui/material";
import React from "react";
import ChipWithIcon from "../../../ChipWithIcon/ChipWithIcon";
import { navigate } from "gatsby";

export default function ShopMainBannerCard({
  image,
  title,
  link,
  textEmoji,
  sx = {},
}) {
  return (
    <Stack
      direction={"column"}
      justifyContent={"space-between"}
      alignItems={"center"}
      sx={{
        width: { xs: "100%", sm: "30vw", md: "20vw" },
        maxWidth: { xs: "100%", lg: "350px" },
        borderRadius: "20px",
        p: { xs: "5px" },
        backdropFilter: "blur(4px)",
        boxShadow: "0 4px 10px 0 rgba(0, 0, 0, 0.25)",
        background: "rgba(255, 255, 255, 0.3)",
        transition: "all 0.4s ease",
        "&:hover": {
          boxShadow: "0 4px 20px 10px rgba(0, 0, 0, 0.4)",
        },
        ...sx,
      }}
    >
      <Box
        sx={{
          width: "100%",
          height: { xs: "40vw", sm: "15vw", md: "12vw" },
          maxHeight: { xs: "40vw", lg: "220px" },
          borderRadius: "20px",
          overflow: "hidden",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          background: `url(${image})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          border: "3px solid #FFFFFF",
        }}
      ></Box>
      <Stack
        width={"100%"}
        direction={"row"}
        justifyContent={"flex-start"}
        alignItems={"center"}
      >
        <ChipWithIcon
          sx={{ width: "100%", margin: "5px 0 0 0" }}
          textEmoji={textEmoji}
          text={title}
          onClick={() => navigate("/shop" + link)}
        />
      </Stack>
    </Stack>
  );
}
