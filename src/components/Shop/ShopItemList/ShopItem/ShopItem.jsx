import { Box, Button, Stack, Typography } from "@mui/material";
import { ReactComponent as ShoppingCart } from "../../../../media/icons/shopping-cart.svg";
import React from "react";
import { Link, navigate } from "gatsby";

export default function ShopItem({ product, noClick }) {
  const {
    name,
    image,
    onSale,
    regularPrice,
    price,
    id,
    type,
    productCategories,
  } = product;

  const handleNavigate = () => {
    const url = `/shop/product/?ID=${id}`;
    !noClick &&
      navigate(url, {
        state: {
          id,
          name,
          productCategory: productCategories?.nodes[0]?.slug,
        },
      });
  };

  return (
    <Stack
      direction={"column"}
      gap={"10px"}
      sx={{ width: "100%", height: "100%" }}
      justifyContent={"space-between"}
      alignItems={"flex-start"}
    >
      <Box
        sx={{
          width: "100%",
          borderRadius: "20px",
          overflow: "hidden",
          height: { xs: "50vw", sm: "30vw", md: "20vw" },
          maxHeight: "250px",
          background: `url(${image?.sourceUrl})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          cursor: "pointer",
        }}
        onClick={() => handleNavigate()}
      ></Box>
      <Stack direction={"column"} gap={"5px"}>
        <Typography variant="body1" color={"primary.main"}>
          <strong>{correctingPrice(onSale ? price : regularPrice)} ₽</strong>{" "}
          <strike>{onSale && correctingPrice(regularPrice) + "  ₽"}</strike>
        </Typography>
        <Typography
          variant="body1"
          color={"primary.main"}
          sx={{ opacity: 0.7 }}
        >
          {name}
        </Typography>
      </Stack>
      <Button fullWidth variant="contained" onClick={() => handleNavigate()}>
        <ShoppingCart
          width={"18px"}
          height={"18px"}
          stroke={"#FFFFFF"}
          cursor={"pointer"}
          style={{ marginRight: "5px" }}
        />
        Купить
      </Button>
    </Stack>
  );
}

export const correctingPrice = (price) => {
  return Number.parseInt(String(price).split(",")[0]).toFixed();
};
