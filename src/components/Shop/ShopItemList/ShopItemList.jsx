import { Container } from "@mui/material";
import React from "react";
import ShopItem from "./ShopItem/ShopItem";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";

export default function ShopItemList({ itemList = [] }) {
  return (
    <Container>
      <Grid2 container spacing={{ xs: "30px" }} minHeight={"20vw"}>
        {itemList.map((item, index) => (
          <Grid2 xs={12} sm={4} lg={3} key={index}>
            <ShopItem product={item} />
          </Grid2>
        ))}
      </Grid2>
    </Container>
  );
}
