import { Box, Button, Container, Stack, Typography } from "@mui/material";
import React from "react";
import arrow, { ReactComponent as Arrow } from "../../../media/icons/Arrow.svg";
import dayjs from "dayjs";
import { mainTheme } from "../../../config/MUI/mainTheme";
import { navigate } from "gatsby";
export default function NewsItem({ newsItemData }) {
  const { date, title, id, descr, image, check, link } = newsItemData;
  dayjs.locale("ru");
  const checkLink = () => {
    if (Array.isArray(check) && check.length > 0) {
      window.open(link, "_blank");
    } else {
      navigate(`/newsMore/?ID=${id}`, { state: { id } });
    }
  };
  return (
    <Stack
      sx={{
        height: { xs: "400px", sm: "auto" },
        bgcolor: "secondary.main",
        padding: { xs: "10px", sm: "20px", lg: "30px" },
        borderRadius: { xs: "15px", sm: "20px", lg: "30px" },
      }}
      direction={{ xs: "column", sm: "row" }}
      justifyContent={"space-between"}
      gap={{ xs: "10px", sm: "20px" }}
    >
      <Box
        sx={{
          minWidth: { xs: "320px", lg: "400px" },
          minHeight: { xs: "200px", lg: "250px" },
          maxWidth: { xs: "320px", lg: "400px" },
          maxHeight: { xs: "200px", lg: "250px" },
          backgroundColor: "#FFF",
          backgroundImage: `url(${image})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          borderRadius: "15px",
          position: "relative",
        }}
      >
        <Box
          position={"absolute"}
          sx={{
            bgcolor: "primary.contrastText",
            opacity: 0.7,
            top: 10,
            right: 10,
            p: "10px 20px",
            borderRadius: "30px",
          }}
        >
          <Typography variant="body1" color="primary.main">
            {date}
          </Typography>
        </Box>
      </Box>
      <Stack
        display={{ xs: "flex", sm: "none" }}
        direction={{ xs: "column" }}
        gap={{ xs: "5px", lg: "20px" }}
      >
        <Typography variant="h4" color="primary.main">
          {title}
        </Typography>
        <Typography
          sx={{
            flexGrow: 1,
            display: "-webkit-box",
            WebkitBoxOrient: "vertical",
            WebkitLineClamp: 5, // задайте нужное количество строк
            overflow: "hidden",
          }}
          variant="body1"
          color="primary.main"
        >
          {descr}
        </Typography>
      </Stack>
      <Stack
        justifyContent={"space-between"}
        width={{ sm: "600px", md: "640px", lg: "662px", xl: "1000px" }}
        direction={{ xs: "column" }}
        gap={{ xs: "10px", lg: "20px" }}
      >
        <Stack
          display={{ xs: "none", sm: "flex" }}
          direction={{ xs: "column" }}
          gap={{ xs: "5px", lg: "20px" }}
        >
          <Typography variant="h4" color="primary.main">
            {title}
          </Typography>
          <Typography
            sx={{
              overflow: "hidden",
              textOverflow: "ellipsis",
            }}
            variant="body1"
            color="primary.main"
          >
            {descr}
          </Typography>
        </Stack>
        <Stack
          direction={"row"}
          justifyContent={{ xs: "center", md: "flex-end" }}
        >
          <Button
            onClick={checkLink}
            sx={{
              width: { xs: "100%", md: "auto" },
              borderColor: "primary.main",
              borderRadius: "30px",
              border: "1px solid",
            }}
          >
            <Typography color={"primary.main"} variant="body1">
              Читать дальше
            </Typography>
            <Stack
              alignItems="center"
              mt="2px"
              marginLeft={{
                xs: "10px",
                sm: "5px",
                md: "13px",
                lg: "18px",
                xl: "10px",
              }}
            >
              <Arrow
                width={"15px"}
                height={"15px"}
                stroke={mainTheme.palette.primary.main}
              />
            </Stack>
          </Button>
        </Stack>
      </Stack>
    </Stack>
  );
}
