import {
  Box,
  Button,
  CircularProgress,
  Container,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useContext } from "react";
import { mainTheme } from "../../config/MUI/mainTheme";
import newsParus from "../../media/images/newsParus.png";
import NewsStack from "./NewsStack/NewsStack";
import NewsSlider from "./NewsSlider/NewsSlider";
import { ModalContext } from "../../context/ModalContext";
import { gql, useQuery } from "@apollo/client";
import { navigate } from "gatsby";
import { Circle } from "@pbe/react-yandex-maps";

const newsData = [
  {
    id: 403,
    image: newsParus,
    title: "«Мой парус» – документальный фильм о море",
    descr:
      "Вместе с главными героями проживем несколько дней, окунемся в мир парусных яхт и узнаем историю создания и развития частного спортивного клуба «Мой парус».",
    date: "10.02.2024",
    link: "https://www.youtube.com/watch?v=iHqW-x6ktec",
  },
];

const APOLLO_QUERY = gql`
  query QueryPosts($categoryName: String!) {
    posts(where: { categoryName: $categoryName }, first: 3) {
      edges {
        node {
          id
          title
          postfield {
            fieldGroupName
            postauthor
            shortdescriptionpost
            postoptionalfield
            postlinkoutservice
            postdata
            postimage {
              sourceUrl
            }
          }
          clubfield {
            clubname
          }
          content
        }
      }
    }
  }
`;

export default function NewsSection() {
  const { data, loading } = useQuery(APOLLO_QUERY, {
    variables: { categoryName: "post" },
  });

  const newsData = data?.posts?.edges.map((node) => ({
    id: node?.node?.id,
    title: node?.node?.title,
    date: node?.node?.postfield?.postdata,
    descr: node?.node?.postfield?.shortdescriptionpost,
    image: node?.node?.postfield?.postimage?.sourceUrl,
    check: node?.node?.postfield?.postoptionalfield,
    link: node?.node?.postfield?.postlinkoutservice,
  }));

  const upMd = useMediaQuery(mainTheme.breakpoints.up("md"));
  const { setModalActive } = useContext(ModalContext);

  return (
    <Box
      sx={{
        width: "100%",
      }}
    >
      <Box bgcolor={{ xs: "secondary.main", md: "transparent" }}>
        <Container>
          <Stack
            bgcolor={"secondary.main"}
            borderRadius={"30px"}
            direction={{ sm: "row" }}
            alignItems={"center"}
            sx={{
              width: "100%",
            }}
            padding={{ xs: "10px 20px", xl: "30px 20px" }}
            justifyContent={"space-between"}
          >
            <Typography
              sx={{ textAlign: "center" }}
              marginLeft={{ xl: "30px" }}
              variant="h3"
            >
              Лента новостей
            </Typography>
            <Button
              variant="contained"
              sx={{ width: { xs: "100%", sm: "auto" }, mt: "10px" }}
            >
              <Typography
                variant="body1"
                mr={"5px"}
                onClick={() => navigate(`/news`)}
              >
                Узнать все новости
              </Typography>
              <svg
                width="12"
                height="12"
                viewBox="0 0 9 17"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M1.70508 1.23977L7.68175 7.21643C8.38758 7.92227 8.38758 9.07727 7.68175 9.7831L1.70508 15.7598"
                  stroke={mainTheme.palette.primary.contrastText}
                  stroke-width="1.5"
                  stroke-miterlimit="10"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </Button>
          </Stack>
        </Container>
      </Box>
      <Box mt={{ xs: "10px", sm: "15px", md: "20px" }}>
        {loading ? (
          <CircularProgress
            size={50}
            sx={{ margin: "0 auto", display: "block" }}
          />
        ) : upMd ? (
          <NewsStack newsData={newsData} />
        ) : (
          <NewsSlider newsData={newsData} />
        )}
      </Box>
    </Box>
  );
}
