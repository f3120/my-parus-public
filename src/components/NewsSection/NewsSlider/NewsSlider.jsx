import { Box, Container } from "@mui/material";
import React from "react";
import Slider from "react-slick";
import NewsItem from "../NewsItem/NewsItem";
import "./NewsSlider.css";

export default function NewsSlider({ newsData = [] }) {
  const settings = {
    dots: false,
    infinite: newsData.length === 1 ? false : true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    centerMode: true,
    centerPadding: "20px",
    className: "news-slider",
  };

  return (
    <Slider {...settings}>
      {newsData.length > 0 &&
        newsData.map((item) => <NewsItem newsItemData={item} />)}
    </Slider>
  );
}
