import { Box, Container, Stack } from "@mui/material";
import React from "react";
import NewsItem from "../NewsItem/NewsItem";

export default function NewsStack({ newsData = [] }) {
  return (
    <Container>
      <Stack direction={"column"} gap={"20px"}>
        {newsData.length > 0 &&
          newsData.map((item) => <NewsItem newsItemData={item} />)}
      </Stack>
    </Container>
  );
}
