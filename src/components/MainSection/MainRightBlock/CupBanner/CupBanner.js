import {
  Box,
  Button,
  CircularProgress,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useContext, useEffect, useRef } from "react";
import { mainTheme } from "../../../../config/MUI/mainTheme";
import { navigate } from "gatsby";
import { useLazyLoad } from "../../../../hooks/useLazyLoad";

export default function CupBanner({ banner, loading }) {
  const upSm = useMediaQuery(mainTheme.breakpoints.up("sm"));
  const upMd = useMediaQuery(mainTheme.breakpoints.up("md"));
  const upLg = useMediaQuery(mainTheme.breakpoints.up("lg"));

  const { firstRowBannerSet, linkbanner } = banner || {};

  const image = upLg
    ? firstRowBannerSet?.photoMax
    : upMd
    ? firstRowBannerSet?.photo1200
    : upSm
    ? firstRowBannerSet?.photo900
    : firstRowBannerSet?.photo600;

  const [currentImage, loadingImageStatus] = useLazyLoad(
    Array.isArray(image?.srcSet) && image?.srcSet[0].sourceUrl,
    Array.isArray(image?.srcSet) &&
      image?.srcSet[image?.srcSet.length - 1].sourceUrl
  );

  return (
    <Stack
      direction={{ xs: "column", md: "row" }}
      sx={{
        borderRadius: "20px",
        overflow: "hidden",
        backgroundColor: "primary.main",
        cursor: "pointer",
        width: "100%",
      }}
      height={{
        xs: "230px",
        sm: "240px",
        md: "200px",
        lg: "265px",
        xl: "290px",
      }}
      onClick={() => {
        navigate(linkbanner);
      }}
    >
      {loading ? (
        <Stack
          height={"100%"}
          width={"100%"}
          alignItems={"center"}
          justifyContent={"center"}
          sx={{ color: "primary.white" }}
        >
          <CircularProgress size={50} color="inherit" />
        </Stack>
      ) : (
        <img
          // loading="lazy"
          src={currentImage}
          alt="TeamCup"
          style={{
            filter: `${loadingImageStatus ? "blur(20px)" : ""}`,
            height: "100%",
            width: " 100%",
            objectFit: "cover",
            objectPosition: "сenter",
          }}
        />
      )}
    </Stack>
  );
}
