import { Box } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React, { useEffect, useRef } from "react";
import CupBanner from "./CupBanner/CupBanner";
import ClubEvents from "./ClubEvents/ClubEvents";
import HowToGuid from "./HowToGuide/HowToGuid";

export default function MainRightBlock({ banners, loading }) {
  return (
    <Box>
      <Grid2
        container
        rowSpacing={{ xs: "10px", lg: "20px" }}
        columnSpacing={{ xs: "10px", lg: "20px" }}
      >
        <Grid2 xs={12} sm={12}>
          <CupBanner
            banner={banners.find(
              (banner) =>
                banner?.categories &&
                banner?.categories.includes("mainrightfirstrow")
            )}
            loading={loading}
          />
        </Grid2>
        <Grid2 xs={4} display={{ xs: "none", md: "flex" }}>
          <ClubEvents />
        </Grid2>
        <Grid2 xs={8} display={{ xs: "none", md: "flex" }}>
          <HowToGuid
            banner={banners.find(
              (banner) =>
                banner?.categories &&
                banner?.categories.includes("mainrightsecondrow")
            )}
            loading={loading}
          />
        </Grid2>
      </Grid2>
    </Box>
  );
}
