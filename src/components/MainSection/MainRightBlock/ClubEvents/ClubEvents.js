import { Box, Typography } from "@mui/material";
import React, { useContext } from "react";
import waves from "../../../../media/icons/WavesToEventCard.svg";
import { ModalContext } from "../../../../context/ModalContext";
import { navigate } from "gatsby";

const roundStyle = {
  position: "absolute",
  width: "200px",
  height: "200px",
  borderRadius: "100%",
  borderWidth: "1px",
  borderStyle: "solid",
  borderColor: "primary.contrastText",
};
export default function ClubEvents() {
  const { setModalActive } = useContext(ModalContext);
  return (
    <Box
      onClick={() => navigate("/news")}
      bgcolor={"primary.main"}
      position={"relative"}
      width={"100%"}
      height={{ md: "190px", lg: "265px", xl: "290px" }}
      sx={{
        borderRadius: "15px",
        display: "flex",
        justifyContent: "center",
        overflow: "hidden",
        cursor: "pointer",
      }}
    >
      <Box
        sx={{
          ...roundStyle,
          top: { xs: -60, lg: -33 },
          left: { xs: -30, lg: -50 },
        }}
      ></Box>
      <Box
        sx={{
          ...roundStyle,
          bottom: { xs: -90, lg: -17, xl: 10 },
          right: { xs: -120, lg: -89, xl: -50 },
        }}
      ></Box>
      <Box
        sx={{
          position: "absolute",
          lineHeight: 0,
          left: 0,
          bottom: -1,
          width: { xs: "100px", lg: "150px", xl: "190px" },
        }}
      >
        <img width={"100%"} src={waves} alt="clubEvents" />
      </Box>
      <Typography
        variant="h5"
        textAlign={"center"}
        mt={{ xs: "51px", lg: "48px", xl: "70px" }}
      >
        События
        <br />
        клуба
      </Typography>
    </Box>
  );
}
