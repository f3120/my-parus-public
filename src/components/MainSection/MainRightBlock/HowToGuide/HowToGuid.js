import {
  Box,
  Button,
  CircularProgress,
  Stack,
  Typography,
} from "@mui/material";
import React, { useContext } from "react";
import arrow45 from "../../../../media/icons/Arrow45deg.svg";
import dog from "../../../../media/icons/Dog.svg";
import ArrowButton from "../../../ArrowButton/ArrowButton";
import { ModalContext } from "../../../../context/ModalContext";
import { useLazyLoad } from "../../../../hooks/useLazyLoad";
import { navigate } from "gatsby";

export default function HowToGuid({ banner, loading }) {
  const { id, shorttitle, title, description, setting, srcSet, linkbanner } =
    banner || {};

  const [currentImage, loadingImageStatus] = useLazyLoad(
    Array.isArray(srcSet) && srcSet[0].sourceUrl,
    Array.isArray(srcSet) && srcSet[srcSet.length - 1].sourceUrl
  );

  console.log(banner);

  if (loading) {
    return (
      <Box
        position={"relative"}
        width={"100%"}
        height={{ md: "190px", lg: "265px", xl: "290px" }}
        sx={{
          borderRadius: "15px",
          display: "flex",
          justifyContent: "center",
          overflow: "hidden",
          border: "2px solid",
          borderColor: "primary.main",
          p: { xs: "15px" },
          cursor: "pointer",
        }}
      >
        <Stack
          height={"100%"}
          width={"100%"}
          alignItems={"center"}
          justifyContent={"center"}
          sx={{ color: "primary.main" }}
        >
          <CircularProgress size={50} color="inherit" />
        </Stack>
      </Box>
    );
  }

  return (
    <Box
      onClick={() => navigate(linkbanner)}
      position={"relative"}
      width={"100%"}
      height={{ md: "190px", lg: "265px", xl: "290px" }}
      sx={{
        borderRadius: "15px",
        display: "flex",
        justifyContent: "center",
        overflow: "hidden",
        border: "2px solid",
        borderColor: "primary.main",
        p: { xs: "15px" },
        cursor: "pointer",
      }}
    >
      <img
        src={currentImage}
        alt="Background"
        style={{
          objectFit: "cover",
          filter: loadingImageStatus ? "blur(10px)" : "none",
          width: "100%",
          height: "100%",
          position: "absolute",
          top: 0,
          left: 0,
        }}
      />
      <Stack
        direction={"column"}
        justifyContent={"space-between"}
        width={"100%"}
        zIndex={1}
      >
        <Box width={"100%"}>
          <Typography variant="h4" textAlign={"start"} color={"primary.main"}>
            {title}
          </Typography>
          <Typography
            variant="body1"
            textAlign={"start"}
            color={"primary.main"}
            display={{ xs: "none", lg: "block" }}
            mt={"5px"}
            width={"60%"}
          >
            {description}
          </Typography>
        </Box>
        <ArrowButton />
      </Stack>
    </Box>
  );
}
