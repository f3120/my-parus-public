import {
  Box,
  Button,
  CircularProgress,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useContext } from "react";
import shop from "../../../media/images/Shop.png";
import arrow45 from "../../../media/icons/Arrow45deg.svg";
import ArrowButton from "../../ArrowButton/ArrowButton";
import { mainTheme } from "../../../config/MUI/mainTheme";
import { ModalContext } from "../../../context/ModalContext";
import { navigate } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";
import { useLazyLoad } from "../../../hooks/useLazyLoad";

export default function MainBanner({ banners, loading }) {
  const mainBanner = banners.find(
    (banner) =>
      banner?.categories && banner?.categories.includes("maindownbanner")
  );
  const {
    id,
    shorttitle,
    title,
    description,
    setting,
    srcSet,
    linkbanner,
    loadingSlide,
  } = mainBanner || {};

  const [currentImage, loadingImageStatus] = useLazyLoad(
    Array.isArray(srcSet) && srcSet[0].sourceUrl,
    Array.isArray(srcSet) && srcSet[srcSet.length - 1].sourceUrl
  );

  const upSm = useMediaQuery(mainTheme.breakpoints.up("sm"));

  if (loading) {
    return (
      <Box
        sx={{
          borderRadius: "20px",
          overflow: "hidden",
          width: "100%",
          height: { xs: "150px", md: "200px", lg: "300px" },
          position: "relative",
          zIndex: 1,
          p: { xs: "10px", sm: "15px", lg: "20px", xl: "20px 35px" },
          cursor: "pointer",
          border: "2px solid primary.main",
        }}
      >
        <Stack
          height={"100%"}
          width={"100%"}
          alignItems={"center"}
          justifyContent={"center"}
          sx={{ color: "primary.main" }}
        >
          <CircularProgress size={50} color="inherit" />
        </Stack>
      </Box>
    );
  }

  return (
    <Box
      onClick={() => navigate(linkbanner)}
      sx={{
        borderRadius: "20px",
        overflow: "hidden",
        width: "100%",
        height: { xs: "150px", md: "200px", lg: "300px" },
        position: "relative",
        zIndex: 1,
        p: { xs: "10px", sm: "15px", lg: "20px", xl: "20px 35px" },
        cursor: "pointer",
      }}
    >
      <img
        src={currentImage}
        alt="Background"
        style={{
          objectFit: "cover",
          filter: loadingImageStatus ? "blur(10px)" : "none",
          width: "100%",
          height: "100%",
          position: "absolute",
          top: 0,
          left: 0,
        }}
      />
      <Box
        sx={{
          position: "absolute",
          width: "100%",
          height: "100%",
          top: 0,
          left: 0,
          padding: { xs: "10px", sm: "15px", lg: "20px", xl: "20px 35px" },
        }}
      >
        <Stack
          direction={"row"}
          justifyContent={"space-between"}
          height={"100%"}
        >
          <Stack
            direction={"column"}
            justifyContent={{ xs: "flex-end", sm: "center" }}
          >
            {/* <Typography
              variant="h3"
              color={"primary.contrastText"}
              display={{ xs: "inline-block", sm: "none" }}
            >
              Магазин
            </Typography>
            <Typography
              variant="h3"
              color={"primary.contrastText"}
              display={{ xs: "none", sm: "inline-block" }}
            >
              {upLg ? "Лимитированная коллекция" : "Коллекция"}
              <br />
              клуба Мой Парус
            </Typography> */}
            <Typography variant="h3" color={"primary.contrastText"}>
              {upSm ? title : shorttitle}
            </Typography>
            <Typography
              variant="body1"
              color={"primary.contrastText"}
              display={{ xs: "none", sm: "inline-block" }}
              width={"70%"}
            >
              {description}
            </Typography>
          </Stack>
          <Stack direction={"column"} justifyContent={"flex-end"}>
            <ArrowButton variant="outlined" />
          </Stack>
        </Stack>
      </Box>
    </Box>
  );
}
