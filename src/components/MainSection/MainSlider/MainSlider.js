import {
  Box,
  Button,
  CircularProgress,
  Stack,
  Typography,
} from "@mui/material";
import React, { createRef, useContext, useEffect, useRef } from "react";
import Slider from "react-slick";
import "./MainSlider.css";
import mainSlide_1 from "../../../media/images/MainSlider_1.png";
import mainSlide_2 from "../../../media/images/MainSlider_2.png";
import arrow45, {
  ReactComponent as Arrow45,
} from "../../../media/icons/Arrow45deg.svg";
import { ModalContext } from "../../../context/ModalContext";
import ArrowButton from "../../ArrowButton/ArrowButton";
import { StaticImage } from "gatsby-plugin-image";
import { useLazyLoad } from "../../../hooks/useLazyLoad";

export default function MainSlider({ servicesBlockRef, slides }) {
  const [currentSlide, setCurrentSlide] = React.useState(0);

  let sliderRef = useRef(null);

  const settings = {
    pauseOnHover: true,
    autoplay: true,
    autoplaySpeed: 3000,
    dots: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    className: "main-slider",
    afterChange: (curSlide) => {
      setCurrentSlide(curSlide);
    },
  };

  const slideStyle = {
    height: { xs: "240px", md: "400px", lg: "550px", xl: "600px" },
    borderRadius: "20px",
    position: "relative",
    overflow: "hidden",
  };

  const dotsStyle = {
    width: "100%",
    height: "4px",
    borderRadius: "20px",
    transition: "all 0.2s ease",
  };

  const dotsWrapperStyle = {
    width: 65 / (slides.length + 1) + "%",
    maxWidth: "15%",
    p: "5px 0",
    cursor: "pointer",
  };

  return (
    <Box position={"relative"}>
      <Slider
        {...settings}
        infinite={slides.length > 0}
        ref={(slider) => {
          sliderRef = slider;
        }}
      >
        <Slide1 slideStyle={slideStyle} servicesBlockRef={servicesBlockRef} />
        {/* <Slide2 slideStyle={slideStyle} servicesBlockRef={servicesBlockRef} /> */}
        {slides.map((slide) => (
          <Slide key={slide.id} slideData={slide} />
        ))}
      </Slider>
      <Box
        position={"absolute"}
        sx={{
          bottom: { xs: "5px", md: "10px" },
          width: "100%",
          // display: { xs: "flex", md: "none" },
          justifyContent: "center",
          zIndex: 1,
        }}
      >
        <Stack
          direction={"row"}
          justifyContent={"center"}
          gap={"7px"}
          width={"100%"}
        >
          {["fstSlide", ...slides].map((item, index) => (
            <Box
              onClick={() => {
                sliderRef.slickGoTo(index);
              }}
              sx={{
                ...dotsWrapperStyle,
              }}
            >
              <Box
                sx={{
                  ...dotsStyle,
                  backgroundColor:
                    currentSlide === index ? "primary.main" : "primary.white",
                }}
              ></Box>
            </Box>
          ))}
        </Stack>
      </Box>
    </Box>
  );
}

function Slide1({ slideStyle, servicesBlockRef }) {
  let buttonRef = useRef(null);
  const { setModalActive } = useContext(ModalContext);

  return (
    <Box
      sx={{
        ...slideStyle,
      }}
    >
      <StaticImage
        src="../../../media/images/MainSlider_1.png"
        alt="Background"
        placeholder="blurred"
        transformOptions={{ fit: "cover" }}
        loading="lazy"
        style={{
          minHeight: "100%",
          zIndex: -1,
        }}
      />
      <Stack
        sx={{
          position: "absolute",
          top: { xs: "10px", sm: "15px", lg: "30px", xl: "40px" },
          right: { xs: "10px", sm: "15px", lg: "30px", xl: "34px" },
        }}
        direction={"row"}
      >
        <Box
          bgcolor={"primary.main"}
          display={"flex"}
          alignItems={"center"}
          justifyContent={"center"}
          sx={{ borderRadius: "30px", padding: "7px 12px" }}
          ref={buttonRef}
        >
          <Typography variant="body1">Наши услуги</Typography>
        </Box>

        <ArrowButton
          onClick={() =>
            servicesBlockRef?.current &&
            servicesBlockRef.current.scrollIntoView({
              behavior: "smooth",
              block: "center",
            })
          }
        />
      </Stack>
      <Stack
        sx={{
          position: "absolute",
          bottom: { xs: "20px", sm: "30px", md: "45px", xl: "75px" },
          left: { xs: "10px", sm: "15px", lg: "20px", xl: "40px" },
        }}
        alignItems={"flex-start"}
      >
        <Typography variant="h2">Мой парус</Typography>
        <Typography variant="body1">
          Интересно и безопасно знакомим
          <br />с миром паруса и моря!
        </Typography>
        <Button variant="outlined" sx={{ mt: { xs: "10px" } }}>
          <Typography
            variant="body1"
            onClick={() => setModalActive("scheduleModal")}
          >
            Записаться
          </Typography>
        </Button>
      </Stack>
    </Box>
  );
}

function Slide2({ slideStyle }) {
  return (
    <Box
      sx={{
        ...slideStyle,
        backgroundPosition: "100% 75%",
      }}
    >
      <StaticImage
        src="../../../media/images/MainSlider_2.png"
        alt="Background"
        placeholder="blurred"
        transformOptions={{ fit: "cover" }}
        loading="lazy"
        style={{
          minHeight: "100%",
          zIndex: -1,
        }}
      />
      <Button
        variant="outlined"
        sx={{
          pointerEvents: "none",
          position: "absolute",
          top: { xs: "10px", lg: "20px" },
          left: { xs: "10px", lg: "20px" },
        }}
      >
        <Typography variant="body1">Инструкторы</Typography>
      </Button>
      <Stack
        sx={{
          position: "absolute",
          bottom: { xs: "20px", sm: "30px", md: "45px", xl: "75px" },
          left: { xs: "10px", sm: "15px", lg: "20px", xl: "40px" },
        }}
        alignItems={"flex-start"}
      >
        <Typography variant="h4" color={"primary.contrastText"}>
          Профессионализм
        </Typography>
        <Typography variant="body1">
          Инструкторы с высшим
          <br />и специальным образованием
        </Typography>
      </Stack>
    </Box>
  );
}

function Slide({ slideData }) {
  const {
    id,
    title,
    shorttitle,
    description,
    setting,
    srcSet,
    linkbanner,
    loadingSlide,
  } = slideData || {};

  const [currentImage, loadingImageStatus] = useLazyLoad(
    Array.isArray(srcSet) && srcSet[0].sourceUrl,
    Array.isArray(srcSet) && srcSet[srcSet.length - 1].sourceUrl
  );

  if (loadingSlide) {
    return (
      <Box
        sx={{
          height: { xs: "240px", md: "400px", lg: "550px", xl: "600px" },
          borderRadius: "20px",
          position: "relative",
          overflow: "hidden",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <CircularProgress size={70} />
      </Box>
    );
  }

  return (
    <Box
      sx={{
        height: { xs: "240px", md: "400px", lg: "550px", xl: "600px" },
        borderRadius: "20px",
        position: "relative",
        overflow: "hidden",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          width: "100%",
          height: "100%",
          background:
            "linear-gradient(180deg, rgba(6, 11, 17, 0) 0%, rgba(6, 11, 17, 0.125) 50%, rgba(6, 11, 17, 0.7) 100%)",
        }}
      ></Box>
      <img
        src={currentImage}
        alt="Background"
        style={{
          objectFit: "cover",
          filter: loadingImageStatus ? "blur(10px)" : "none",
          width: "100%",
          height: "100%",
          position: "absolute",
          zIndex: -1,
        }}
      />
      <Button
        variant="outlined"
        sx={{
          pointerEvents: "none",
          position: "absolute",
          top: { xs: "10px", lg: "20px" },
          left: { xs: "10px", lg: "20px" },
        }}
      >
        <Typography variant="body1">{shorttitle}</Typography>
      </Button>
      <Stack
        sx={{
          position: "absolute",
          bottom: { xs: "20px", sm: "30px", md: "45px", xl: "75px" },
          left: { xs: "10px", sm: "15px", lg: "20px", xl: "40px" },
        }}
        alignItems={"flex-start"}
      >
        <Typography variant="h4" color={"primary.contrastText"}>
          {title}
        </Typography>
        <Typography variant="body1">{description}</Typography>
      </Stack>
    </Box>
  );
}
