import { Box, Container } from "@mui/material";
import React, { useEffect } from "react";
import MainSlider from "./MainSlider/MainSlider";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import MainRightBlock from "./MainRightBlock/MainRightBlock";
import MainBanner from "./MainBanner/MainBanner";
import { gql, useQuery } from "@apollo/client";
import { convertSrcSet, getSrcSetObject } from "../../tools/tools";

const APOLLO_QUERY = gql`
  query MainSectionBanners {
    posts(where: { categoryName: "mainbanner-banner" }, first: 1000) {
      edges {
        node {
          id
          clubfield {
            clubname
          }
          bannerfield {
            description
            setting
            title
            shorttitle
            linkbanner
            photo {
              srcSet
            }
          }
          banner_main_first_row {
            fieldGroupName
            photo1200 {
              sourceUrl
              srcSet
            }
            photo600 {
              sourceUrl
              srcSet
            }
            photo900 {
              srcSet
              sourceUrl
            }
            photoMax {
              sourceUrl
              srcSet
            }
          }
          categories {
            nodes {
              slug
            }
          }
        }
      }
    }
  }
`;

export default function MainSection({ servicesBlockRef }) {
  const { data, loading, error } = useQuery(APOLLO_QUERY);

  const [banners, setBanners] = React.useState([
    { id: "loading", loadingSlide: true },
  ]);

  useEffect(() => {
    if (!loading && !error && Array.isArray(data?.posts?.edges)) {
      setBanners(
        data?.posts?.edges.map((node) => ({
          id: node.node.id,
          title: node.node.bannerfield.title,
          shorttitle: node.node.bannerfield.shorttitle,
          description: node.node.bannerfield.description,
          setting: node.node.bannerfield.setting,
          srcSet: convertSrcSet(node.node.bannerfield.photo?.srcSet),
          firstRowBannerSet: node.node.banner_main_first_row
            ? {
                photo1200: {
                  srcSet: convertSrcSet(
                    node.node.banner_main_first_row?.photo1200?.srcSet
                  ),
                },
                photo600: {
                  srcSet: convertSrcSet(
                    node.node.banner_main_first_row?.photo600?.srcSet
                  ),
                },
                photo900: {
                  srcSet: convertSrcSet(
                    node.node.banner_main_first_row?.photo900?.srcSet
                  ),
                },
                photoMax: {
                  srcSet: convertSrcSet(
                    node.node.banner_main_first_row?.photoMax?.srcSet
                  ),
                },
              }
            : null,
          linkbanner: node.node.bannerfield.linkbanner,
          categories: node.node.categories.nodes.map((node) => node.slug),
        }))
      );
    }
  }, [data, loading, error]);

  return (
    <Box mt={"10px"} width={"100%"}>
      <Container>
        <Grid2
          container
          rowSpacing={{ xs: "20px", sm: "10px", md: "20px", lg: "40px" }}
          columnSpacing={{ xs: 0, sm: "10px", lg: "20px" }}
        >
          <Grid2 xs={12} sm={8} md={6}>
            <MainSlider
              servicesBlockRef={servicesBlockRef}
              slides={banners.filter(
                (banner) =>
                  banner?.categories && banner.categories.includes("slider")
              )}
            />
          </Grid2>
          <Grid2 xs={12} sm={4} md={6}>
            <MainRightBlock banners={banners} loading={loading} />
          </Grid2>
          <Grid2 xs={12} sm={12}>
            <MainBanner banners={banners} loading={loading} />
          </Grid2>
        </Grid2>
      </Container>
    </Box>
  );
}
