import { Box, Stack, Typography } from "@mui/material";
import dayjs from "dayjs";
import React from "react";
import str from "../../../media/icons/arrow-right.svg";
import { useTheme } from "@mui/material/styles";
import { navigate } from "gatsby";
export default function EventsCard({ eventData }) {
  console.log({ eventData });
  const theme = useTheme();
  const {
    date,
    title,
    id,
    description,
    background,
    check,
    link,
    dateStart,
    dateEnd,
    age,
    status,
  } = eventData;

  // let eventStatus;
  // let colorEventStatus;

  // if (status === "waiting") {
  //   eventStatus = "Ожидает начала";
  //   colorEventStatus = theme.palette.secondary.gray;
  // } else if (status === "open") {
  //   eventStatus = "Введется набор команд";
  //   colorEventStatus = theme.palette.secondary.green;
  // } else if (status === "closed") {
  //   eventStatus = "Набор закрыт";
  //   colorEventStatus = theme.palette.secondary.red;
  // } else {
  //   eventStatus = status;
  // }

  return (
    <Stack
      sx={{
        boxShadow: "0 0 20px 5px rgba(50, 79, 123, 0.25)",

        borderRadius: "25px",

        direction: "column",
      }}
    >
      <Stack
        sx={{
          height: { xs: "130px", sm: "150px", md: "150px", lg: "200px" },
          backgroundImage: `url(${background})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          borderRadius: "25px 25px 0 0",
          padding: "10px",
        }}
        direction={"column"}
        justifyContent={"space-between"}
      >
        <Stack justifyContent={"end"} direction="row" flexWrap="wrap">
          <Stack
            backgroundColor={"primary.white"}
            p={"8px 12px 8px 12px"}
            borderRadius={"30px"}
          >
            <Typography variant="h6" color={statusMap.get(status)?.color}>
              {statusMap.get(status)?.title}
            </Typography>
          </Stack>
        </Stack>
        <Stack justifyContent={"start"} direction="row" flexWrap="wrap">
          <Stack
            backgroundColor={"primary.white"}
            p={"8px 12px 8px 12px"}
            borderRadius={"30px"}
          >
            <Typography color={"primary.main"}>{age}</Typography>
          </Stack>
        </Stack>
      </Stack>
      <Stack
        sx={{
          overflow: "hidden",
        }}
        height={{
          xs: "230px",
          sm: "230px",
          md: "250px",
          lg: "270px",
          xl: "300px",
        }}
        padding={{ xs: "10px", xl: "20px" }}
        direction={"column"}
        justifyContent={"space-between"}
      >
        <Typography color={"primary.main"} variant="h5">
          {title}
        </Typography>

        <Stack direction={"column"}>
          <Stack
            alignItems={"center"}
            gap={"5px"}
            direction={"row"}
            marginBottom={"10px"}
          >
            <Typography variant="h6" color={"primary.main"}>
              {dateStart}
            </Typography>

            <Box width={"16px"} height={"16px"}>
              <img width="100%" src={str} alt="clubEvents" />
            </Box>

            <Typography variant="h6" color={"primary.main"}>
              {dateEnd}
            </Typography>
          </Stack>
          <Box
            sx={{
              marginTop: "auto",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "43px",
              borderRadius: "30px",
              border: "1px solid #FFFFFF ",
              cursor: "pointer",
              backgroundColor: "primary.main",
            }}
            onClick={() => navigate(`/event/?ID=${id}`)}
          >
            <Typography color={"primary.white"}>Подробнее</Typography>
          </Box>
        </Stack>
      </Stack>
    </Stack>
  );
}

const statusMap = new Map([
  ["waiting", { title: "Ожидает начала", color: "secondary.gray" }],
  ["open", { title: "Введется набор команд", color: "secondary.green" }],
  ["closed", { title: "Набор закрыт", color: "secondary.red" }],
]);
