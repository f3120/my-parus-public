import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React from "react";
import EventsCard from "../EventsCard/EventsCard";

export default function EventsList({ filteredData }) {
  return (
    <Grid2 marginTop={"50px"} spacing={"20px"} container>
      {filteredData?.map((item) => (
        <Grid2 xs={12} sm={6} md={4} xl={3}>
          <EventsCard eventData={item} />
        </Grid2>
      ))}
    </Grid2>
  );
}
