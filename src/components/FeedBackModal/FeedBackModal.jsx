import React, { useContext } from "react";
import { ModalContext } from "../../context/ModalContext";
import {
  Alert,
  Button,
  Checkbox,
  Dialog,
  FormControl,
  MenuItem,
  Select,
  Snackbar,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { mainTheme } from "../../config/MUI/mainTheme";
import { Link } from "gatsby";
import emailjs from "@emailjs/browser";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import dayjs from "dayjs";
import "dayjs/locale/ru";
import { ruRU } from "@mui/x-date-pickers/locales";
import { citiesData } from "../../config/data/citiesData";

const modalID = "feedback";
export default function FeedBackModal() {
  const { isModalActive, setModalActive, setModalInactive } =
    useContext(ModalContext);
  const [formData, setFormData] = React.useState({
    name: undefined,
    email: undefined,
    phone: undefined,
    parentName: undefined,
    level: undefined,
    location: undefined,
    message: undefined,
    agree: false,
    date: undefined,
  });

  const handleSubmit = () => {
    emailjs.send(
      "service_d6lm83x",
      "template_9z4he3m",
      formData,
      "aPHzbaZ9nf1dYaLxr"
    );
    setFormData({
      name: undefined,
      email: undefined,
      phone: undefined,
      parentName: undefined,
      level: undefined,
      location: undefined,
      message: undefined,
      agree: false,
      date: undefined,
    });

    setModalInactive(modalID);
    setModalActive("snackbarFeedBack");
  };

  return (
    <Dialog
      fullWidth
      maxWidth={"md"}
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "15px",
        },
      }}
    >
      <Stack
        direction={"column"}
        padding={{ xs: "10px", sm: "15px" }}
        justifyContent={"space-between"}
        gap={"15px"}
      >
        <Stack
          direction={"column"}
          p={"10px"}
          sx={{
            backgroundColor: "secondary.main",
            borderRadius: "15px",
          }}
        >
          <Typography variant="h4">Напишите нам</Typography>
          <Typography variant="caption" color={"primary.main"}>
            Какой уровень занятий Вас интересует и в каком из клубов?
            <br />
            Если еще не определились, смело оставляйте эти поля пустыми.
            <br />
            Мы обязательно свяжемся с Вами и поможем определиться!
          </Typography>
        </Stack>
        <Stack direction={"column"} gap={"10px"}>
          <TextField
            error={formData.name === ""}
            label="Имя и фамилия занимающегося"
            required
            variant="filled"
            fullWidth
            size="small"
            value={formData.name}
            onChange={(e) => setFormData({ ...formData, name: e.target.value })}
          />
          <TextField
            error={formData.email === ""}
            required
            label="E-mail"
            variant="filled"
            fullWidth
            size="small"
            value={formData.email}
            onChange={(e) =>
              setFormData({ ...formData, email: e.target.value })
            }
          />
          <TextField
            error={formData.phone === ""}
            required
            label="Телефон"
            variant="filled"
            fullWidth
            size="small"
            value={formData.phone}
            onChange={(e) =>
              setFormData({ ...formData, phone: e.target.value })
            }
          />
          <TextField
            label="ФИО родителя, если занимается ребенок"
            variant="filled"
            fullWidth
            size="small"
            value={formData.parentName}
            onChange={(e) =>
              setFormData({ ...formData, parentName: e.target.value })
            }
          />
          <FormControl fullWidth variant={"filled"}>
            <Select
              disableUnderline={true}
              MenuProps={{ PaperProps: { sx: { borderRadius: "15px" } } }}
              IconComponent={KeyboardArrowDownIcon}
              defaultValue={-1}
              onChange={(e) =>
                setFormData({ ...formData, level: e.target.value })
              }
              sx={{
                "& .MuiSelect-select": {
                  paddingTop: { xs: "10px", sm: "15px" },
                  paddingBottom: { xs: "10px", sm: "15px" },
                },
              }}
            >
              <MenuItem value={-1} disabled>
                <Typography variant="body1" color={"primary.main"}>
                  Выберите уровень
                </Typography>
              </MenuItem>
              <MenuItem value={"OptiFUN"}>
                <Typography variant="body1" color={"primary.main"}>
                  OptiFUN
                </Typography>
              </MenuItem>
              <MenuItem value={"OptiSAIL"}>
                <Typography variant="body1" color={"primary.main"}>
                  OptiSAIL
                </Typography>
              </MenuItem>
              <MenuItem value={"OptiRACE"}>
                <Typography variant="body1" color={"primary.main"}>
                  OptiRACE
                </Typography>
              </MenuItem>
              <MenuItem value={"LaserFUN"}>
                <Typography variant="body1" color={"primary.main"}>
                  LaserFUN
                </Typography>
              </MenuItem>
              <MenuItem value={"LaserSAIL"}>
                <Typography variant="body1" color={"primary.main"}>
                  LaserSAIL
                </Typography>
              </MenuItem>
              <MenuItem value={"LaserRACE"}>
                <Typography variant="body1" color={"primary.main"}>
                  LaserRACE
                </Typography>
              </MenuItem>
            </Select>
          </FormControl>
          <FormControl fullWidth variant={"filled"}>
            <Select
              disableUnderline={true}
              MenuProps={{ PaperProps: { sx: { borderRadius: "15px" } } }}
              IconComponent={KeyboardArrowDownIcon}
              defaultValue={-1}
              onChange={(e) =>
                setFormData({ ...formData, location: e.target.value })
              }
              sx={{
                "& .MuiSelect-select": {
                  paddingTop: { xs: "10px", sm: "15px" },
                  paddingBottom: { xs: "10px", sm: "15px" },
                },
              }}
            >
              <MenuItem value={-1} disabled>
                <Typography variant="body1" color={"primary.main"}>
                  Выберите место
                </Typography>
              </MenuItem>
              {citiesData.map((city) => (
                <MenuItem value={city.id}>
                  <Typography
                    variant="body1"
                    color={"primary.main"}
                    textAlign={"center"}
                  >
                    {city.name}
                  </Typography>
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <LocalizationProvider
            dateAdapter={AdapterDayjs}
            adapterLocale="ru"
            localeText={
              ruRU.components.MuiLocalizationProvider.defaultProps.localeText
            }
          >
            <DatePicker
              sx={{
                backgroundColor: mainTheme.palette.secondary.main,
                color: mainTheme.palette.primary.main,
                borderRadius: "30px",
                "& .MuiInputBase-root": {
                  borderRadius: "30px",
                  "& .MuiOutlinedInput-notchedOutline": {
                    border: "none",
                  },
                },
                "& .MuiInputBase-input": {
                  borderRadius: "30px",
                  color: mainTheme.palette.primary.main,
                },
                "& .MuiInputLabel-root": {
                  color: mainTheme.palette.primary.main,
                  top: "-5px",
                },
              }}
              label="Выберите желаемую дату"
              onChange={(date) =>
                setFormData({
                  ...formData,
                  date: date ? date.format("DD.MM.YYYY") : null,
                })
              }
            />
          </LocalizationProvider>

          <TextField
            label="Комментарий"
            multiline
            sx={{
              "& .MuiInputBase-multiline": {
                backgroundColor: mainTheme.palette.secondary.main,
                "&.Mui-focused": {
                  backgroundColor: mainTheme.palette.secondary.main,
                },
                "&:hover": {
                  backgroundColor: mainTheme.palette.secondary.main,
                },
              },
            }}
            variant="filled"
            fullWidth
            size="small"
            value={formData.comment}
            onChange={(e) =>
              setFormData({ ...formData, comment: e.target.value })
            }
          />
        </Stack>
        <Stack
          direction={"row"}
          justifyContent={"center"}
          alignItems={"center"}
          width={"100%"}
        >
          <Checkbox
            checked={formData.agree}
            onChange={(e) =>
              setFormData({ ...formData, agree: e.target.checked })
            }
          />
          <Typography variant="body1" color={"primary.main"}>
            Я согласен на обработку{" "}
            <Typography
              color={"primary.main"}
              component={"span"}
              onClick={() => setModalActive("privacyPolicy")}
              sx={{ textDecoration: "underline", cursor: "pointer" }}
            >
              персональных данных
            </Typography>{" "}
            и с условиями{" "}
            <Typography
              component={"span"}
              color={"primary.main"}
              onClick={() => setModalActive("userAgreement")}
              sx={{ textDecoration: "underline", cursor: "pointer" }}
            >
              пользовательского соглашения
            </Typography>
          </Typography>
        </Stack>
        <Button
          onClick={handleSubmit}
          variant="contained"
          disabled={
            !formData.agree ||
            !formData.name ||
            !formData.phone ||
            !formData.email
          }
        >
          Отправить
        </Button>
      </Stack>
    </Dialog>
  );
}
