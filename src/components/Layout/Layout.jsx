import { ThemeProvider } from "@emotion/react";
import React, { useContext, useEffect } from "react";
import { mainTheme } from "../../config/MUI/mainTheme";
import {
  Alert,
  Box,
  Container,
  Snackbar,
  Stack,
  Typography,
} from "@mui/material";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { ReactComponent as Chat } from "../../media/icons/Chat.svg";
import { ModalContext } from "../../context/ModalContext";
import ShopHeader from "../Shop/ShopHeader/ShopHeader";
import FeedBackModal from "../FeedBackModal/FeedBackModal";
import DevelopeCardModal from "../DevelopeCardModal/DevelopeCardModal";
import ScheduleModal from "../ScheduleModal/ScheduleModal";

export default function Layout({
  children,
  sectionGap,
  sx = {},
  displayShopHeader,
  hideCart,
}) {
  const { isModalActive, setModalActive, setModalInactive } =
    useContext(ModalContext);

  let clubRegion =
    typeof window !== "undefined" && localStorage.getItem("clubRegion");
  const [clubRegionState, setClubRegionState] = React.useState(
    clubRegion || undefined
  );
  useEffect(() => {
    console.log({ window, clubRegionState });
    typeof window !== "undefined" &&
      localStorage.setItem("clubRegion", clubRegionState || "");
  }, [clubRegionState]);

  return (
    <Box
      display={"flex"}
      flexDirection={"column"}
      minHeight={"100vh"}
      position={"relative"}
      width={"100%"}
      sx={sx}
    >
      <Header
        clubRegion={clubRegionState}
        setClubRegionState={setClubRegionState}
      />
      {displayShopHeader && <ShopHeader hideCart={hideCart} />}
      <Stack
        direction={"column"}
        flexGrow={2}
        gap={sectionGap || { xs: "45px", sm: "50px", md: " 70px", xl: "80px" }}
        width={"100%"}
        mb={{ xs: "45px", sm: "50px", md: " 70px", xl: "80px" }}
      >
        {children}
      </Stack>
      <Box
        sx={{
          width: "100%",
          position: "fixed",
          bottom: "30px",
          zIndex: 1001,
          pointerEvents: "none",
        }}
      >
        <Container>
          <Stack direction={"row"} justifyContent={"flex-end"}>
            <Box
              sx={{
                width: "50px",
                height: "50px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "primary.main",
                borderRadius: "100%",
                cursor: "pointer",
                border: "2px solid white",
                pointerEvents: "auto",
              }}
              onClick={() => setModalActive("feedback")}
            >
              <Chat stroke={"white"} width={"60%"} height={"60%"} />
            </Box>
          </Stack>
        </Container>
      </Box>
      <Footer
        clubRegionState={clubRegionState}
        setClubRegionState={setClubRegionState}
      />
      <Snackbar
        open={isModalActive("snackbarFeedBack")}
        autoHideDuration={5000}
        onClose={(event, reason) => {
          if (reason === "clickaway") {
            return;
          }
          setModalInactive("snackbarFeedBack");
        }}
      >
        <Alert
          onClose={(event, reason) => {
            if (reason === "clickaway") {
              return;
            }
            setModalInactive("snackbarFeedBack");
          }}
          sx={{ width: "100%" }}
        >
          <Typography variant="body1" color={"primary.main"}>
            Сообщение отправлено!
          </Typography>
        </Alert>
      </Snackbar>
      <DevelopeCardModal />
      <ScheduleModal />
      <FeedBackModal />
    </Box>
  );
}
