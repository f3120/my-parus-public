import { Box, Stack, Typography } from "@mui/material";
import React from "react";

export default function WithItommria({ title, icon }) {
  return (
    <Stack
      margin={"5px"}
      borderRadius={"100px"}
      backgroundColor={"primary.white"}
      direction={"column"}
      alignItems={"center"}
      p={{ xs: "8px 15px 5px 8px", md: "10px 25px 10px 25px" }}
    >
      <Box
        width={{ xs: "16px", md: "32px" }}
        height={{ xs: "16px", md: "32px" }}
      >
        <img width={"100%"} src={icon} alt="arrow" />
      </Box>
      <Typography
        color={"primary.main"}
        variant="h6"
        fontSize={{ xs: "16px", md: "20px" }}
        fontWeight={900}
      >
        {title}
      </Typography>
    </Stack>
  );
}
