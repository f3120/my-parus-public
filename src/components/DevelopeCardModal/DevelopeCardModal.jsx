import { Box, Button, Dialog, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import animal from "../../media/icons/Animal.svg";
import arrowLeft from "../../media/icons/ArrowLeft.svg";
import { ModalContext } from "../../context/ModalContext";
import { navigate } from "gatsby";

const modalID = "developCard";

export default function DevelopeCardModal() {
  const { isModalActive, setModalInactive } = useContext(ModalContext);

  return (
    <Dialog
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "15px",
        },
      }}
    >
      <Stack
        alignItems={"center"}
        direction={{ xs: "column", sm: "row" }}
        padding={{ xs: "10px", sm: "15px", md: "20px", lg: "30px" }}
        justifyContent={"space-between"}
      >
        <Box
          width={{ xs: "40%", sm: "250px", md: "330px", lg: "400px" }}
          sx={{
            marginTop: "5px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img width="100%" src={animal} alt="clubEvents" />
        </Box>
        <Stack direction={"column"} marginLeft={{ xs: "10px", sm: "20px" }}>
          <Stack
            direction="column"
            gap={{ xs: "5px", md: "10px" }}
            marginTop={{ xs: "10px", sm: "0px", lg: "5px" }}
          >
            <Typography color={"primary.main"} variant="h3">
              Ой-ой...
            </Typography>
            <Typography color={"primary.main"} variant="body1">
              Нас чуть не унесло, но не переживайте! Эта <b>страница</b> пока{" "}
              <b>в разработке</b>
            </Typography>
            <Typography color={"primary.main"} variant="body1">
              Готовим крутой редизайн для ярких приключений в мире парусов!
            </Typography>
          </Stack>

          <Button
            variant="contained"
            sx={{
              "&:hover": {
                backgroundColor: "secondary.main",
                boxShadow: "none",
              },
              bgcolor: "secondary.main",
              width: { sm: "195px", md: "261px" },
              marginTop: { xs: "10px", sm: "20px", md: "40px" },
            }}
            onClick={() => {
              setModalInactive(modalID);
            }}
          >
            <Stack direction={"row"}>
              <Box>
                <img src={arrowLeft} alt="clubEvents" />
              </Box>
              <Typography
                marginLeft={{ xs: "5px", lg: "10px" }}
                textAlign={"center"}
                color={"primary.main"}
              >
                Вернуться
              </Typography>
            </Stack>
          </Button>
        </Stack>
      </Stack>
    </Dialog>
  );
}
