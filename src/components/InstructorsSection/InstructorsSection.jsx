import {
  Box,
  Container,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React from "react";
import InstructorsSlide from "./InstructorsStackSlide/InstructorsStackSlide";
import holiday from "../../media/icons/holiday.svg";
import victory from "../../media/icons/victory.svg";
import fire from "../../media/icons/fire.svg";
import heart from "../../media/icons/heart.svg";
import support from "../../media/icons/support.svg";
import WithItomSirius from "../WithItomSirius/WithItomSirius";
import Slider from "react-slick";
import Alexandr from "../../media/images/Alexandr.png";
import Diana from "../../media/images/Diana.png";
import Lera from "../../media/images/Lera.png";
import { mainTheme } from "../../config/MUI/mainTheme";
import InstructorsSliderSlide from "./InstructorsSliderSlide/InstructorsSliderSlide";
import { graphql, useStaticQuery } from "gatsby";
import { gql, useQuery } from "@apollo/client";

const textItem = [
  { title: "Доверие ", icon: heart },
  { title: "Рост", icon: holiday },
  { title: "Победы", icon: victory },
  { title: "Поддержку", icon: support },
  { title: "Заинтересованность", icon: fire },
];

const APOLLO_QUERY = gql`
  query QueryInstructors($categoryName: String!) {
    posts(where: { categoryName: $categoryName }, first: 1000) {
      edges {
        node {
          id
          instructorsfield {
            fieldGroupName
            instructordescription
            instructorexperience
            instructorname
            smilecode
            instructorphoto {
              sourceUrl
            }
          }
          clubfield {
            clubname
          }
        }
      }
    }
  }
`;

export default function InstructorsSection({ clubId }) {
  const { loading, error, data } = useQuery(APOLLO_QUERY, {
    variables: { categoryName: "instructor" },
  });

  console.log({ data, error });

  const instructors = !loading
    ? data?.posts?.edges
        .filter((node) => node?.node?.clubfield?.clubname === clubId)
        .map((node) => ({
          name: node?.node?.instructorsfield?.instructorname,
          experience: node?.node?.instructorsfield?.instructorexperience,
          description: node?.node?.instructorsfield?.instructordescription,
          badge: node?.node?.instructorsfield?.smilecode,
          photo: node?.node?.instructorsfield?.instructorphoto?.sourceUrl,
        }))
    : [];

  console.log(instructors);

  const upMd = useMediaQuery(mainTheme.breakpoints.up("md"));

  const settings = {
    dots: false,
    // infinite: true,
    pauseOnHover: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    className: "main-slider",
    infinite: instructors?.length === 1 ? false : true,
  };

  return (
    <Box backgroundColor={"secondary.main"}>
      <Container>
        <Grid2
          container
          paddingBottom={"20px"}
          paddingTop={"20px"}
          columnSpacing={"10px"}
        >
          <Grid2 xs={12} sm={6} md={12} lg={4}>
            <Typography variant="h3">Наши инструкторы — это про:</Typography>
            <Stack
              paddingBottom={"20px"}
              marginTop={{ xs: "10px", sm: "10px", md: "20px" }}
              flexWrap="wrap"
              direction="row"
            >
              {textItem.map((item) => (
                <WithItomSirius icon={item?.icon} title={item?.title} />
              ))}
            </Stack>
          </Grid2>

          {upMd ? (
            <Grid2 xs md={12} lg={8}>
              <InstructorsSlide instructor={instructors} />
            </Grid2>
          ) : (
            <Grid2 xs={12} sm={6}>
              <Slider {...settings}>
                {instructors.map((data) => (
                  <InstructorsSliderSlide instructor={data} />
                ))}
              </Slider>
            </Grid2>
          )}
        </Grid2>
      </Container>
    </Box>
  );
}
