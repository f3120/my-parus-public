import { Box, Stack, Typography } from "@mui/material";
import React, { useState } from "react";

export default function InstructorsSlide({ instructor }) {
  const [active, setActive] = useState(0);
  function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
  }
  return (
    <Stack direction={"row"} gap={"20px"}>
      {instructor.map((item, idx) => (
        <Box
          display={"flex"}
          onMouseEnter={() => setActive(idx)}
          onMouseLeave={() => setActive(0)}
          position={"relative"}
          flex={1}
          flexGrow={idx === active ? 3 : 1}
          sx={{
            backgroundImage: `url(${item.photo})`,
            alignItems: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            borderRadius: "30px",
            overflow: "hidden",
            backgroundPosition: "center",
            transition: "flex-grow 0.3s",

            width: { md: "440px", lg: "550px", xl: "472px" },
            height: { md: "400px", lg: "500px", xl: "528px" },
          }}
        >
          <Stack
            padding={"15px"}
            width={"100%"}
            height={"100%"}
            direction={"column"}
            justifyContent={"flex-end"}
            alignItems={idx === active ? "start" : "center"}
            sx={{
              background:
                "linear-gradient(180deg, rgba(6, 11, 17, 0) 0%, rgba(6, 11, 17, 0.125) 50%, rgba(6, 11, 17, 0.7) 100%)",
            }}
            // left={idx === active ? "30px" : "50px"}
          >
            <Stack alignItems={idx === active ? "start" : "center"}>
              <Typography variant="caption">Инструктор </Typography>
              <Typography marginTop={"1px"} variant="h5">
                {item.name}
              </Typography>
            </Stack>

            <Stack
              marginTop={"10px"}
              direction={"row"}
              width={"100%"}
              justifyContent={idx === active ? "start" : "center"}
            >
              <Box
                padding={"15px"}
                display={"flex"}
                justifyContent={"center"}
                alignItems={"center"}
                borderRadius={"100px"}
                backgroundColor={"primary.white"}
                width={{ xs: "26px", md: "52px", lg: "62px" }}
                height={{ xs: "26px", md: "52px", lg: "62px" }}
              >
                <p style={{ fontSize: "26px" }}>{decodeHtml(item.badge)}</p>
              </Box>
              <Stack
                width={"80%"}
                marginLeft={"10px"}
                display={idx === active ? "flex" : "none"}
                direction={"column"}
              >
                <Typography variant="caption">
                  Стаж: {item.experience}
                </Typography>
                <Typography
                  marginTop={{ lg: "3px", xl: "5px" }}
                  variant="caption"
                >
                  {item.description}
                </Typography>
              </Stack>
            </Stack>
          </Stack>
        </Box>
      ))}
    </Stack>
  );
}
