import { Box, Stack, Typography } from "@mui/material";
import React from "react";

export default function InstructorsSliderSlide({ instructor }) {
  function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
  }
  return (
    <Stack direction={"row"} gap={"20px"}>
      <Box
        position={"relative"}
        flex={1}
        width={{ xs: "260px", sm: "275px" }}
        height={{ xs: "261px", sm: "290px" }}
        sx={{
          backgroundImage: `url(${instructor.photo})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          borderRadius: "30px",
          overflow: "hidden",
          backgroundPosition: "center",
          transition: "flex-grow 0.3s",
        }}
      >
        <Stack
          width={"100%"}
          height={"100%"}
          direction={"column"}
          justifyContent={"flex-end"}
          sx={{
            background:
              "linear-gradient(180deg, rgba(6, 11, 17, 0) 0%, rgba(6, 11, 17, 0.125) 50%, rgba(6, 11, 17, 0.7) 100%)",
          }}
          padding={"15px"}
        >
          <Typography variant="caption">Инструктор </Typography>
          <Typography marginTop={"1px"} variant="h5">
            {instructor.name}
          </Typography>
          <Stack marginTop={"10px"} direction={"row"}>
            <Box
              display={"flex"}
              justifyContent={"center"}
              alignItems={"center"}
              borderRadius={"100px"}
              backgroundColor={"primary.white"}
              width={{ xs: "26px", md: "52px", lg: "62px" }}
              height={{ xs: "26px", md: "52px", lg: "62px" }}
            >
              <p style={{ fontSize: "16px" }}>{decodeHtml(instructor.badge)}</p>
            </Box>
            <Box marginLeft={"10px"} display={"flex"} alignItems={"center"}>
              <Typography variant="caption">
                Стаж: {instructor.experience}
              </Typography>
            </Box>
          </Stack>
        </Stack>
      </Box>
    </Stack>
  );
}
