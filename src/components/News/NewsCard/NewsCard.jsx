import { Box, Stack, Typography } from "@mui/material";
import React from "react";

import { navigate } from "gatsby";
export default function NewsCard({ news }) {
  const { date, title, id, description, background, check, link } = news;

  const checkLink = () => {
    if (Array.isArray(check) && check.length > 0) {
      window.open(link, "_blank");
    } else {
      navigate(`/newsMore/?ID=${id}`, { state: { id } });
    }
  };
  return (
    <Stack
      sx={{
        boxShadow: "0 0 20px 5px rgba(50, 79, 123, 0.25)",

        borderRadius: "25px",

        direction: "column",
      }}
    >
      <Stack
        sx={{
          height: { xs: "130px", sm: "150px", md: "150px", lg: "200px" },
          backgroundImage: `url(${background})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          borderRadius: "25px 25px 0 0",
          padding: "10px",
        }}
      >
        <Stack justifyContent={"end"} direction="row" flexWrap="wrap">
          <Stack
            backgroundColor={"primary.main"}
            p={"8px 12px 8px 12px"}
            borderRadius={"30px"}
          >
            <Typography color={"primary.white"}>{date}</Typography>
          </Stack>
        </Stack>
      </Stack>
      <Stack
        sx={{
          overflow: "hidden",
        }}
        height={{
          xs: "230px",
          sm: "230px",
          md: "250px",
          lg: "270px",
          xl: "300px",
        }}
        padding={{ xs: "10px", xl: "20px" }}
        direction={"column"}
      >
        <Stack>
          <Typography color={"primary.main"} variant="h5">
            {title}
          </Typography>

          <Typography
            sx={{
              WebkitBoxOrient: "vertical",
              WebkitLineClamp: 6, // Задайте нужное количество строк
              display: "-webkit-box",
              overflow: "hidden",
            }}
            marginTop={"5px"}
            color={"primary.main"}
            variant="body1"
          >
            {description}
          </Typography>
        </Stack>

        <Box
          onClick={checkLink}
          sx={{
            marginTop: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "43px",
            borderRadius: "30px",
            border: "1px solid #FFFFFF ",
            cursor: "pointer",
            backgroundColor: "primary.main",
          }}
        >
          <Typography color={"primary.white"}>Читать дальше</Typography>
        </Box>
      </Stack>
    </Stack>
  );
}
