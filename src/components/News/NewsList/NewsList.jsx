import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React from "react";
import NewsCard from "../NewsCard/NewsCard";

export default function NewsList({ news }) {
  return (
    <Grid2 spacing={"20px"} container>
      {news?.map((item) => (
        <Grid2 xs={12} sm={6} md={4} xl={3}>
          <NewsCard news={item} />
        </Grid2>
      ))}
    </Grid2>
  );
}
