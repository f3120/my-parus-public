import { Stack, Typography } from "@mui/material";
import React from "react";

export default function NewsTitle() {
  return (
    <Stack
      marginTop={"20px"}
      marginBottom={"30px"}
      alignItems={"center"}
      justifyContent={"center"}
      width={"100% "}
      backgroundColor={"secondary.main"}
      borderRadius={"40px"}
      padding={{ xs: "15px", lg: "30px" }}
      p={{
        xs: "10px ",
        md: "20px",
        lg: "30px ",
      }}
    >
      <Typography variant="h3">Лента новостей</Typography>
    </Stack>
  );
}
