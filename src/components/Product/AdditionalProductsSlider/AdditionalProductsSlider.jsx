import { Box, Container, Stack, Typography } from "@mui/material";
import React, { useEffect } from "react";
import Slider from "react-slick";
import ShopItem from "../../Shop/ShopItemList/ShopItem/ShopItem";
import "./AdditionalProductsSlider.css";

export default function AdditionalProductsSlider({ related = [] }) {
  const [isDragging, setIsDragging] = React.useState(false);
  const [emptySlides, setEmptySlides] = React.useState([]);

  const sliderRef = React.useRef(null);

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    // swipeToSlide: true,
    arrows: true,
    className: "addProduct-slider",
    beforeChange: () => {
      setIsDragging(true);
    },
    afterChange: () => {
      setIsDragging(false);
    },
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  useEffect(() => {
    if (!sliderRef.current) return;

    const currentSlidesToShow =
      settings.responsive.find(
        (item) => item.breakpoint === sliderRef.current.state.breakpoint
      )?.settings?.slidesToShow || settings.slidesToShow;

    if (related.length < currentSlidesToShow) {
      setEmptySlides(
        new Array(currentSlidesToShow - related.length).fill(<div></div>)
      );
    } else setEmptySlides([]);
  }, [related, sliderRef.current]);

  return (
    <Container>
      <Stack direction={"column"} gap={"20px"}>
        <Typography variant="h5" color={"primary.main"}>
          Дополнить образ
        </Typography>
        <Box position={"relative"} width={"100%"}>
          {related.length > 0 && (
            <Slider
              {...settings}
              ref={(slider) => (sliderRef.current = slider)}
            >
              {[...related, ...emptySlides].map((item, index) => (
                <Box key={item?.node?.id + index} height={"100%"}>
                  {item?.node ? (
                    <ShopItem product={item?.node} noClick={isDragging} />
                  ) : (
                    <div></div>
                  )}
                </Box>
              ))}
            </Slider>
          )}
        </Box>
      </Stack>
    </Container>
  );
}
