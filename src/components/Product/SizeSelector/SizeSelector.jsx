import { Box, Stack, Typography } from "@mui/material";
import React from "react";
import "./SizeSelector.css";
import Slider from "react-slick";

export default function SizeSelector({
  variations = [],
  selectedSize,
  setSelectedSize,
}) {
  return (
    <Stack direction={"column"} width={"100%"} gap={"10px"}>
      <Typography variant="body1" color={"primary.main"}>
        Выберите размер:
      </Typography>
      <Stack
        direction={"row"}
        width={"100%"}
        gap={"10px"}
        flexWrap={"wrap"}
        justifyContent={{ xs: "space-between", sm: "flex-start" }}
      >
        {variations?.nodes.map((item) => (
          <Stack
            bgcolor={
              selectedSize?.name === item?.name
                ? "primary.main"
                : "secondary.main"
            }
            height={"50px"}
            width={"50px"}
            justifyContent={"center"}
            alignItems={"center"}
            borderRadius={"10px"}
            sx={{
              cursor: "pointer",
              transition: "0.3s",
              "&:hover": {
                boxShadow: "0px 2px 10px 0px rgba(0,0,0,0.2)",
              },
            }}
            onClick={() => setSelectedSize(item)}
          >
            <Typography
              variant="body1"
              color={
                selectedSize?.name === item?.name
                  ? "primary.white"
                  : "primary.main"
              }
            >
              {String(item?.name).split(" - ")[1]}
            </Typography>
          </Stack>
        ))}
      </Stack>
    </Stack>
  );
}
