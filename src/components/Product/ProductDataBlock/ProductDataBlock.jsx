import { Button, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import { correctingPrice } from "../../Shop/ShopItemList/ShopItem/ShopItem";
import { ReactComponent as InfoCircle } from "../../../media/icons/infoCircle.svg";
import { ReactComponent as SmallTruck } from "../../../media/icons/smallTruck.svg";

import ProductInfoModal from "./ProductInfoModal/ProductInfoModal";
import { ModalContext } from "../../../context/ModalContext";
import ProductDeliveryModal from "./ProductDeliveryModal/ProductDeliveryModal";
import { mainTheme } from "../../../config/MUI/mainTheme";

export default function ProductDataBlock({ productData }) {
  const { setModalActive } = useContext(ModalContext);

  const {
    name,
    onSale,
    price,
    regularPrice,
    description,
    productTags,
    stockStatus,
  } = productData;

  const buttonStyle = {
    "&:hover": {
      backgroundColor: "secondary.main",
      boxShadow: "0px 2px 10px 0px rgba(0,0,0,0.2)",
    },
    bgcolor: "secondary.main",
  };

  return (
    <Stack
      direction={"column"}
      width={"100%"}
      alignItems={"flex-start"}
      gap={"10px"}
    >
      <Typography variant="h5" color={"primary.main"}>
        {name}
      </Typography>
      <Typography variant="caption" color={"primary.main"}>
        {stockStatus === "IN_STOCK" ? "В наличии" : "Нет в наличии"}
      </Typography>
      <Stack
        direction={"column"}
        alignItems={"flex-start"}
        gap={"3px"}
        sx={{
          backgroundColor: "secondary.main",
          padding: "10px 25px",
          borderRadius: "10px",
        }}
      >
        <Typography variant="body1" color={"primary.main"}>
          <strong>{correctingPrice(onSale ? price : regularPrice)} ₽</strong>{" "}
          <strike>{onSale && correctingPrice(regularPrice) + "  ₽"}</strike>
        </Typography>
        {onSale && (
          <Typography variant="caption" color={"primary.main"}>
            Цена со скидкой{" "}
            {(
              ((correctingPrice(regularPrice) - correctingPrice(price)) /
                correctingPrice(regularPrice)) *
              100
            ).toFixed()}
            %
          </Typography>
        )}
      </Stack>
      <Typography
        variant="body1"
        color={"primary.main"}
        width={{ xs: "100%", sm: "80%" }}
      >
        <ul
          style={{
            width: "100%",
            listStyleType: "disc",
            color: mainTheme.palette.primary.main,
            padding: "0 0 0 20px",
            margin: "10px 0",
          }}
        >
          {productTags?.nodes?.map((item) => (
            <li key={item?.name} style={{ paddingLeft: "10px" }}>
              <Typography
                variant="body1"
                color={"primary.main"}
                sx={{
                  textTransform: "lowercase",
                  margin: "5px 0",
                }}
              >
                {item?.name}
              </Typography>
            </li>
          ))}
        </ul>
      </Typography>
      <Stack
        width={"100%"}
        direction={"row"}
        justifyContent={"flex-start"}
        alignItems={"center"}
        gap={"10px"}
      >
        <Button
          sx={buttonStyle}
          variant="contained"
          disabled={typeof description === "string" ? false : true}
          onClick={() => setModalActive("productInfoModal")}
        >
          <Stack direction={"row"} gap={"5px"} alignItems={"center"}>
            <Typography variant="body1" color={"primary.main"}>
              Описание
            </Typography>
            <InfoCircle width={"20px"} height={"20px"} />
          </Stack>
        </Button>
        <Button
          sx={buttonStyle}
          variant="contained"
          onClick={() => setModalActive("productDeliveryModal")}
        >
          <Stack direction={"row"} gap={"5px"} alignItems={"center"}>
            <Typography variant="body1" color={"primary.main"}>
              Доставка
            </Typography>
            <SmallTruck width={"20px"} height={"20px"} />
          </Stack>
        </Button>
      </Stack>
      <ProductInfoModal description={description} />
      <ProductDeliveryModal />
    </Stack>
  );
}
