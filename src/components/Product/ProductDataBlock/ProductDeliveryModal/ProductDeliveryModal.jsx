import { Box, Dialog, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import { ModalContext } from "../../../../context/ModalContext";
import { mainTheme } from "../../../../config/MUI/mainTheme";
import ChipWithIcon from "../../../ChipWithIcon/ChipWithIcon";

const modalID = "productDeliveryModal";
export default function ProductDeliveryModal() {
  const { isModalActive, setModalInactive } = useContext(ModalContext);

  return (
    <Dialog
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "20px",
        },
      }}
    >
      <Stack
        direction={"column"}
        padding={"20px"}
        gap={"10px"}
        alignItems={"flex-start"}
      >
        <Typography
          variant="h5"
          color={"primary.main"}
          sx={{
            textAlign: "center",
            width: "100%",
            padding: "15px 30px",
            backgroundColor: "secondary.main",
            borderRadius: "10px",
          }}
        >
          Осуществление доставки
        </Typography>
        <ChipWithIcon text={"Доставка по России"} textEmoji={"🚚"} />
        <Typography variant="body1" color={"primary.main"}>
          Доставка по России осуществляется <strong>Почтой</strong>
        </Typography>
        <ChipWithIcon
          text={"Самовывоз"}
          textEmoji={"📦"}
          sx={{ marginTop: "10px" }}
        />
        <Typography variant="body1" color={"primary.main"}>
          Самовывоз осуществляется по адресу:
          <br />
          Республика Крым, г. Ялта, с. Оползневое, ул. Генерала Острякова, д.9
        </Typography>
      </Stack>
    </Dialog>
  );
}
