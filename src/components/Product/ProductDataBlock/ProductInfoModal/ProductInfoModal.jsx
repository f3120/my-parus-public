import { Box, Dialog, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import { ModalContext } from "../../../../context/ModalContext";
import { mainTheme } from "../../../../config/MUI/mainTheme";
import parse from "html-react-parser";

const modalID = "productInfoModal";
export default function ProductInfoModal({ description }) {
  const { isModalActive, setModalInactive } = useContext(ModalContext);

  return (
    <Dialog
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "20px",
        },
      }}
    >
      <Stack
        direction={"column"}
        padding={"20px"}
        gap={"20px"}
        alignItems={"flex-start"}
      >
        <Typography
          variant="h5"
          color={"primary.main"}
          width={"100%"}
          textAlign={"center"}
        >
          Описание
        </Typography>
        <Typography variant="body1" color={"primary.main"}>
          {typeof description === "string" && parse(description)}
        </Typography>
      </Stack>
    </Dialog>
  );
}
