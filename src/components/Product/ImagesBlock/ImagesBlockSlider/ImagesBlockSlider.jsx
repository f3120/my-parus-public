import React, { useRef } from "react";
import Slider from "react-slick";
import "./ImagesBlockSlider.css";
import { Box, Stack } from "@mui/material";

export default function ImagesBlockSlider({
  galleryImages = [],
  nav1,
  nav2,
  setSliderRef1,
  currentSlide,
  setCurrentSlide,
}) {
  const settings = {
    dots: false,
    infinite: galleryImages.length === 1 ? false : true,
    speed: 500,
    slidesToShow: 1,
    arrows: false,
    afterChange: (curSlide) => {
      setCurrentSlide(curSlide);
    },
    className: "productBig-slider",
  };

  const dotsStyle = {
    width: "100%",
    height: "4px",
    borderRadius: "20px",
    transition: "all 0.2s ease",
  };

  const dotsWrapperStyle = {
    width: 65 / galleryImages.length + "%",
    maxWidth: "15%",
    p: "5px 0",
    cursor: "pointer",
  };

  return (
    <Box position={"relative"}>
      <Slider
        {...settings}
        asNavFor={nav2}
        ref={(slider) => {
          setSliderRef1(slider);
        }}
      >
        {galleryImages.length > 0 &&
          galleryImages.map((item, index) => (
            <Box
              key={index}
              height={{ xs: "260px", md: "30vw" }}
              sx={{
                borderRadius: "20px",
                display: "flex !important",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#0000000f",
              }}
              overflow={"hidden"}
              position={"relative"}
            >
              <img
                height={"100%"}
                src={item.sourceUrl}
                alt={"Изображение товара"}
              />
            </Box>
          ))}
      </Slider>
      <Box
        position={"absolute"}
        sx={{
          bottom: { xs: "5px", md: "10px" },
          width: "100%",
          display: { xs: "flex", md: "none" },
          justifyContent: "center",
        }}
      >
        <Stack
          direction={"row"}
          justifyContent={"center"}
          gap={"7px"}
          width={"100%"}
        >
          {galleryImages.length > 0 &&
            galleryImages.map((item, index) => (
              <Box
                onClick={() => {
                  nav1.slickGoTo(index);
                }}
                sx={{
                  ...dotsWrapperStyle,
                }}
              >
                <Box
                  sx={{
                    ...dotsStyle,
                    backgroundColor:
                      currentSlide === index ? "primary.main" : "primary.white",
                  }}
                ></Box>
              </Box>
            ))}
        </Stack>
      </Box>
    </Box>
  );
}
