import { Box, Stack } from "@mui/material";
import React, { useRef } from "react";
import Slider from "react-slick";
import "./ImagesSelectorSlider.css";

export default function ImagesSelectorSlider({
  galleryImages = [],
  nav1,
  setSliderRef2,
  currentSlide,
}) {
  const settings = {
    dots: false,
    infinite: galleryImages.length < 4 ? false : true,
    speed: 500,
    slidesToShow: 4,
    swipeToSlide: true,
    arrows: true,
    className: "productSelector-slider",
  };

  return (
    <Box
      position={"relative"}
      width={"100%"}
      display={{ xs: "none", md: "block" }}
    >
      <Slider
        {...settings}
        asNavFor={nav1}
        ref={(slider) => {
          setSliderRef2(slider);
        }}
        focusOnSelect={true}
      >
        {galleryImages.length > 0 &&
          [
            ...galleryImages,
            ...Array(
              galleryImages.length < settings.slidesToShow
                ? settings.slidesToShow - galleryImages.length
                : 0
            ),
          ].map((item, index) =>
            item?.sourceUrl ? (
              <Box
                // onClickCapture={() => !isDragging && setCurrentSlide(index)}
                key={index}
                height={{ xs: "100px" }}
                sx={{
                  borderRadius: "10px",
                  display: "flex !important",
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#0000000f",
                }}
                overflow={"hidden"}
                position={"relative"}
              >
                {currentSlide === index && (
                  <Box
                    sx={{
                      width: "100%",
                      height: "100%",
                      position: "absolute",
                      backgroundColor: "#0000003d",
                    }}
                  ></Box>
                )}
                <img
                  height={"100%"}
                  src={item.sourceUrl}
                  alt={"Изображение товара"}
                />
              </Box>
            ) : (
              <div></div>
            )
          )}
      </Slider>
    </Box>
  );
}
