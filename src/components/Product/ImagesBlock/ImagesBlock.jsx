import { Stack } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import ImagesBlockSlider from "./ImagesBlockSlider/ImagesBlockSlider";
import ImagesSelectorSlider from "./ImagesSelectorSlider/ImagesSelectorSlider";

export default function ImagesBlock({ galleryImages }) {
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);

  const [currentSlide, setCurrentSlide] = React.useState(0);

  return (
    <Stack
      width={"100%"}
      direction={"column"}
      gap={"20px"}
      className="slider-container"
    >
      <ImagesBlockSlider
        galleryImages={galleryImages}
        nav1={nav1}
        nav2={nav2}
        setSliderRef1={setNav1}
        currentSlide={currentSlide}
        setCurrentSlide={setCurrentSlide}
      />
      <ImagesSelectorSlider
        galleryImages={galleryImages}
        nav1={nav1}
        setSliderRef2={setNav2}
        currentSlide={currentSlide}
      />
    </Stack>
  );
}
