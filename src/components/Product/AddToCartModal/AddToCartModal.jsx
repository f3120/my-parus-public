import { Box, Button, Dialog, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import { ModalContext } from "../../../context/ModalContext";
import { navigate } from "gatsby";
import { ReactComponent as ShopBag } from "../../../media/icons/shopBag.svg";

const modalID = "addToCart";
export default function AddToCartModal({ productName }) {
  const { isModalActive, setModalInactive } = useContext(ModalContext);

  return (
    <Dialog
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "20px",
        },
      }}
    >
      <Stack
        direction={"column"}
        padding={"20px"}
        gap={"15px"}
        alignItems={"center"}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "secondary.main",
            width: "50px",
            height: "50px",
            borderRadius: "10px",
          }}
        >
          <ShopBag width={"24px"} height={"24px"} />
        </Box>
        <Typography variant="h5" color={"primary.main"} textAlign={"center"}>
          Вы добавили в корзину:
          <br />
          {productName}
        </Typography>
        <Button
          sx={{
            padding: "7px 20px",
          }}
          variant="contained"
          onClick={() => {
            setModalInactive(modalID);
          }}
        >
          Продолжить покупки
        </Button>
        <Button
          sx={{
            padding: "7px 20px",

            "&:hover": {
              backgroundColor: "secondary.main",
              boxShadow: "0px 2px 10px 0px rgba(0,0,0,0.2)",
            },
            bgcolor: "secondary.main",
            color: "primary.main",
          }}
          variant="contained"
          onClick={() => {
            setModalInactive(modalID);
            navigate("/cart");
          }}
        >
          Перейти в корзину
        </Button>
      </Stack>
    </Dialog>
  );
}
