import { Box, Paper } from "@mui/material";
import React, { useState } from "react";
import Masonry from "react-masonry-css";

import GalleryPageBlock from "../GalleryPageBlock/GalleryPageBlock";
import Loader from "../../Loader/Loader";
import GalleryPageModal from "../GalleryPageModal/GalleryPageModal";

const masonryGridStyle = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  margin: "-10px",
};

const masonryGridColumnStyle = {
  width: "100%",
  padding: "10px",
};
export default function GalleryPageList({ photo, loading }) {
  const [selectedIndex, setSelectedIndex] = useState(null);
  console.log({ photo });
  if (loading) {
    return <Loader />;
  }
  return (
    <Masonry breakpointCols={{ default: 3, 600: 2 }} style={masonryGridStyle}>
      {photo?.map((item, i) => (
        <Paper key={i} style={{ ...masonryGridColumnStyle, boxShadow: "none" }}>
          <GalleryPageBlock
            setSelectedIndex={() => setSelectedIndex(i)}
            loading={loading}
            photo={item}
          />
        </Paper>
      ))}
      {selectedIndex !== null && (
        <GalleryPageModal
          photos={photo}
          selectedIndex={selectedIndex}
          setSelectedIndex={setSelectedIndex}
        />
      )}
    </Masonry>
  );
}
