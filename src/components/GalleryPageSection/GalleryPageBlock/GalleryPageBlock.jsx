import { Box, Button } from "@mui/material";
import React from "react";
import Loader from "../../Loader/Loader";
import save, { ReactComponent as Save } from "../../../media/icons/Save.svg";

import send, { ReactComponent as Send } from "../../../media/icons/Send.svg";
import { useLazyLoad } from "../../../hooks/useLazyLoad";
export default function GalleryPageBlock({ photo, loading, setSelectedIndex }) {
  // console.log({ photo });
  const [currentImage, loadingImageStatus] = useLazyLoad(
    Array.isArray(photo) && photo[0].sourceUrl,
    Array.isArray(photo) && photo[photo.length - 1].sourceUrl
  );

  if (loading) {
    return <Loader />;
  }
  return (
    <Box
      sx={{ position: "relative", cursor: "pointer" }}
      onClick={() => setSelectedIndex()}
    >
      <img
        src={currentImage}
        alt=""
        style={{ width: "100%", borderRadius: "20px" }}
      />
      <Box
        sx={{
          background: "rgba(255, 255, 255, 0.5)",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          position: "absolute",
          left: "10px",
          top: "10px",

          borderRadius: "100px",
          width: "40px",
          height: "40px",
          transition: "transform 0.2s ease-in-out",
          ":hover": {
            background: "rgba(255, 255, 255, 0.3)",
            transform: "scale(1.1)",
          },
        }}
        onClick={(e) => {
          e.stopPropagation();
          const url = photo[photo.length - 1].sourceUrl;
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", "image.jpg");
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }}
      >
        <img src={save} width={"60%"} alt="vectorLeft" />
      </Box>
    </Box>
  );
}
