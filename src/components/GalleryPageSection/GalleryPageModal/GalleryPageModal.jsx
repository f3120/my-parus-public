import { Box, Button, Dialog, IconButton, Typography } from "@mui/material";
import React, { useState } from "react";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIos";
import vectorLeft, {
  ReactComponent as VectorLeft,
} from "../../../media/icons/VectorLeft.svg";
import vectorRight, {
  ReactComponent as VectorRight,
} from "../../../media/icons/VectorRight.svg";
import send, { ReactComponent as Send } from "../../../media/icons/Send.svg";
import save, { ReactComponent as Save } from "../../../media/icons/Save.svg";
import { useSwipeable } from "react-swipeable";
import { useLazyLoad } from "../../../hooks/useLazyLoad";

export default function GalleryPageModal({
  photos,
  selectedIndex,
  setSelectedIndex,
}) {
  console.log({ photos });
  const handlePrevClick = () => {
    if (selectedIndex > 0) {
      setSelectedIndex(selectedIndex - 1);
    }
  };

  const handleNextClick = () => {
    if (selectedIndex < photos.length - 1) {
      setSelectedIndex(selectedIndex + 1);
    }
  };

  const handleCloseClick = () => {
    setSelectedIndex(null);
  };

  const handlers = useSwipeable({
    onSwipedLeft: handleNextClick,
    onSwipedRight: handlePrevClick,
  });

  return (
    <Dialog
      fullScreen={true}
      open={selectedIndex !== null}
      onClose={() => setSelectedIndex(null)}
      PaperProps={{
        sx: {
          width: "100%",
          height: "100%",
          m: 0,
          backgroundColor: "rgba(0, 0, 0, 0.5)",
          boxShadow: "none",
          backdropFilter: "blur(5px)",
        },
      }}
      {...handlers}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
        }}
      >
        <IconButton
          onClick={handlePrevClick}
          sx={{
            display: { xs: "none", md: "flex" },
            position: "absolute",
            left: "40px",
            zIndex: 100,
          }}
        >
          <Box
            sx={{
              backgroundColor: selectedIndex === 0 ? "#808080" : "#515253",
              opacity: selectedIndex === 0 ? 0.3 : 1,
            }}
            width={"60px"}
            height={"60px"}
            backgroundColor={"#515253"}
            borderRadius={"100px"}
            alignItems={"center"}
            display={"flex"}
            justifyContent={"center"}
          >
            {/* <VectorLeft width={"100%"} /> */}
            <img src={vectorLeft} alt="vectorLeft" />
          </Box>
        </IconButton>
        <IconButton
          sx={{
            display: { xs: "none", md: "flex" },
            position: "absolute",
            right: "40px",
            zIndex: 100,
          }}
          onClick={handleNextClick}
        >
          <Box
            sx={{
              backgroundColor:
                selectedIndex === photos.length - 1 ? "#808080" : "#515253",
              opacity: selectedIndex === photos.length - 1 ? 0.3 : 1,
            }}
            width={"60px"}
            height={"60px"}
            backgroundColor={"#515253"}
            borderRadius={"100px"}
            alignItems={"center"}
            display={"flex"}
            justifyContent={"center"}
          >
            {/* <VectorLeft width={"100%"} /> */}
            <img src={vectorRight} alt="vectorLeft" />
          </Box>
        </IconButton>
        <IconButton
          display={{ xs: "none", sm: "block" }}
          onClick={handleCloseClick}
          sx={{ position: "absolute", top: "40px", right: "40px", zIndex: 100 }}
        >
          <Box
            width={"40px"}
            height={"40px"}
            backgroundColor={"#FFFFFF"}
            borderRadius={"100px"}
            alignItems={"center"}
            display={"flex"}
            justifyContent={"center"}
          >
            {/* <VectorLeft width={"100%"} /> */}
            <img src={send} width={"50%"} alt="vectorLeft" />
          </Box>
        </IconButton>
        <Box
          onClick={() => {
            const url =
              photos[selectedIndex][photos[selectedIndex].length - 1].sourceUrl;
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("download", "image.jpg");
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }}
          width={"40px"}
          height={"40px"}
          alignItems={"center"}
          display={"flex"}
          justifyContent={"center"}
          sx={{
            top: "48px",
            right: "110px",
            position: "absolute",
            backgroundColor: "#C5C7C7",
            zIndex: 100,
            borderRadius: "100px",
          }}
        >
          <Button>
            <img src={save} width={"70%"} alt="vectorLeft" />
          </Button>
        </Box>
        <ModalPhoto photos={photos} index={selectedIndex} key={selectedIndex} />
        <Box
          sx={{
            position: "absolute",
            bottom: "35px",
            left: "50%",
            transform: "translateX(-50%)",
            padding: "4px 10px",
            borderRadius: "10px",
            backgroundColor: "#585858",
          }}
        >
          <Typography color="#FFFFFF">
            {selectedIndex + 1}/{photos.length}
          </Typography>
        </Box>
      </Box>
    </Dialog>
  );
}

const ModalPhoto = ({ photos, index }) => {
  const [currentImage, loadingImageStatus] = useLazyLoad(
    Array.isArray(photos[index]) && photos[index][0].sourceUrl,
    Array.isArray(photos[index]) &&
      photos[index][photos[index].length - 1].sourceUrl
  );

  return (
    <Box
      sx={{
        position: "relative",
        width: { xs: "100%", md: "80%" },
        height: { xs: "80%", md: "50%", lg: "80%" },
        backgroundImage: `url(${currentImage})`,
        backgroundSize: "contain",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        borderRadius: "20px",
        display: "flex",
        justifyContent: "center",
      }}
    ></Box>
  );
};
