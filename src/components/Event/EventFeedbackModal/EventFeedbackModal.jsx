import React, { useContext } from "react";
import { ModalContext } from "../../../context/ModalContext";
import {
  Button,
  Checkbox,
  Dialog,
  FormControl,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { mainTheme } from "../../../config/MUI/mainTheme";
import { Link } from "gatsby";
import emailjs from "@emailjs/browser";

const modalID = "eventFeedback";
export default function EventFeedbackModal({ eventName }) {
  const { isModalActive, setModalActive, setModalInactive } =
    useContext(ModalContext);
  const [formData, setFormData] = React.useState({
    eventName: eventName,
    name: undefined,
    club: undefined,
    city: undefined,
    email: undefined,
    phone: undefined,
    clothesColor: undefined,
    message: undefined,
    agree: false,
  });

  const handleSubmit = () => {
    console.log({ formData });
    emailjs.send(
      "service_d6lm83x",
      "template_tzxy49v",
      formData,
      "aPHzbaZ9nf1dYaLxr"
    );
    setFormData({
      eventName: eventName,
      name: undefined,
      club: undefined,
      city: undefined,
      email: undefined,
      phone: undefined,
      clothesColor: undefined,
      message: undefined,
      agree: false,
    });
    setModalInactive(modalID);
    setModalActive("snackbarFeedBack");
  };

  return (
    <Dialog
      fullWidth
      maxWidth={"md"}
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "15px",
        },
      }}
    >
      <Stack
        direction={"column"}
        padding={{ xs: "10px", sm: "15px" }}
        justifyContent={"space-between"}
        gap={"15px"}
      >
        <Stack
          direction={"column"}
          p={"10px"}
          sx={{
            backgroundColor: "secondary.main",
            borderRadius: "15px",
          }}
        >
          <Typography variant="h4">Форма для участия</Typography>
          <Typography variant="caption" color={"primary.main"}>
            Заполните данную форму для участия в мероприятии
            <br />
            По статусу вашей заявки с вами свяжутся по электронной почте
          </Typography>
        </Stack>
        <Stack direction={"column"} gap={"10px"}>
          <TextField
            error={formData.name === ""}
            label="Название команды"
            required
            variant="filled"
            fullWidth
            size="small"
            value={formData.name}
            onChange={(e) => setFormData({ ...formData, name: e.target.value })}
          />
          <TextField
            error={formData.club === ""}
            label="Школа/клуб"
            required
            variant="filled"
            fullWidth
            size="small"
            value={formData.club}
            onChange={(e) => setFormData({ ...formData, club: e.target.value })}
          />
          <TextField
            error={formData.city === ""}
            label="Город"
            required
            variant="filled"
            fullWidth
            size="small"
            value={formData.city}
            onChange={(e) => setFormData({ ...formData, city: e.target.value })}
          />
          <TextField
            error={formData.email === ""}
            required
            label="E-mail"
            variant="filled"
            fullWidth
            size="small"
            value={formData.email}
            onChange={(e) =>
              setFormData({ ...formData, email: e.target.value })
            }
          />
          <TextField
            error={formData.phone === ""}
            required
            label="Телефон"
            variant="filled"
            fullWidth
            size="small"
            value={formData.phone}
            onChange={(e) =>
              setFormData({ ...formData, phone: e.target.value })
            }
          />

          <FormControl fullWidth variant={"filled"}>
            <Select
              disableUnderline={true}
              MenuProps={{ PaperProps: { sx: { borderRadius: "15px" } } }}
              IconComponent={KeyboardArrowDownIcon}
              defaultValue={-1}
              onChange={(e) =>
                setFormData({ ...formData, clothesColor: e.target.value })
              }
              sx={{
                "& .MuiSelect-select": {
                  paddingTop: { xs: "10px", sm: "15px" },
                  paddingBottom: { xs: "10px", sm: "15px" },
                },
              }}
            >
              <MenuItem value={-1} disabled>
                <Typography variant="body1" color={"primary.main"}>
                  Желаемый цвет манишек
                </Typography>
              </MenuItem>
              <MenuItem value={"синий"}>
                <Typography variant="body1" color={"primary.main"}>
                  Синий
                </Typography>
              </MenuItem>
              <MenuItem value={"зеленый"}>
                <Typography variant="body1" color={"primary.main"}>
                  Зеленый
                </Typography>
              </MenuItem>
              <MenuItem value={"желтый"}>
                <Typography variant="body1" color={"primary.main"}>
                  Желтый
                </Typography>
              </MenuItem>
              <MenuItem value={"красный"}>
                <Typography variant="body1" color={"primary.main"}>
                  Красный
                </Typography>
              </MenuItem>
            </Select>
          </FormControl>

          <TextField
            label="Комментарий"
            multiline
            sx={{
              "& .MuiInputBase-multiline": {
                backgroundColor: mainTheme.palette.secondary.main,
                "&.Mui-focused": {
                  backgroundColor: mainTheme.palette.secondary.main,
                },
                "&:hover": {
                  backgroundColor: mainTheme.palette.secondary.main,
                },
              },
            }}
            variant="filled"
            fullWidth
            size="small"
            value={formData.comment}
            onChange={(e) =>
              setFormData({ ...formData, comment: e.target.value })
            }
          />
        </Stack>
        <Stack
          direction={"row"}
          justifyContent={"center"}
          alignItems={"center"}
          width={"100%"}
        >
          <Checkbox
            checked={formData.agree}
            onChange={(e) =>
              setFormData({ ...formData, agree: e.target.checked })
            }
          />
          <Typography variant="body1" color={"primary.main"}>
            Я согласен на обработку{" "}
            <Typography
              color={"primary.main"}
              component={"span"}
              onClick={() => setModalActive("privacyPolicy")}
              sx={{ textDecoration: "underline", cursor: "pointer" }}
            >
              персональных данных
            </Typography>{" "}
            и с условиями{" "}
            <Typography
              component={"span"}
              color={"primary.main"}
              onClick={() => setModalActive("userAgreement")}
              sx={{ textDecoration: "underline", cursor: "pointer" }}
            >
              пользовательского соглашения
            </Typography>
          </Typography>
        </Stack>
        <Button
          onClick={handleSubmit}
          variant="contained"
          disabled={
            !formData.agree ||
            !formData.name ||
            !formData.phone ||
            !formData.email ||
            !formData.club ||
            !formData.city
          }
        >
          Отправить
        </Button>
      </Stack>
    </Dialog>
  );
}
