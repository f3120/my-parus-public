import { StackedLineChart } from "@mui/icons-material";
import { Box, Button, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import { ReactComponent as DownloadIcon } from "../../../media/icons/downloadicon.svg";
import dayjs from "dayjs";
import { ModalContext } from "../../../context/ModalContext";

export default function EventMainSection({ postData, clubName }) {
  const { setModalActive } = useContext(ModalContext);
  return (
    <Stack
      direction={{ xs: "column", md: "row" }}
      sx={{
        width: "100%",
        height: { md: "100vh" },
        position: "relative",
        top: "-68px",
        marginBottom: "-68px",
        pt: "68px",
        background: "linear-gradient(360deg, #000b1b 0%, #002051 100%);",
      }}
    >
      <Stack
        sx={{
          height: "calc(100vh - 68px)",
          width: { xs: "100%", md: "60%" },
          padding: { xs: "20px", md: "40px" },
        }}
        direction={"column"}
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Stack
          width={"100%"}
          direction={"column"}
          gap={"10px"}
          justifyContent={"center"}
          alignItems={{ xs: "center", md: "flex-start" }}
        >
          <Stack
            direction={"row"}
            gap={"5px"}
            alignItems={"center"}
            sx={{ cursor: "pointer" }}
            onClick={() => {
              const url = postData?.eventpresentation?.sourceUrl;
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", "presentation.pdf");
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
            }}
          >
            <DownloadIcon width="16px" height="16px" />
            <Typography
              variant="body1"
              color={"primary.white"}
              sx={{ opacity: 0.7 }}
            >
              Скачать презентацию
            </Typography>
          </Stack>
          <Stack
            direction={"row"}
            gap={"5px"}
            alignItems={"center"}
            sx={{ cursor: "pointer" }}
            onClick={() => {
              const url = postData?.eventreglament?.sourceUrl;
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", "reglament.pdf");
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
            }}
          >
            <DownloadIcon width="16px" height="16px" />
            <Typography
              variant="body1"
              color={"primary.white"}
              sx={{ opacity: 0.7 }}
            >
              Скачать положение
            </Typography>
          </Stack>
        </Stack>
        <Stack
          direction={"column"}
          gap={"10px"}
          width={"100%"}
          alignItems={"center"}
        >
          <Typography
            variant="body1"
            color={"primary.white"}
            sx={{ opacity: 0.7 }}
          >
            {postData?.eventstart &&
              dayjs(postData?.eventstart, "DD.MM.YYYY")
                .format("DD MMMM")
                .toUpperCase()}{" "}
            -{" "}
            {postData?.eventend &&
              dayjs(postData?.eventend, "DD.MM.YYYY")
                .format("DD MMMM")
                .toUpperCase()}
          </Typography>
          <Typography variant="h2" color={"primary.white"} textAlign={"center"}>
            {postData?.eventtittle}
          </Typography>
          <Typography
            variant="body1"
            color={"primary.white"}
            textAlign={"center"}
          >
            {postData?.eventdescriptions}
          </Typography>
          {postData?.statusevent === "open" ? (
            <Button
              variant="contained"
              sx={{
                backgroundColor: "primary.white",
                color: "primary.main",
                "&:hover": { color: "primary.white" },
              }}
              onClick={() => setModalActive("eventFeedback")}
            >
              <Typography variant="h6" color={"inherit"}>
                Участвовать
              </Typography>
            </Button>
          ) : (
            <Typography variant="h6" color={"primary.white"}>
              Участие недоступно
            </Typography>
          )}
        </Stack>
        <Stack
          gap={{ xs: "10px", md: "20px" }}
          width={"100%"}
          direction={"row"}
          alignItems={"center"}
          justifyContent={"center"}
          flexWrap={"wrap"}
        >
          <TagPaper
            tagName={"Статус"}
            tagVal={postData?.statusevent}
            icon={"⌛"}
          />

          <TagPaper
            tagName={"Возраст"}
            tagVal={postData?.eventage}
            icon={"👱"}
          />
          <TagPaper tagName={"Клуб"} tagVal={clubName} icon={"📍"} />
        </Stack>
      </Stack>
      <Box
        sx={{
          width: { xs: "100%", md: "40%" },
          height: { xs: "300px", md: "100%" },
          overflow: "hidden",
          borderTopLeftRadius: "20px",
          borderTopRightRadius: { xs: "20px", md: "0" },
          borderBottomLeftRadius: { md: "20px" },
        }}
      >
        <img
          width={"100%"}
          height={"100%"}
          style={{ objectFit: "cover" }}
          src={postData?.eventphoto?.sourceUrl}
          alt="eventPhoto"
        />
      </Box>
    </Stack>
  );
}

const TagPaper = ({ tagName, tagVal, icon }) => {
  return (
    <Stack
      direction={"column"}
      alignItems={"center"}
      sx={{
        backgroundColor: "primary.white",
        padding: "5px 10px",
        borderRadius: "10px",
        width: { xs: "140px", md: "220px", lg: "230px" },
      }}
    >
      {icon}
      <Typography variant="caption" color={"primary.main"} mt={"3px"}>
        {tagName}
      </Typography>
      <Typography
        variant="body1"
        fontWeight={700}
        color={"primary.main"}
        textAlign={"center"}
        noWrap
      >
        {statusMap.get(tagVal) || tagVal}
      </Typography>
    </Stack>
  );
};

const statusMap = new Map([
  ["waiting", "Ожидает начала"],
  ["open", "Введется набор"],
  ["closed", "Набор закрыт"],
]);
