import React, { useEffect, useState } from "react";
import "./EventGallery.css";
import { Box, Container, Stack, Typography } from "@mui/material";
import camera from "../../../media/images/camera.png";
import Slider from "react-slick";
import GalleryPageModal from "../../GalleryPageSection/GalleryPageModal/GalleryPageModal";

export default function EventGallery({ eventGallery = [] }) {
  console.log({ eventGallery });
  const [emptySlides, setEmptySlides] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const [isDragging, setIsDragging] = React.useState(false);
  const sliderRef = React.useRef(null);

  const settings = {
    dots: false,
    infinite: eventGallery.length > 1 ? true : false,
    speed: 500,
    slidesToShow: 5,
    // swipeToSlide: true,
    autoplay: true,
    autoplaySpeed: 2000,
    pauseOnHover: true,

    arrows: false,
    className: "eventGallery-slider",
    beforeChange: () => {
      setIsDragging(true);
    },
    afterChange: () => {
      setIsDragging(false);
    },
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
  };

  useEffect(() => {
    if (!sliderRef.current) return;

    const currentSlidesToShow =
      settings.responsive.find(
        (item) => item.breakpoint === sliderRef.current.state.breakpoint
      )?.settings?.slidesToShow || settings.slidesToShow;

    if (eventGallery.length < currentSlidesToShow) {
      setEmptySlides(
        new Array(currentSlidesToShow - eventGallery.length).fill(<div></div>)
      );
    } else setEmptySlides([]);
  }, [eventGallery, sliderRef.current]);

  return (
    <Box
      sx={{
        width: "100%",
        bgcolor: "primary.white",
        boxShadow: "0 0 20px 5px rgba(50, 79, 123, 0.25)",
        borderRadius: { xs: "20px", sm: "30px", md: "40px" },
        padding: "20px 0",
      }}
    >
      <Container>
        <Stack direction={"column"} gap={"20px"}>
          <Typography variant="h5" color={"primary.main"}>
            Галерея мероприятия
          </Typography>
          <Box position={"relative"} width={"100%"}>
            {eventGallery.length > 0 ? (
              <Slider
                {...settings}
                ref={(slider) => (sliderRef.current = slider)}
              >
                {[...eventGallery, ...emptySlides].map((item, index) => (
                  <Box key={item?.id + index} height={"100%"}>
                    {item?.id ? (
                      <GalleryCard
                        image={item}
                        setSelectedIndex={() => setSelectedIndex(index)}
                        isDragging={isDragging}
                      />
                    ) : (
                      <div></div>
                    )}
                  </Box>
                ))}
              </Slider>
            ) : (
              <Stack direction={"column"} gap={"10px"} alignItems={"center"}>
                <Stack
                  justifyContent={"center"}
                  alignItems={"center"}
                  sx={{
                    padding: "30px",
                    borderRadius: "100%",
                    bgcolor: "secondary.main",
                  }}
                >
                  <img src={camera} alt="camera" />
                </Stack>
                <Typography
                  variant="subtitle1"
                  color={"primary.main"}
                  sx={{ textAlign: "center" }}
                >
                  Чемпионат проводится впервые
                </Typography>
                <Typography
                  variant="subtitle1"
                  color={"primary.main"}
                  sx={{ textAlign: "center" }}
                >
                  Фото с мероприятия пока нет,
                  <br />
                  но скоро они тут появятся
                </Typography>
              </Stack>
            )}
          </Box>
        </Stack>
      </Container>
      <GalleryPageModal
        photos={eventGallery.map((photo) => photo.sourceUrl)}
        selectedIndex={selectedIndex}
        setSelectedIndex={setSelectedIndex}
      />
    </Box>
  );
}

const GalleryCard = ({ image, setSelectedIndex, isDragging }) => {
  console.log({ image });
  return (
    <Stack
      sx={{
        width: "100%",
        borderRadius: "20px",
        height: { xs: "30vw", sm: "100px", md: "150px", xl: "200px" },
        padding: "10px",
        background: "linear-gradient(180deg, #0b131c 30.96%, #060b11 100%)",
        position: "relative",
        overflow: "hidden",
        cursor: "pointer",
      }}
      direction={"column"}
      justifyContent={"flex-end"}
      alignItems={"flex-start"}
      onClick={() => !isDragging && setSelectedIndex()}
    >
      <img
        src={image?.sourceUrl}
        alt="winner"
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          objectFit: "cover",
          zIndex: 1,
        }}
      />
    </Stack>
  );
};
