import { Container, Stack, Typography } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React from "react";

export default function EventPartners({ eventsPartners = [] }) {
  return (
    <Container>
      <Stack
        gap={"20px"}
        direction={"column"}
        alignItems={"center"}
        justifyContent={"center"}
      >
        <Typography variant="h5" color={"primary.main"}>
          Наши партнеры
        </Typography>
        <Grid2 container spacing={"20px"} width={"100%"}>
          {eventsPartners?.map((partner) => (
            <Grid2 xs={6} sm={3} lg={2}>
              <Stack
                sx={{
                  width: "100%",
                  bgcolor: "primary.white",
                  boxShadow: "0 0 20px 5px rgba(50, 79, 123, 0.25)",
                  borderRadius: { xs: "20px" },
                  height: { xs: "80px", md: "120px", xl: "150px" },
                  cursor: "pointer",
                }}
                direction={"row"}
                justifyContent={"center"}
                alignItems={"center"}
                onClick={() =>
                  window.open(partner?.partnersfields?.link, "_blank")
                }
              >
                <img
                  height={"70%"}
                  width={"70%"}
                  alt="partner logo"
                  style={{ objectFit: "contain" }}
                  src={partner?.partnersfields?.photo?.sourceUrl}
                ></img>
              </Stack>
            </Grid2>
          ))}
        </Grid2>
      </Stack>
    </Container>
  );
}
