import { Box, Container, Stack, Typography } from "@mui/material";
import React, { useEffect } from "react";
import Slider from "react-slick";
import cubok from "../../../media/images/cubok.png";
import "./EventWinners.css";

export default function EventWinners({ eventWinners = [] }) {
  console.log({ eventWinners });
  const [emptySlides, setEmptySlides] = React.useState([]);

  const sliderRef = React.useRef(null);

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    // swipeToSlide: true,
    arrows: true,
    className: "eventWinners-slider",
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
  };

  useEffect(() => {
    if (!sliderRef.current) return;

    const currentSlidesToShow =
      settings.responsive.find(
        (item) => item.breakpoint === sliderRef.current.state.breakpoint
      )?.settings?.slidesToShow || settings.slidesToShow;

    if (eventWinners.length < currentSlidesToShow) {
      setEmptySlides(
        new Array(currentSlidesToShow - eventWinners.length).fill(<div></div>)
      );
    } else setEmptySlides([]);
  }, [eventWinners, sliderRef.current]);

  return (
    <Box
      sx={{
        width: "100%",
        bgcolor: "primary.white",
        boxShadow: "0 0 20px 5px rgba(50, 79, 123, 0.25)",
        borderRadius: { xs: "20px", sm: "30px", md: "40px" },
        padding: "20px 0",
      }}
    >
      <Container>
        <Stack direction={"column"} gap={"20px"}>
          <Typography variant="h5" color={"primary.main"}>
            Победители
          </Typography>
          <Box position={"relative"} width={"100%"}>
            {eventWinners.length > 0 ? (
              <Slider
                {...settings}
                ref={(slider) => (sliderRef.current = slider)}
              >
                {[...eventWinners, ...emptySlides].map((item, index) => (
                  <Box key={item?.id + index} height={"100%"}>
                    {item?.id ? <WinnerCard winner={item} /> : <div></div>}
                  </Box>
                ))}
              </Slider>
            ) : (
              <Stack direction={"column"} gap={"10px"} alignItems={"center"}>
                <Stack
                  justifyContent={"center"}
                  alignItems={"center"}
                  sx={{
                    padding: "30px",
                    borderRadius: "100%",
                    bgcolor: "secondary.main",
                  }}
                >
                  <img src={cubok} alt="cubok" />
                </Stack>
                <Typography
                  variant="subtitle1"
                  color={"primary.main"}
                  sx={{ textAlign: "center" }}
                >
                  Чемпионат проводится впервые
                </Typography>
                <Typography
                  variant="subtitle1"
                  color={"primary.main"}
                  sx={{ textAlign: "center" }}
                >
                  Победителей пока еще нет,
                  <br />
                  но ты можешь стать первым!
                </Typography>
              </Stack>
            )}
          </Box>
        </Stack>
      </Container>
    </Box>
  );
}

const WinnerCard = ({ winner }) => {
  console.log({ winner });
  return (
    <Stack
      sx={{
        width: "100%",
        borderRadius: "20px",
        height: { xs: "40vw", sm: "150px", md: "200px", xl: "250px" },
        padding: "10px",
        background: "linear-gradient(180deg, #0b131c 30.96%, #060b11 100%)",
        position: "relative",
        overflow: "hidden",
      }}
      direction={"column"}
      justifyContent={"flex-end"}
      alignItems={"flex-start"}
    >
      <img
        src={winner?.victoryusereventfield?.photovictoryuser?.sourceUrl}
        alt="winner"
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          objectFit: "cover",
          zIndex: 1,
        }}
      />
      <Box
        sx={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          background:
            "linear-gradient(180deg, #0B131C00 30.96%, #060b11c7 100%)",
          zIndex: 2,
        }}
      ></Box>
      <Typography variant="caption" color={"primary.white"} zIndex={3}>
        {numberMap.get(winner?.victoryusereventfield?.eventresultuser)}
      </Typography>
      <Typography color={"primary.white"} variant={"h6"} zIndex={3}>
        {winner?.victoryusereventfield?.victoryusername}
      </Typography>
      <Typography
        variant="caption"
        color={"primary.white"}
        zIndex={3}
        display={{ xs: "none", md: "block" }}
      >
        {winner?.victoryusereventfield?.descriptionvictorydate}
      </Typography>
    </Stack>
  );
};

const numberMap = new Map([
  ["one", "1 место"],
  ["two", "2 место"],
  ["three", "3 место"],
]);
