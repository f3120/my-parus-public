import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

import React, { useState } from "react";
import { correctingPrice } from "../../Shop/ShopItemList/ShopItem/ShopItem";
import { navigate } from "gatsby";

export default function CartSummarySection({ cartItems = [] }) {
  const [expanded, setExpanded] = React.useState(false);

  return (
    <Box
      sx={{
        backgroundColor: "secondary.main",
        borderRadius: "20px",
        padding: "15px 0",
        width: { xs: "100%", lg: "45%" },
        marginRight: { xs: "0", lg: "40px" },
        position: { xs: "fixed", sm: "static" },
        bottom: { xs: 0, sm: "auto" },
        zIndex: 1002,
      }}
    >
      <Container sx={{ padding: { lg: "0 10px" } }}>
        <Stack
          direction={"column"}
          justifyContent={"space-between"}
          alignItems={"center"}
        >
          <Stack
            width={"100%"}
            direction={"row"}
            justifyContent={"space-between"}
            alignItems={"center"}
            sx={{ cursor: { xs: "pointer", lg: "auto" } }}
            onClick={() => setExpanded(!expanded)}
          >
            <Typography variant="h5" color={"primary.main"}>
              Общая сумма
            </Typography>
            <Stack
              direction={"row"}
              gap={"10px"}
              justifyContent={"flex-end"}
              alignItems={"center"}
            >
              <Typography variant="h5" color={"primary.main"}>
                {cartItems.reduce(
                  (a, b) =>
                    a +
                    (b?.regularPrice
                      ? correctingPrice(
                          b?.onSale ? b?.price : b?.regularPrice
                        ) * b?.quantity
                      : 0),
                  0
                )}{" "}
                ₽
              </Typography>
              <KeyboardArrowDownIcon
                sx={{
                  color: "primary.main",
                  transform: expanded ? "rotate(-180deg)" : "none",
                  display: { xs: "block", lg: "none" },
                  transition: "all 0.3s ease",
                }}
              />
            </Stack>
          </Stack>
          <Stack
            direction={"column"}
            gap={"15px"}
            width={"100%"}
            justifyContent={"center"}
            alignItems={"center"}
            sx={{
              mt: { xs: expanded ? "15px" : "0px", lg: "15px" },
              transition: expanded ? "all 0.7s ease" : "all 0.3s ease",
              maxHeight: { xs: expanded ? "500px" : "0px", lg: "500px" },
              overflow: "hidden",
            }}
          >
            <Box
              width={"100%"}
              height={"1px"}
              sx={{ backgroundColor: "primary.main", opacity: "0.3" }}
            ></Box>
            <Stack
              direction={"row"}
              justifyContent={"space-between"}
              width={"100%"}
              alignItems={"center"}
            >
              <Typography variant="body1" color={"primary.main"}>
                Количество товаров
              </Typography>
              <Typography
                variant="body1"
                fontWeight={900}
                color={"primary.main"}
              >
                {cartItems.filter((item) => item?.quantity).length}
              </Typography>
            </Stack>
            <Stack
              direction={"row"}
              justifyContent={"space-between"}
              width={"100%"}
              alignItems={"center"}
            >
              <Typography variant="body1" color={"primary.main"}>
                Сумма
              </Typography>
              <Typography
                variant="body1"
                fontWeight={900}
                color={"primary.main"}
              >
                {cartItems.reduce(
                  (a, b) =>
                    a +
                    (b?.regularPrice
                      ? correctingPrice(b?.regularPrice) * b?.quantity
                      : 0),
                  0
                )}{" "}
                ₽
              </Typography>
            </Stack>
            <Stack
              direction={"row"}
              justifyContent={"space-between"}
              width={"100%"}
              alignItems={"center"}
            >
              <Typography variant="body1" color={"primary.main"}>
                С учетом скидок
              </Typography>
              <Typography
                variant="body1"
                fontWeight={900}
                color={"primary.main"}
              >
                {cartItems.reduce(
                  (a, b) =>
                    a +
                    (b?.regularPrice
                      ? correctingPrice(
                          b?.onSale ? b?.price : b?.regularPrice
                        ) * b?.quantity
                      : 0),
                  0
                )}{" "}
                ₽
              </Typography>
            </Stack>
            <Box
              width={"100%"}
              height={"1px"}
              sx={{ backgroundColor: "primary.main", opacity: "0.3" }}
            ></Box>
            <Stack
              direction={"row"}
              justifyContent={"space-between"}
              width={"100%"}
              alignItems={"center"}
            >
              <Typography variant="body1" color={"primary.main"}>
                Общая сумма
              </Typography>
              <Typography
                variant="body1"
                fontWeight={900}
                color={"primary.main"}
              >
                {cartItems.reduce(
                  (a, b) =>
                    a +
                    (b?.regularPrice
                      ? correctingPrice(
                          b?.onSale ? b?.price : b?.regularPrice
                        ) * b?.quantity
                      : 0),
                  0
                )}{" "}
                ₽
              </Typography>
            </Stack>
          </Stack>
          <Button
            disabled={!cartItems?.length}
            fullWidth
            variant="contained"
            onClick={() => {
              navigate("/cart/order");
            }}
            sx={{ mt: "15px" }}
          >
            <Typography variant="subtitle1">К оформлению</Typography>
          </Button>
        </Stack>
      </Container>
    </Box>
  );
}
