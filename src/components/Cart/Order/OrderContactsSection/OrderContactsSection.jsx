import {
  Box,
  Button,
  Checkbox,
  Container,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useContext, useEffect, useState } from "react";
import { AddressSuggestions } from "react-dadata";
import DeliveryBlock from "./DeliveryBlock/DeliveryBlock";
import { mainTheme } from "../../../../config/MUI/mainTheme";
import { Link } from "gatsby";
import "./OrderContactsSection.css";
import { gql, useMutation } from "@apollo/client";
import OrderStatusModal from "./OrderStatusModal/OrderStatusModal";
import { ModalContext } from "../../../../context/ModalContext";
import { CartContext } from "../../../../context/CartContext";

export const APOLLO_MUTATION = gql`
  mutation MyMutation(
    $billing: CustomerAddressInput
    $lineItems: [LineItemInput]
  ) {
    createOrder(
      input: {
        billing: $billing
        lineItems: $lineItems
        shipping: $billing
        status: PROCESSING
      }
    ) {
      clientMutationId
      orderId
    }
  }
`;

export default function OrderContactsSection({
  cartItems,
  cdekDekiveryData,
  setCdekDekiveryData,
}) {
  const { setModalActive } = useContext(ModalContext);
  const { clearCart } = useContext(CartContext);

  const [handleMutate, { data, loading, error }] = useMutation(APOLLO_MUTATION);

  const [value, setValue] = useState();

  const [selectedDeliveryType, setSelectedDeliveryType] =
    React.useState("pickup");

  const [formData, setFormData] = useState({
    firstName: undefined,
    lastName: undefined,
    email: undefined,
    phone: undefined,
    delivery: undefined,
    adress: {
      address1: undefined,
      city: undefined,
      country: "RU",
      postcode: undefined,
    },
    comment: undefined,
    agree: false,
  });

  useEffect(() => {
    console.log({ value });
    setFormData({
      ...formData,
      adress: {
        address1: value?.unrestricted_value || undefined,
        city: value?.data?.city || undefined,
        country: "RU",
        postcode: value?.data?.postal_code || undefined,
      },
    });
  }, [value]);

  useEffect(() => {
    if (!loading && (data || error)) {
      setModalActive("orderStatus");
      if (!error) {
        setFormData({
          firstName: undefined,
          lastName: undefined,
          email: undefined,
          phone: undefined,
          delivery: undefined,
          adress: {
            address1: undefined,
            city: undefined,
            country: "RU",
            postcode: undefined,
          },
          comment: undefined,
          agree: false,
        });
        clearCart();
      }
    }
  }, [loading]);

  return (
    <Box
      sx={{
        order: { xs: 2, lg: 1 },
        backgroundColor: "secondary.main",
        borderRadius: "20px",
        padding: "15px 0",
        width: { xs: "100%" },
      }}
    >
      <Container sx={{ padding: { lg: "0 10px" } }}>
        <Stack direction={"column"} gap={"10px"} width={"100%"}>
          <Typography variant="h5" color={"primary.main"}>
            Контактные данные
          </Typography>
          <TextField
            error={formData.name === ""}
            label="Имя"
            required
            variant="filled"
            fullWidth
            size="small"
            value={formData.name}
            sx={{
              "& input": {
                backgroundColor: "primary.white",
              },
            }}
            onChange={(e) =>
              setFormData({ ...formData, firstName: e.target.value })
            }
          />
          <TextField
            error={formData.name === ""}
            label="Фамилия"
            required
            variant="filled"
            fullWidth
            size="small"
            value={formData.name}
            sx={{
              "& input": {
                backgroundColor: "primary.white",
              },
            }}
            onChange={(e) =>
              setFormData({ ...formData, lastName: e.target.value })
            }
          />

          <TextField
            error={formData.phone === ""}
            required
            label="Телефон"
            variant="filled"
            fullWidth
            size="small"
            value={formData.phone}
            sx={{
              "& input": {
                backgroundColor: "primary.white",
              },
            }}
            onChange={(e) =>
              setFormData({ ...formData, phone: e.target.value })
            }
          />
          <TextField
            error={formData.email === ""}
            required
            label="E-mail"
            variant="filled"
            fullWidth
            size="small"
            value={formData.email}
            sx={{
              "& input": {
                backgroundColor: "primary.white",
              },
            }}
            onChange={(e) =>
              setFormData({ ...formData, email: e.target.value })
            }
          />
          <Typography variant="h5" color={"primary.main"}>
            Доставка
          </Typography>
          <DeliveryBlock
            cartItems={cartItems}
            selectedDeliveryType={selectedDeliveryType}
            setSelectedDeliveryType={setSelectedDeliveryType}
            cdekDekiveryData={cdekDekiveryData}
            setCdekDekiveryData={setCdekDekiveryData}
          />
          {selectedDeliveryType === "post" && (
            <>
              <AddressSuggestions
                inputProps={{
                  required: true,
                  placeholder: "Адрес",
                  style: {
                    fontFamily: "Inter",
                    color: mainTheme.palette.primary.main,
                    border: "none",
                    boxShadow: "none",
                  },
                }}
                token="3b10b4e7fb549892c86bd896f04930039360d488"
                value={value}
                onChange={setValue}
              />
              {formData.adress?.address1 && !formData.adress?.postcode ? (
                <Typography variant="body1" color={"primary.main"} ml={"5px"}>
                  Введите полный адресс
                </Typography>
              ) : (
                <Typography variant="body1" color={"primary.main"} ml={"5px"}>
                  Почтовый индекс: {formData.adress?.postcode}
                </Typography>
              )}
            </>
          )}
          <TextField
            label="Комментарий"
            multiline
            sx={{
              "& textarea": {
                backgroundColor: mainTheme.palette.primary.white,
              },
              "& .MuiInputBase-multiline": {
                backgroundColor: mainTheme.palette.primary.white,
                "&.Mui-focused": {
                  backgroundColor: mainTheme.palette.primary.white,
                },
                "&:hover": {
                  backgroundColor: mainTheme.palette.primary.white,
                },
              },
            }}
            variant="filled"
            fullWidth
            size="small"
            value={formData.comment}
            onChange={(e) =>
              setFormData({ ...formData, comment: e.target.value })
            }
          />
          <Stack
            direction={"row"}
            justifyContent={"center"}
            alignItems={"center"}
            width={"100%"}
          >
            <Checkbox
              checked={formData.agree}
              onChange={(e) =>
                setFormData({ ...formData, agree: e.target.checked })
              }
            />
            <Typography variant="body1" color={"primary.main"}>
              Я согласен на обработку{" "}
              <Typography
                onClick={() => setModalActive("privacyPolicy")}
                component={"span"}
                color={"primary.main"}
                sx={{ textDecoration: "underline", cursor: "pointer" }}
              >
                персональных данных
              </Typography>{" "}
              и с условиями{" "}
              <Typography
                color={"primary.main"}
                component={"span"}
                onClick={() => setModalActive("userAgreement")}
                sx={{ textDecoration: "underline", cursor: "pointer" }}
              >
                пользовательского соглашения
              </Typography>
            </Typography>
          </Stack>
          <Button
            fullWidth
            variant="contained"
            onClick={() =>
              // console.log({
              //   dataToMutate: {
              //     variables: {
              //       billing: {
              //         address1: formData.adress?.address1,
              //         city: formData.adress?.city,
              //         country: "RU",
              //         email: formData.email,
              //         firstName: formData.firstName,
              //         lastName: formData.lastName,
              //         phone: formData.phone,
              //         postcode: formData.adress?.postcode,
              //       },
              //       lineItems: cartItems.map((item) => ({
              //         productId: item?.productId,
              //         variationId: item?.selectedSize?.databaseId,
              //         quantity: item?.quantity,
              //         name: item?.name,
              //       })),
              //     },
              //   },
              // })
              handleMutate({
                variables: {
                  billing: {
                    address1:
                      deliveryTypeMap.get(selectedDeliveryType) +
                      " " +
                      (selectedDeliveryType === "cdek"
                        ? cdekDekiveryData?.adress?.address1 +
                          " " +
                          cdekTypeMap.get(cdekDekiveryData?.type) +
                          " " +
                          cdekDekiveryData?.tariff?.delivery_sum +
                          " " +
                          cdekDekiveryData?.tariff?.tariff_name
                        : formData.adress?.address1),
                    city:
                      selectedDeliveryType === "cdek"
                        ? cdekDekiveryData?.adress?.city
                        : formData.adress?.city,
                    country: "RU",
                    email: formData.email,
                    firstName: formData.firstName,
                    lastName: formData.lastName,
                    phone: formData.phone,
                    postcode:
                      selectedDeliveryType === "cdek"
                        ? cdekDekiveryData?.adress?.postcode
                        : formData.adress?.postcode,
                  },
                  lineItems: cartItems.map((item) => ({
                    productId: item?.productId,
                    variationId: item?.selectedSize?.databaseId,
                    quantity: item?.quantity,
                    name: item?.selectedSize?.name || item?.name,
                  })),
                },
              })
            }
            disabled={
              cartItems?.length < 1 ||
              !formData.agree ||
              !formData.firstName ||
              !formData.lastName ||
              !formData.phone ||
              !formData.email ||
              (!formData.adress?.postcode && selectedDeliveryType === "post") ||
              (selectedDeliveryType === "cdek" &&
                !cdekDekiveryData?.adress?.postcode)
            }
          >
            Оформить заказ
          </Button>
        </Stack>
      </Container>
      <OrderStatusModal
        status={error ? "error" : "success"}
        message={error?.message}
      />
    </Box>
  );
}

const deliveryTypeMap = new Map([
  ["post", "(Почта)"],
  ["pickup", "(Самовывоз)"],
  ["cdek", "(СДЕК)"],
]);

const cdekTypeMap = new Map([
  ["office", "(До офиса)"],
  ["door", "(До двери)"],
]);
