import { Radio, Stack, Typography } from "@mui/material";
import React, { useContext, useEffect, useState } from "react";
import { ModalContext } from "../../../../../context/ModalContext";

export default function DeliveryBlock({
  cartItems = [],
  selectedDeliveryType,
  setSelectedDeliveryType,
  cdekDekiveryData,
  setCdekDekiveryData,
}) {
  useEffect(() => {
    const script1 = document.createElement("script");
    script1.src = "https://cdn.jsdelivr.net/npm/@cdek-it/widget@3";
    script1.async = true;
    document.body.appendChild(script1);

    const script2 = document.createElement("script");
    script2.src = "https://cdn.jsdelivr.net/npm/@unocss/runtime";
    script2.async = true;
    document.body.appendChild(script2);

    const initWidget = () => {
      if (window.CDEKWidget) {
        new window.CDEKWidget({
          from: "Ялта",
          root: "cdek-map",
          apiKey: "01d82c49-55d3-45c7-9759-3e9cf7ec977e",
          servicePath: "https://my-sail.ru/wordpress_admin/service.php",
          // servicePath: "https://widget.cdek.ru/service.php",
          // servicePath: "http://localhost/service.php",

          defaultLocation: "Ялта",
          goods: cartItems.map((item) => ({
            weight: item?.weight || 30,
            length: item?.length || 30,
            width: item?.width || 200,
            height: item?.height || 10,
          })),
          onChoose: function (_type, tariff, address) {
            console.log({
              type: _type,
              tariff,
              adress: {
                address1: address?.city + ", " + address?.address || undefined,
                city: address?.city || undefined,
                country: "RU",
                postcode: address?.postal_code || undefined,
              },
            });
            setCdekDekiveryData({
              type: _type,
              tariff,
              adress: {
                address1: address?.city + ", " + address?.address || undefined,
                city: address?.city || undefined,
                country: "RU",
                postcode: address?.postal_code || undefined,
              },
            });
          },
        });
      } else {
        setTimeout(initWidget, 100);
      }
    };

    initWidget();

    return () => {
      document.body.removeChild(script1);
      document.body.removeChild(script2);
    };
  }, []);

  return (
    <Stack sx={{ width: "100%" }} direction={"column"} gap={"10px"}>
      <Stack
        sx={{
          width: "100%",
          backgroundColor: "primary.white",
          padding: "15px 10px",
          borderRadius: "20px",
        }}
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"flex-start"}
      >
        <Radio
          checked={selectedDeliveryType === "pickup"}
          onChange={(e) => setSelectedDeliveryType(e.target.value)}
          value="pickup"
          name="radio-buttons"
        />
        <Stack direction={"column"} gap={"10px"} width={"100%"}>
          <Stack
            direction={{ xs: "column", sm: "row" }}
            gap={"5px"}
            width={"100%"}
            justifyContent={{ sm: "space-between" }}
          >
            <Typography variant="subtitle1" color={"primary.main"}>
              Самовывоз
            </Typography>
            <Typography
              variant="subtitle1"
              fontWeight={900}
              color={"primary.main"}
            >
              +0.00 ₽
            </Typography>
          </Stack>
          <Typography variant="caption" color={"primary.main"}>
            Заберите покупку лично в офисе в Крыму по адресу: Республика Крым,
            г. Ялта, с. Оползневое, ул. Генерала Острякова, д.9,
            <br />
            яхтенный порт "Имеретинский"
          </Typography>
        </Stack>
      </Stack>
      <Stack
        sx={{
          width: "100%",
          backgroundColor: "primary.white",
          padding: "15px 10px",
          borderRadius: "20px",
        }}
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"flex-start"}
      >
        <Radio
          checked={selectedDeliveryType === "post"}
          onChange={(e) => setSelectedDeliveryType(e.target.value)}
          value="post"
          name="radio-buttons"
        />
        <Stack direction={"column"} gap={"10px"} width={"100%"}>
          <Stack
            direction={{ xs: "column", sm: "row" }}
            gap={"5px"}
            width={"100%"}
            justifyContent={{ sm: "space-between" }}
          >
            <Typography variant="subtitle1" color={"primary.main"}>
              Почта
            </Typography>
            <Typography
              variant="subtitle1"
              fontWeight={900}
              color={"primary.main"}
            >
              Индивидуально
            </Typography>
          </Stack>
          <Typography variant="caption" color={"primary.main"}>
            Укажите выш адресс, и заберите покупку на почте.
            <br />
            Стоимость рассчитывается индивидуально
          </Typography>
        </Stack>
      </Stack>
      <Stack
        sx={{
          width: "100%",
          backgroundColor: "primary.white",
          padding: "15px 10px",
          borderRadius: "20px",
        }}
        direction={"column"}
        gap={"10px"}
      >
        <Stack
          sx={{
            width: "100%",
          }}
          direction={"row"}
          justifyContent={"space-between"}
          alignItems={"flex-start"}
        >
          <Radio
            checked={selectedDeliveryType === "cdek"}
            onChange={(e) => setSelectedDeliveryType(e.target.value)}
            value="cdek"
            name="radio-buttons"
          />
          <Stack direction={"column"} gap={"10px"} width={"100%"}>
            <Stack
              direction={{ xs: "column", sm: "row" }}
              gap={"5px"}
              width={"100%"}
              justifyContent={{ sm: "space-between" }}
            >
              <Typography variant="subtitle1" color={"primary.main"}>
                СДЕК
              </Typography>
              <Typography
                variant="subtitle1"
                fontWeight={900}
                color={"primary.main"}
              >
                {cdekDekiveryData?.tariff?.delivery_sum
                  ? cdekDekiveryData?.tariff?.delivery_sum + " ₽"
                  : "Индивидуально"}
              </Typography>
            </Stack>
            <Typography variant="caption" color={"primary.main"}>
              Доставка в пункты выдачи СДЕК
            </Typography>
          </Stack>
        </Stack>
        <div
          id="cdek-map"
          style={{
            width: "100%",
            height: "70vh",
            display: selectedDeliveryType === "cdek" ? "block" : "none",
            borderRadius: "10px",
            overflow: "hidden",
            zIndex: "1",
          }}
        ></div>
      </Stack>
    </Stack>
  );
}
