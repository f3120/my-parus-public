import { Box, Button, Dialog, Stack, Typography } from "@mui/material";
import React, { useContext } from "react";
import { ModalContext } from "../../../../../context/ModalContext";
import { ReactComponent as SuccesIcon } from "../../../../../media/icons/succesIcon.svg";
import { navigate } from "gatsby";

const modalID = "orderStatus";
export default function OrderStatusModal({ status, message }) {
  const { isModalActive, setModalInactive } = useContext(ModalContext);
  return (
    <Dialog
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "15px",
        },
      }}
    >
      <Stack
        direction={"column"}
        padding={"20px"}
        gap={"20px"}
        alignItems={"center"}
        maxWidth={"400px"}
      >
        {status === "success" ? (
          <>
            <Stack
              sx={{
                width: "100px",
                height: "100px",
                borderRadius: "100%",
                backgroundColor: "secondary.main",
              }}
              alignItems={"center"}
              justifyContent={"center"}
            >
              <Stack
                sx={{
                  width: "50px",
                  height: "50px",
                  borderRadius: "100%",
                  backgroundColor: "primary.main",
                }}
                alignItems={"center"}
                justifyContent={"center"}
              >
                <SuccesIcon width={"24px"} height={"24px"} />
              </Stack>
            </Stack>

            <Typography
              variant="h4"
              color={"primary.main"}
              textAlign={"center"}
            >
              Ваш заказ принят
            </Typography>

            <Stack direction={"column"} gap={"5px"}>
              <Typography
                variant="body1"
                color={"primary.main"}
                textAlign={"center"}
              >
                Уважаемый покупатель, рады сообщить, что Ваш заказ{" "}
                <strong>принят</strong> в обработку
              </Typography>
              <Typography
                variant="body1"
                color={"primary.main"}
                textAlign={"center"}
              >
                В ближайшее время с Вами <strong>свяжутся</strong> для{" "}
                <strong>подтверждения</strong> заказа
              </Typography>
              <Typography
                variant="body1"
                color={"primary.main"}
                textAlign={"center"}
              >
                Спасибо, что Вы с нами!
              </Typography>
              <Button
                sx={{
                  padding: "7px 20px",
                }}
                variant="contained"
                onClick={() => {
                  navigate("/shop");
                  setModalInactive(modalID);
                }}
              >
                Отлично
              </Button>
            </Stack>
          </>
        ) : (
          <>
            <Typography
              variant="h4"
              color={"primary.main"}
              textAlign={"center"}
            >
              Что то пошло не так
            </Typography>
            <Typography
              variant="body1"
              color={"primary.main"}
              textAlign={"center"}
            >
              {message}
            </Typography>
          </>
        )}
      </Stack>
    </Dialog>
  );
}
