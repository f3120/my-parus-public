import { Box, Dialog, Stack, Typography } from "@mui/material";
import React, { useContext, useEffect } from "react";
import { ModalContext } from "../../../../context/ModalContext";

const modalID = "cdekModal";
export default function OrderCdekModal({}) {
  const { activeModals, isModalActive, setModalInactive } =
    useContext(ModalContext);

  useEffect(() => {
    const script1 = document.createElement("script");
    script1.src = "https://cdn.jsdelivr.net/npm/@cdek-it/widget@3";
    script1.async = true;
    document.body.appendChild(script1);

    const script2 = document.createElement("script");
    script2.src = "https://cdn.jsdelivr.net/npm/@unocss/runtime";
    script2.async = true;
    document.body.appendChild(script2);

    const initWidget = () => {
      if (isModalActive(modalID) && window.CDEKWidget) {
        new window.CDEKWidget({
          from: "Новосибирск",
          root: "cdek-map",
          apiKey: "f4e034c2-8c37-4168-8b97-99b6b3b268d7",
          servicePath: "https://my-sail.ru/wordpress_admin/service.php",
          // servicePath: "https://widget.cdek.ru/service.php",
          // servicePath: "http://localhost/service.php",

          defaultLocation: "Новосибирск",
        });
      } else {
        setTimeout(initWidget, 100);
      }
    };

    initWidget();

    return () => {
      document.body.removeChild(script1);
      document.body.removeChild(script2);
    };
  }, [activeModals]);

  return (
    <Dialog
      open={isModalActive(modalID)}
      onClose={() => setModalInactive(modalID)}
      PaperProps={{
        sx: {
          borderRadius: "15px",
          overflow: "hidden",

          maxWidth: "70vw",
        },
      }}
    >
      <div id="cdek-map" style={{ width: "70vw", height: "70vh" }}></div>
    </Dialog>
  );
}
