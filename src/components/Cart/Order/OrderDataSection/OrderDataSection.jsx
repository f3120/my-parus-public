import { Box, Button, Container, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import { correctingPrice } from "../../../Shop/ShopItemList/ShopItem/ShopItem";
import CartItem from "../../CartItemsSection/CartItem/CartItem";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

export default function OrderDataSection({
  cartItems = [],
  cdekDekiveryData,
  updateCartItem,
  deleteCartItem,
}) {
  const [expanded, setExpanded] = React.useState(false);
  return (
    <Box
      sx={{
        backgroundColor: "secondary.main",
        borderRadius: "20px",
        padding: "15px 0",
        width: { xs: "100%", lg: "45%" },
        marginRight: { xs: "0", lg: "40px" },
        order: { xs: 1, lg: 2 },
        cursor: { xs: "pointer", lg: "default" },
      }}
      onClick={() => setExpanded(!expanded)}
    >
      <Container sx={{ padding: { lg: "0 10px" } }}>
        <Stack
          direction={"column"}
          justifyContent={"space-between"}
          alignItems={"center"}
        >
          <Stack
            width={"100%"}
            direction={"row"}
            justifyContent={"space-between"}
            alignItems={"center"}
          >
            <Typography variant="subtitle1" color={"primary.main"}>
              Итог
            </Typography>
            <Stack
              direction={"row"}
              gap={"10px"}
              justifyContent={"flex-end"}
              alignItems={"center"}
            >
              <Typography variant="h5" color={"primary.main"}>
                {cartItems.reduce(
                  (a, b) =>
                    a +
                    (b?.regularPrice
                      ? correctingPrice(
                          b?.onSale ? b?.price : b?.regularPrice
                        ) * b?.quantity
                      : 0),
                  0
                ) + (cdekDekiveryData?.tariff?.delivery_sum || 0)}{" "}
                ₽
              </Typography>
              <KeyboardArrowDownIcon
                sx={{
                  color: "primary.main",
                  transform: expanded ? "rotate(-180deg)" : "none",
                  display: { xs: "block", lg: "none" },
                  transition: "all 0.3s ease",
                }}
              />
            </Stack>
          </Stack>

          <Stack
            width={"100%"}
            sx={{
              mt: { xs: expanded ? "15px" : "0px", lg: "15px" },
              transition: expanded ? "all 0.7s ease" : "all 0.3s ease",
              maxHeight: { xs: expanded ? "500px" : "0px", lg: "500px" },
              overflow: "hidden",
            }}
          >
            <Stack
              direction={"column"}
              gap={"15px"}
              width={"100%"}
              justifyContent={"center"}
              alignItems={"center"}
              sx={{
                mt: "15px",

                overflow: "hidden",
              }}
            >
              <Box
                width={"100%"}
                height={"1px"}
                sx={{ backgroundColor: "primary.main", opacity: "0.3" }}
              ></Box>
              <Stack
                direction={"row"}
                justifyContent={"space-between"}
                width={"100%"}
                alignItems={"center"}
              >
                <Typography variant="body1" color={"primary.main"}>
                  Сумма
                </Typography>
                <Typography
                  variant="body1"
                  fontWeight={700}
                  color={"primary.main"}
                >
                  {cartItems.reduce(
                    (a, b) =>
                      a +
                      (b?.regularPrice
                        ? correctingPrice(
                            b?.onSale ? b?.price : b?.regularPrice
                          ) * b?.quantity
                        : 0),
                    0
                  )}{" "}
                  ₽
                </Typography>
              </Stack>
              <Stack
                direction={"row"}
                justifyContent={"space-between"}
                width={"100%"}
                alignItems={"center"}
              >
                <Typography variant="body1" color={"primary.main"}>
                  Стоимость доставки
                </Typography>
                <Typography
                  variant="body1"
                  fontWeight={700}
                  color={"primary.main"}
                >
                  {cdekDekiveryData?.tariff?.delivery_sum
                    ? cdekDekiveryData?.tariff?.delivery_sum + " ₽"
                    : "Индивидуально"}
                </Typography>
              </Stack>
              <Box
                width={"100%"}
                height={"1px"}
                sx={{ backgroundColor: "primary.main", opacity: "0.3" }}
              ></Box>
            </Stack>
            <Stack
              width={"100%"}
              mt={"15px"}
              direction={"column"}
              gap={"15px"}
              justifyContent={"center"}
              alignItems={"center"}
            >
              {cartItems
                .filter((cartItem) => cartItem && cartItem?.quantity)
                .map((cartItem) => (
                  <>
                    <CartItem
                      cartItem={cartItem}
                      setCartItem={(item) => updateCartItem(item)}
                      key={cartItem.id}
                      deleteCartItem={deleteCartItem}
                      viewOnly
                    />
                    <Box
                      width={"100%"}
                      height={"3px"}
                      sx={{ backgroundColor: "secondary.main" }}
                    ></Box>
                  </>
                ))}
            </Stack>
          </Stack>
        </Stack>
      </Container>
    </Box>
  );
}
