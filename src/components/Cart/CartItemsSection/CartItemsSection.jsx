import { Box, Container, Stack, Typography } from "@mui/material";
import React from "react";
import CartItem from "./CartItem/CartItem";

export default function CartItemsSection({
  cartItems = [],
  updateCartItem,
  deleteCartItem,
}) {
  return (
    <Stack
      direction={"column"}
      gap={"20px"}
      width={"100%"}
      padding={{ xs: "20px", sm: "0" }}
    >
      {cartItems
        .filter((cartItem) => cartItem && cartItem?.quantity)
        .map((cartItem) => (
          <>
            <CartItem
              cartItem={cartItem}
              setCartItem={(item) => updateCartItem(item)}
              key={cartItem.id}
              deleteCartItem={deleteCartItem}
            />
            <Box
              width={"100%"}
              height={"3px"}
              sx={{ backgroundColor: "secondary.main" }}
            ></Box>
          </>
        ))}
    </Stack>
  );
}
