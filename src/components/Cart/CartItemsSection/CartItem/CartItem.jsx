import { gql, useQuery } from "@apollo/client";
import {
  Box,
  CircularProgress,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { correctingPrice } from "../../../Shop/ShopItemList/ShopItem/ShopItem";
import { mainTheme } from "../../../../config/MUI/mainTheme";
import { navigate } from "gatsby";

export const ProductQuery = gql`
  query ($id: ID!) {
    product(id: $id) {
      type
      id
      name
      onSale
      description
      productTags {
        nodes {
          name
        }
      }
      galleryImages {
        nodes {
          sourceUrl
        }
      }
      ... on SimpleProduct {
        description
        price(format: RAW)
        productId
        regularPrice(format: RAW)
        stockStatus
        stockQuantity
        weight
        width
        height
        length
      }
      ... on VariableProduct {
        price(format: RAW)
        productId
        regularPrice(format: RAW)
        stockStatus
        stockQuantity
        variations {
          nodes {
            name
            id
            databaseId
            stockStatus
            stockQuantity
          }
        }
        id
        weight
        width
        height
        length
      }
      related {
        edges {
          node {
            id
            name
            image {
              sourceUrl
            }
            ... on SimpleProduct {
              enclosure
              price(format: RAW)
              regularPrice(format: RAW)
              salePrice
              id
              productId
              onSale
              shippingRequired
              weight
              width
              height
              length
            }
            ... on VariableProduct {
              price(format: RAW)
              onSale
              regularPrice(format: RAW)
              productId
              weight
              width
              height
              length
            }
          }
        }
      }
      attributes {
        nodes {
          attributeId
          id
          label
          name
          options
          position
          scope
          variation
          visible
        }
      }
      image {
        sourceUrl
      }
      productCategories {
        nodes {
          slug
        }
      }
    }
  }
`;
export default function CartItem({
  cartItem,
  setCartItem,
  viewOnly,
  deleteCartItem,
}) {
  const { loading, error, data } = useQuery(ProductQuery, {
    variables: { id: cartItem?.id || "" },
  });

  const product = data?.product;

  useEffect(() => {
    product &&
      setCartItem({
        ...cartItem,
        price: product?.price,
        onSale: product?.onSale,
        regularPrice: product?.regularPrice,
        productId: product?.productId,
        quantity: cartItem?.quantity,
        name: product?.name,
        weight: product?.weight * 1000,
        length: product?.length,
        width: product?.width,
        height: product?.height,
      });
  }, [product]);

  const {
    name,
    onSale,
    price,
    regularPrice,
    description,
    galleryImages,
    productTags,
    type,
    //type = "SIMPLE" || "VARIABLE"
    related,
    variations,
    productCategories,
    stockStatus,
    // stockStatus = "IN_STOCK" || "OUT_OF_STOCK"
  } = product || {};

  const upSm = useMediaQuery(mainTheme.breakpoints.up("sm"));

  if (loading) {
    return (
      <Stack alignItems={"center"} justifyContent={"center"}>
        <CircularProgress />
      </Stack>
    );
  }

  const handleNavigate = () => {
    const url = `/shop/product/?ID=${cartItem?.id}`;
    navigate(url, {
      state: {
        id: cartItem?.id,
        name,
        productCategory: productCategories?.nodes[0]?.slug,
      },
    });
  };

  return (
    <Stack
      width={"100%"}
      direction={{ xs: "column", sm: "row" }}
      gap={"20px"}
      alignItems={"center"}
      justifyContent={"space-between"}
    >
      <Stack
        width={{ xs: "100%", sm: "103px" }}
        direction={"row"}
        alignItems={viewOnly ? "normal" : "flex-start"}
        gap={"10px"}
        justifyContent={"flex-start"}
      >
        <Box
          height={{ xs: viewOnly ? "80px" : "137px" }}
          width={{ xs: viewOnly ? "80px" : "40%", sm: "100%" }}
          sx={{
            borderRadius: "20px",
            display: "flex !important",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#0000000f",
            cursor: "pointer",
          }}
          overflow={"hidden"}
          position={"relative"}
          onClick={() => handleNavigate()}
        >
          <img height={"100%"} src={galleryImages?.nodes?.[0]?.sourceUrl} />
        </Box>
        {!upSm && (
          <Stack
            direction={"column"}
            gap={"10px"}
            justifyContent={viewOnly ? "space-between" : "flex-start"}
            width={viewOnly ? "100%" : "auto"}
          >
            <Typography
              variant="h5"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
              onClick={() => handleNavigate()}
              fontWeight={"700"}
            >
              {cartItem?.selectedSize?.name || name}
            </Typography>
            <Typography
              variant="body1"
              color={"primary.main"}
              display={viewOnly ? "none" : "block"}
            >
              <strong>
                {correctingPrice(onSale ? price : regularPrice)} ₽
              </strong>{" "}
              <strike>{onSale && correctingPrice(regularPrice) + "  ₽"}</strike>
            </Typography>
            <Typography variant="h5" fontWeight={700} color={"primary.main"}>
              {cartItem.quantity +
                " × " +
                correctingPrice(onSale ? price : regularPrice)}{" "}
              ₽
            </Typography>
          </Stack>
        )}
      </Stack>

      <Stack
        width={"100%"}
        direction={"row"}
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        {upSm && (
          <Stack
            direction={"column"}
            gap={"10px"}
            width={viewOnly ? "100%" : "auto"}
          >
            <Typography
              variant="h5"
              color={"primary.main"}
              sx={{ cursor: "pointer" }}
              onClick={() => handleNavigate()}
              fontWeight={"700"}
            >
              {cartItem?.selectedSize?.name || name}
            </Typography>
            <Typography
              variant="body1"
              color={"primary.main"}
              display={viewOnly ? "none" : "block"}
            >
              <strong>
                {correctingPrice(onSale ? price : regularPrice)} ₽
              </strong>{" "}
              <strike>{onSale && correctingPrice(regularPrice) + "  ₽"}</strike>
            </Typography>
            <Typography
              variant="h5"
              fontWeight={700}
              color={"primary.main"}
              display={!viewOnly ? "none" : "block"}
            >
              {cartItem.quantity +
                " × " +
                correctingPrice(onSale ? price : regularPrice)}{" "}
              ₽
            </Typography>
          </Stack>
        )}

        <Stack
          width={!viewOnly && "100%"}
          direction={"row"}
          gap={"30px"}
          alignItems={"center"}
          justifyContent={{ xs: "space-between", sm: "flex-end" }}
        >
          <Stack
            direction={"row"}
            justifyContent={"flex-end"}
            alignItems={"center"}
            sx={{
              width: "120px",
              height: { xs: "40px", sm: "50px" },
              overflow: "hidden",
              backgroundColor: "secondary.main",
              borderRadius: "15px",
            }}
            display={viewOnly ? "none" : "flex"}
          >
            <Box
              sx={{
                width: "30%",
                height: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                cursor: "pointer",
                color: cartItem?.quantity === 1 ? "grey" : "primary.main",
                pointerEvents: cartItem?.quantity === 1 ? "none" : "auto",
                transition: "all 0.3s ease",
                "&:hover": {
                  backgroundColor: "primary.main",
                  color: "primary.white",
                },
              }}
              onClick={() => {
                setCartItem({ ...cartItem, quantity: cartItem?.quantity - 1 });
              }}
            >
              <Typography
                variant="body1"
                fontWeight={900}
                color={"inherit"}
                textAlign={"center"}
              >
                -
              </Typography>
            </Box>

            <Typography
              variant="body1"
              fontWeight={900}
              color={"primary.main"}
              flex={2}
              textAlign={"center"}
            >
              {cartItem?.quantity}
            </Typography>
            <Box
              sx={{
                width: "30%",
                height: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                cursor: "pointer",
                transition: "all 0.3s ease",
                color: "primary.main",
                "&:hover": {
                  backgroundColor: "primary.main",
                  color: "primary.white",
                },
              }}
              onClick={() => {
                setCartItem({
                  ...cartItem,
                  quantity: cartItem?.quantity + 1,
                });
              }}
            >
              <Typography
                variant="body1"
                fontWeight={900}
                color={"inherit"}
                textAlign={"center"}
              >
                +
              </Typography>
            </Box>
          </Stack>
          {!viewOnly && (
            <Typography variant="h5" fontWeight={700} color={"primary.main"}>
              {cartItem.quantity *
                correctingPrice(onSale ? price : regularPrice)}{" "}
              ₽
            </Typography>
          )}

          <Typography
            variant="h4"
            fontWeight={500}
            color={"primary.main"}
            sx={{ cursor: "pointer" }}
            display={viewOnly ? "none" : "flex"}
            onClick={() => {
              deleteCartItem(cartItem?.id, cartItem?.selectedSize?.databaseId);
            }}
          >
            ×
          </Typography>
        </Stack>
      </Stack>
    </Stack>
  );
}
