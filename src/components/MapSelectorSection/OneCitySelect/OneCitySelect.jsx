import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React from "react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import location from "../../../media/icons/Location.svg";
import calendar from "../../../media/icons/Calendar.svg";
import phone from "../../../media/icons/Phone.svg";
import ArrowButton from "../../ArrowButton/ArrowButton";
import { mainTheme } from "../../../config/MUI/mainTheme";

const chipStyle = {
  borderRadius: "20px",
  p: { xs: "4px 10px 4px 4px" },
  bgcolor: "secondary.main",
};

const iconStyle = {
  minWidth: "20px",
  height: "20px",
  bgcolor: "primary.contrastText",
  borderRadius: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

export default function OneCitySelect({ cityData, selectCity, setSelectCity }) {
  const [expanded, setExpanded] = React.useState(false);
  const upMd = useMediaQuery(mainTheme.breakpoints.up("md"));

  return (
    <>
      {!upMd ? (
        <Accordion
          elevation={0}
          sx={{ borderRadius: "15px !important" }}
          expanded={expanded}
          onChange={() => {
            setExpanded(!expanded);
            !expanded && setSelectCity(cityData);
          }}
        >
          <AccordionSummary aria-controls="panel1-content" id="panel1-header">
            <Stack
              width={"100%"}
              direction={"row"}
              alignItems={"center"}
              justifyContent={{ xs: "center", sm: "flex-start" }}
            >
              <Typography variant="h4" display={"block"}>
                {cityData.name}
              </Typography>
              <KeyboardArrowDownIcon
                sx={{
                  color: "primary.main",
                  transform: expanded ? "rotate(-180deg)" : "none",
                  transition: "all 0.3s ease",
                }}
              />
            </Stack>
          </AccordionSummary>
          <AccordionDetails
            onClick={() => setSelectCity(cityData)}
            sx={{ paddingTop: 0, position: "relative" }}
          >
            <Stack
              direction={{ xs: "column", sm: "row" }}
              width={{ xs: "100%", sm: "70%" }}
              flexWrap={"wrap"}
              alignItems={"flex-start"}
              sx={{ position: "relative" }}
              gap={"8px"}
              position={"relative"}
            >
              <Stack
                direction={"row"}
                alignItems={"center"}
                gap={"4px"}
                sx={chipStyle}
              >
                <Box sx={iconStyle}>
                  <img src={location} width={"60%"} alt="location" />
                </Box>
                <Typography variant="body1" color={"primary.main"}>
                  {cityData.adress}
                </Typography>
              </Stack>
              <Stack
                direction={"row"}
                alignItems={"center"}
                gap={"4px"}
                sx={chipStyle}
              >
                <Box sx={iconStyle}>
                  <img src={phone} width={"60%"} alt="location" />
                </Box>
                <Typography variant="body1" color={"primary.main"}>
                  {cityData.phone}
                </Typography>
              </Stack>
              {/* <Stack
                direction={"row"}
                alignItems={"center"}
                gap={"4px"}
                sx={chipStyle}
              >
                <Box sx={iconStyle}>
                  <img src={calendar} width={"60%"} alt="location" />
                </Box>
                <Typography variant="body1" color={"primary.main"}>
                  {cityData.workingTime}
                </Typography>
              </Stack> */}
            </Stack>
            <ArrowButton sx={{ position: "absolute", right: 10, bottom: 10 }} />
          </AccordionDetails>
        </Accordion>
      ) : (
        <Box
          sx={{
            height: "100%",
            bgcolor: "primary.contrastText",
            borderRadius: "15px",
            p: { xs: "15px", lg: "20px" },
            position: "relative",
            cursor: "pointer",
            transition: "box-shadow 0.3s ease",
            "&:hover": {
              boxShadow: "0 4px 30px 0 rgba(0, 0, 0, 0.1)",
            },
          }}
          onClick={() => setSelectCity(cityData)}
        >
          <Stack
            direction={{ xs: "row", lg: "column" }}
            alignItems={{ xs: "flex-start" }}
          >
            <Typography
              variant="h4"
              display={"block"}
              width={{ lg: "100%" }}
              textAlign={{ lg: "center" }}
              mr={{ xs: "50px", lg: 0 }}
            >
              {cityData.name}
            </Typography>
            <Stack
              mt={{ lg: "15px" }}
              direction={{ xs: "column", sm: "row", lg: "column" }}
              width={"calc(100% - 50px)"}
              flexWrap={"wrap"}
              alignItems={"flex-start"}
              sx={{ position: "relative" }}
              gap={"8px"}
              position={"relative"}
            >
              <Stack
                direction={"row"}
                alignItems={"center"}
                gap={"4px"}
                sx={chipStyle}
              >
                <Box sx={iconStyle}>
                  <img src={location} width={"60%"} alt="location" />
                </Box>
                <Typography variant="body1" color={"primary.main"}>
                  {cityData.adress}
                </Typography>
              </Stack>
              {/* <Stack
                direction={"row"}
                alignItems={"center"}
                gap={"4px"}
                sx={chipStyle}
              >
                <Box sx={iconStyle}>
                  <img src={phone} width={"60%"} alt="location" />
                </Box>
                <Typography variant="body1" color={"primary.main"}>
                  {cityData.phone}
                </Typography>
              </Stack> */}
              {/* <Stack
                direction={"row"}
                alignItems={"center"}
                gap={"4px"}
                sx={chipStyle}
              >
                <Box sx={iconStyle}>
                  <img src={calendar} width={"60%"} alt="location" />
                </Box>
                <Typography variant="body1" color={"primary.main"}>
                  {cityData.workingTime}
                </Typography>
              </Stack> */}
            </Stack>
          </Stack>
          <ArrowButton
            sx={{
              position: "absolute",
              right: { xs: 10, lg: 20 },
              top: { xs: 10, lg: "auto" },
              bottom: { xs: "auto", lg: 10 },
              backgroundColor:
                selectCity?.id === cityData.id
                  ? "primary.main"
                  : "primary.white",
              border: selectCity?.id === cityData.id ? null : "1px solid",
              borderColor:
                selectCity?.id === cityData.id
                  ? null
                  : mainTheme.palette.primary.main,
            }}
            svgProps={{
              stroke:
                selectCity?.id === cityData.id
                  ? mainTheme.palette.primary.white
                  : mainTheme.palette.primary.main,
            }}
          />
        </Box>
      )}
    </>
  );
}
