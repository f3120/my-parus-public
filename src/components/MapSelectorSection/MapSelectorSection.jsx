import { Box, Container, Stack, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useState } from "react";
import { Placemark } from "@pbe/react-yandex-maps";
import BaseMap from "./BaseMap/BaseMap";
import OneCitySelect from "./OneCitySelect/OneCitySelect";
import { citiesData } from "../../config/data/citiesData";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import { mainTheme } from "../../config/MUI/mainTheme";
import { gql, useQuery } from "@apollo/client";

const APOLLO_QUERY = gql`
  query CitiesData {
    posts(where: { categoryName: "clubdescription" }) {
      nodes {
        clubdescription {
          addressclub
          nameclub
          phoneclub
          pointclub {
            latitude
            longitude
          }
        }
        clubfield {
          clubname
        }
      }
    }
  }
`;

export default function MapSelectorSection() {
  const { loading, error, data } = useQuery(APOLLO_QUERY);

  const [cities, setCities] = useState(
    mergeCities(citiesData, data?.posts?.nodes)
  );

  useEffect(() => {
    setCities(mergeCities(citiesData, data?.posts?.nodes));
  }, [data]);

  const [selectCity, setSelectCity] = useState(cities[0]);
  // xs: "40px", sm: "40px", md: "60px"
  return (
    <Container>
      <Box
        width={"100%"}
        height={{ xs: 210, md: 360, lg: 440 }}
        position={"relative"}
        borderRadius={"30px 30px 0px 0px"}
        overflow={"hidden"}
        zIndex={2}
      >
        <Stack
          sx={{
            position: "absolute",
            zIndex: 1,
            backgroundColor: mainTheme.palette.primary.white + "66",
            backdropFilter: "blur(7px)",
            borderRadius: "20px",
            p: { xs: "10px 15px", md: "20px 30px" },
            top: { xs: 10, md: 15, xl: 30 },
            left: "10px",
          }}
          gap={1}
        >
          <Typography variant="h4" lineHeight={1}>
            {selectCity?.name}
          </Typography>
          <Typography variant="body1" color={"primary.main"}>
            {selectCity?.adress}
          </Typography>
        </Stack>

        <BaseMap
          state={{
            center: selectCity?.coordinate,
            zoom: 15,
            behaviors: ["drag"],
          }}
          allPlacemark={<Placemark geometry={selectCity?.coordinate} />}
        />
      </Box>

      <Box
        bgcolor={"secondary.main"}
        p={{ xs: "10px 0", md: "20px 0" }}
        borderRadius={"0 0 30px 30px"}
        overflow={"hidden"}
      >
        <Container>
          <Grid2 container rowSpacing={{ xs: "10px" }} columnSpacing={"30px"}>
            {cities.map((city) => (
              <Grid2 xs={12} lg>
                <OneCitySelect
                  cityData={city}
                  selectCity={selectCity}
                  setSelectCity={setSelectCity}
                />
              </Grid2>
            ))}
          </Grid2>
        </Container>
      </Box>
    </Container>
  );
}

const mergeCities = (localCities, WPCities) => {
  if (!Array.isArray(WPCities)) {
    return localCities;
  }
  return WPCities.map((WPcity) => {
    const localCity = localCities.find(
      (lc) => WPcity.clubdescription.nameclub === lc.name
    );

    return {
      ...localCity,
      name: WPcity.clubdescription.nameclub || localCity.name,
      adress: WPcity.clubdescription.addressclub || localCity.adress,
      phone: WPcity.clubdescription.phoneclub || localCity.phone,
      coordinate: [
        WPcity.clubdescription.pointclub.longitude || localCity?.coordinate[0],
        WPcity.clubdescription.pointclub.latitude || localCity?.coordinate[1],
      ],
    };
  });
};
