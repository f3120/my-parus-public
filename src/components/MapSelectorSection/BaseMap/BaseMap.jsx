import { YMaps, Map, Placemark } from "@pbe/react-yandex-maps";
import React from "react";
import { Box } from "@mui/material";
import "./BaseMap.css";

export default function BaseMap({ state, allPlacemark }) {
  return (
    <Box width={"100%"} height={"100%"} zIndex={1} position={"relative"}>
      <YMaps>
        <Map
          defaultState={{ center: [0, 0], zoom: 1 }}
          width={"100%"}
          height={"100%"}
          state={state}
        >
          {allPlacemark}
        </Map>
      </YMaps>
    </Box>
  );
}
