import React from "react";

import { Box, Button, Stack, Typography } from "@mui/material";

import arrow, { ReactComponent as Arrow } from "../../../media/icons/Arrow.svg";
import jackdaw, {
  ReactComponent as Jackdaw,
} from "../../../media/icons/Jackdaw.svg";
// import cercle, {
//   ReactComponent as Cercle,
// } from "../../../media/icons/Cercle.svg";
export default function ServicesCard({
  title,
  content,
  contentShort,
  media,
  setSelectedService,
}) {
  return (
    <Stack
      bgcolor={"primary.main"}
      sx={{
        width: "100%",
        height: "100%",
        justifyContent: "space-between",
        borderRadius: "20px",
      }}
      padding={{ xs: "10px", sm: "5px", md: "10px", lg: "15px", xl: "15px" }}
    >
      <Stack direction="column">
        <Stack
          direction={{ xs: "row", sm: "column", lg: "row" }}
          alignItems={"center"}
        >
          <Box
            padding={{ xs: "3px", md: "5px" }}
            height={"auto"}
            width={{
              xs: "56px",
              lg: "100px",
            }}
            sx={{
              borderRadius: "100%",
              backgroundColor: "#FFFFFF",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              lineHeight: 0,
            }}
          >
            <img width="100%" src={media} alt="clubEvents" />
          </Box>

          <Typography
            width={"60%"}
            color={"primary.white"}
            variant="h5"
            marginLeft={{ xs: "7px", lg: "15px" }}
            textAlign={{ xs: "start", sm: "center", md: "center", lg: "start" }}
          >
            {title}
          </Typography>
        </Stack>
        {contentShort &&
          contentShort.length > 0 &&
          contentShort.map((contentItem, index) => {
            // if (index < 2)
            return (
              <Stack
                direction="row"
                alignItems="center"
                marginTop={"20px"}
                display={{ xs: "none", lg: "flex" }}
              >
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    minWidth: "24px",
                    minHeight: "24px",
                    maxWidth: "24px",
                    maxHeight: "24px",
                    borderRadius: "100%",
                    border: "1px solid",
                    borderColor: "primary.white",
                  }}
                >
                  <Jackdaw width={"70%"} height={"70%"} stroke="#FFFFFF" />
                </Box>
                <Typography
                  variant="body1"
                  display={{ xs: "none", lg: "block" }}
                  marginLeft={"10px"}
                >
                  {contentItem}
                </Typography>
              </Stack>
            );
          })}
      </Stack>

      <Button
        onClick={() => setSelectedService()}
        sx={{
          borderRadius: "30px",
          width: "100%",
          border: "1px solid",
          borderColor: "primary.white",
          marginTop: {
            xs: "15px",
            sm: "10px",
            md: "20px",
            lg: "40px",
            xl: "40px",
          },
        }}
      >
        <Typography
          variant="body1"
          display={{ xs: "block", sm: "none", md: "block" }}
        >
          Подробнее
        </Typography>
        <Typography
          variant="body1"
          display={{ xs: "none", sm: "block", md: "none" }}
        >
          Подробнее
        </Typography>
        <Box
          sx={{ lineHeight: 0, pt: "2px" }}
          marginLeft={{
            xs: "10px",
            sm: "5px",
            md: "13px",
            lg: "18px",
            xl: "10px",
          }}
        >
          <Arrow width="15px" height="15px" stroke="#FFFFFF" />
        </Box>
      </Button>
    </Stack>
  );
}
