import {
  Box,
  Button,
  Stack,
  Dialog,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useContext, useEffect, useState } from "react";
import ChipWithIcon from "../../ChipWithIcon/ChipWithIcon";
import closeIcon, {
  ReactComponent as CloseIcon,
} from "../../../media/icons/CloseIcon.svg";

import jackdaw, {
  ReactComponent as Jackdaw,
} from "../../../media/icons/Jackdaw.svg";
import { ModalContext } from "../../../context/ModalContext";
import { mainTheme } from "../../../config/MUI/mainTheme";
export default function ServiceModal({
  serviceData,
  selectedService,
  setSelectedService,
}) {
  const { isModalActive, setModalActive, setModalInactive } =
    useContext(ModalContext);

  const [localServiceData, setLocalServiceData] = useState(serviceData);

  useEffect(() => {
    serviceData && setLocalServiceData(serviceData);
  }, [serviceData]);

  const { title, media, content, contentShort } = localServiceData || {};

  const upMd = useMediaQuery(mainTheme.breakpoints.up("md"));

  return (
    <Dialog
      open={typeof selectedService !== "undefined"}
      onClose={() => setSelectedService(undefined)}
      PaperProps={{
        sx: {
          borderRadius: "15px",
          width: { xs: "100%", sm: "450px", md: "650px" },
          m: 0,
          overflow: "hidden",
          display: "flex",
          flexDirection: "column",
        },
      }}
    >
      <Stack
        direction="column"
        padding={{ xs: "10px", sm: "20px" }}
        justifyContent={"space-between"}
        sx={{ flex: 1, overflow: "hidden" }}
      >
        {/* Блок заголовка */}
        <Stack
          direction="row"
          bgcolor={"primary.main"}
          sx={{ width: "100%", borderRadius: "15px" }}
          height={{ xs: "67px", md: "152px" }}
          padding={{ xs: "5px", md: "20px" }}
          paddingRight={{ xs: "55px" }}
          alignItems={"center"}
        >
          <Box
            height={"auto"}
            padding={{ xs: "3px", md: "5px" }}
            width={{
              xs: "56px",
              sm: "57px",
              md: "112px",
              lg: "111px",
              xl: "111px",
            }}
            sx={{
              borderRadius: "100%",
              backgroundColor: "#FFFFFF",
              lineHeight: 0,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img width="100%" src={media} alt="clubEvents" />
          </Box>
          <Typography
            variant="h5"
            marginLeft={{ xs: "7px", lg: "15px" }}
            paddingRight={{ xs: "35px", md: "45px" }}
            textAlign={{
              xs: "start",
              sm: "center",
              md: "center",
              lg: "start",
            }}
          >
            {title}
          </Typography>
          <Button
            sx={{
              position: "absolute",
              right: { xs: "15px", md: "25px" },
            }}
            onClick={() => setSelectedService(undefined)}
          >
            <CloseIcon
              width={upMd ? "40px" : "25px"}
              height={upMd ? "40px" : "25px"}
              stroke={"#FFFFFF"}
              style={{
                padding: "5px",
                border: "1px solid #FFFFFF",
                borderRadius: "100%",
              }}
            />
          </Button>
        </Stack>
        <Stack sx={{ flex: 1, overflow: "auto" }}>
          <Stack direction={"column"} gap={"10px"} marginTop={"15px"}>
            {contentShort &&
              Array.isArray(contentShort) &&
              contentShort.map((item, index) => (
                <Stack direction="row" alignItems={"center"}>
                  <Box
                    marginRight={"5px"}
                    color={"primary.main"}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      minWidth: "24px",
                      minHeight: "24px",
                      maxWidth: "24px",
                      maxHeight: "24px",
                      borderRadius: "50%",
                      border: "1px solid",
                      borderColor: "primary.main",
                    }}
                  >
                    <Jackdaw
                      width={"70%"}
                      height={"70%"}
                      stroke={mainTheme.palette.primary.main}
                    />
                  </Box>
                  <Typography color={"primary.main"} key={index}>
                    {item}
                  </Typography>
                </Stack>
              ))}
          </Stack>
          <Typography
            marginTop={"15px"}
            textAlign={"center"}
            variant="h5"
            color={"primary.main"}
          >
            Из чего состоит:
          </Typography>
          <Stack direction={"column"} gap={"10px"} marginTop={"10px"}>
            {content &&
              Array.isArray(content) &&
              content.map((item, index) => (
                <Stack direction="row" alignItems={"center"}>
                  <Box
                    marginRight={"5px"}
                    color={"primary.main"}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      minWidth: "24px",
                      minHeight: "24px",
                      maxWidth: "24px",
                      maxHeight: "24px",
                      borderRadius: "50%",
                      border: "1px solid",
                      borderColor: "primary.main",
                    }}
                  >
                    <Jackdaw
                      width={"70%"}
                      height={"70%"}
                      stroke={mainTheme.palette.primary.main}
                    />
                  </Box>
                  <Typography color={"primary.main"} key={index}>
                    {item}
                  </Typography>
                </Stack>
              ))}
          </Stack>
          {/* Блок "из чего состоит" */}

          {/* Блок "что получают" */}
          <Stack marginTop={"10px"}>
            <Typography
              textAlign={"center"}
              color={"primary.main"}
              variant="h5"
            >
              Ваш результат:
            </Typography>
            <Stack direction="row" flexWrap="wrap">
              {localServiceData?.chipArray &&
                localServiceData?.chipArray.map((chip) => (
                  <ChipWithIcon
                    icon={chip?.icon}
                    text={chip?.text}
                    sx={{ margin: "5px" }}
                    iconStyle={chip?.style || { width: "16px", height: "16px" }}
                  />
                ))}
            </Stack>
          </Stack>
        </Stack>

        <Button
          variant="contained"
          sx={{
            display: "flex",
            justifyContent: "center",

            borderRadius: "30px",
            marginTop: "15px",
          }}
          onClick={() => {
            setModalActive("scheduleModal");
            setSelectedService(undefined);
          }}
        >
          <Typography textAlign={"center"}>Записаться</Typography>
        </Button>
      </Stack>
    </Dialog>
  );
}
