import React, { useEffect } from "react";
import { useState } from "react";
import { Box, Container, Stack, Typography } from "@mui/material";
import ServicesCard from "./ServicesCard/ServicesCard";
import optiFUN, {
  ReactComponent as OptiFUN,
} from "../../media/icons/OptiFUN.svg";
import optiSAIL, {
  ReactComponent as OptiSAIL,
} from "../../media/icons/OptiSAIL.svg";
import optiRACE, {
  ReactComponent as OptiRACE,
} from "../../media/icons/OptiRACE.svg";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import ServiceModal from "./ServiceModal/ServiceModal";
import smiley from "../../media/icons/smiley.svg";
import rope from "../../media/icons/rope.svg";
import boat from "../../media/icons/boat.svg";
import str from "../../media/icons/str.svg";
import blow from "../../media/icons/blow.svg";
import finish from "../../media/icons/finish.svg";
import holiday from "../../media/icons/holiday.svg";
import victory from "../../media/icons/victory.svg";
import { gql, useQuery } from "@apollo/client";
import { name } from "dayjs/locale/ru";

const card = [
  {
    id: "optiFUN",
    typeservice: "FUN",
    title: "Знакомство с яхтингом",
    media: optiFUN,
    contentShort: [
      "Разовая ознакомительная практика на Оптимисте или Лазере",
      "Продолжительность — 2-3 часа (половина времени на берегу/ половина на воде)",
    ],
    content: [
      "Знакомимся с устройством яхты на берегу",
      "Помогаем вооружать яхту",
      "Осваиваем первый морской узел",
      "Выходим на воду на катере",
      "Катаемся с инструктором на яхте",
      "При желании пробуем рулить",
    ],
    chipArray: [
      { icon: smiley, text: "понимание, парус – это мое или нет" },
      { icon: rope, text: "yмение вязать морской узел" },
      { icon: boat, text: "первый опыт руления" },
      {
        icon: optiFUN,
        text: "желтая наклейка OptiFUN",
        style: { width: "30px", height: "30px" },
      },
    ],
  },
  {
    id: "optiSAIL",
    typeservice: "SAIL",
    title: "Курсы для новичков",
    media: optiSAIL,
    contentShort: [
      "Курс из 7-10 практик",
      "Продолжительность каждой практики 2-3 часа",
      "Занятия в группах по 3-8 человек",
    ],
    content: [
      "Курс включает теорию яхтинга и практику на воде с 1го дня",
      "Получаем наглядные материалы",
      "Осваиваем вооружение/разоружение яхты",
      "Вяжем базовые морские узлы и запоминаем термины",
      "Пробуем самостоятельно рулить и выполнять основные маневры на яхте",
      "Определяем курсы яхты относительно ветра",
      "Знакомимся с хождением в лавировку (против ветра)",
    ],
    chipArray: [
      {
        icon: str,
        text: "самостоятельное вооружение и управление швертботом Оптимист или Лазер",
      },
      { icon: holiday, text: "диплом клуба OptiSAIL или LaserSAIL" },
      {
        icon: optiSAIL,
        text: "синяя наклейка OptiSAIL или LaserSAIL",
        style: { width: "30px", height: "30px" },
      },
    ],
  },
  {
    id: "optiRACE",
    typeservice: "RACE",
    title: "Яхтинг для продолжающих",
    media: optiRACE,
    contentShort: [
      "Разовые практики в «продолжающей» группе",
      "Занятия по абонементам",
      "Участие в клубных состязаниях (парусных гонках)",
    ],
    content: [
      "Осваиваем 16 базовых элементов техники управления яхтой",
      "Получаем начальные понятия о парусных гонках",
      "Проходим гоночной дистанции",
      "Участвуем в клубных состязаниях (парусных гонках)",
    ],
    chipArray: [
      {
        icon: boat,
        text: "более уверенная техника управления яхтой при различных ветро-волновых условиях",
      },

      {
        icon: optiRACE,
        text: "переход на уровень «гонщик» - красная наклейка OptiRACE или LaserRACE",
        style: { width: "30px", height: "30px" },
      },
    ],
  },
];

const APOLLO_QUERY = gql`
  query ServicesMain {
    posts(where: { categoryName: "servicemain" }) {
      nodes {
        servicemain {
          name
          result
          serviceOmposition
          shortdescription
          typeservice
        }
      }
    }
  }
`;

export default function ServicesSection({ servicesBlockRef }) {
  const { loading, error, data } = useQuery(APOLLO_QUERY);

  const [selectedService, setSelectedService] = useState(undefined);
  const [services, setServices] = useState(
    mergeServices(card, data?.posts?.nodes)
  );

  useEffect(() => {
    setServices(mergeServices(card, data?.posts?.nodes));
    console.log(
      "mergeServices",
      mergeServices(card, data?.posts?.nodes),
      data?.posts?.nodes
    );
  }, [data]);

  return (
    <Box
      sx={{
        width: "100%",
      }}
      ref={servicesBlockRef}
    >
      <Box bgcolor={{ xs: "primary.main", md: "transparent" }}>
        <Container>
          <Stack
            borderRadius={"30px"}
            direction={{ xl: "row" }}
            bgcolor={"primary.main"}
            alignItems={"center"}
            sx={{
              width: "100%",
            }}
            paddingTop={{ xs: "10px", xl: "30px" }}
            paddingBottom={{ xs: "10px", xl: "30px" }}
          >
            <Typography
              sx={{ textAlign: "center" }}
              marginLeft={{ xl: "30px" }}
              variant="h3"
              color={"primary.white"}
            >
              Услуги клуба
            </Typography>
            <Stack direction={{ xl: "colomn" }}>
              <Typography
                display={{ xs: "none", xl: "block" }}
                marginLeft={{ xl: "108px" }}
              >
                Для детей 6-14 лет на яхтах класса "Оптимист"
              </Typography>
              <Typography
                display={{ xs: "none", xl: "block" }}
                marginLeft={{ xl: "108px" }}
                mt={"5px"}
              >
                Для взрослых, юношей и девушек 12-18 лет на яхтах класса "Laser"
              </Typography>
            </Stack>
          </Stack>
        </Container>
      </Box>

      <Container>
        <Box marginTop={{ xs: "10px", sm: "11px", md: "15px", lg: "20px" }}>
          <Grid2 container spacing={{ xs: "10px", sm: "10px", md: "20px" }}>
            {services.map((item, index) => (
              <Grid2 xs={12} sm={4}>
                <ServicesCard
                  {...item}
                  setSelectedService={() => setSelectedService(index)}
                />
              </Grid2>
            ))}
          </Grid2>
        </Box>
      </Container>
      <ServiceModal
        selectedService={selectedService}
        serviceData={
          typeof selectedService !== "undefined" && services[selectedService]
        }
        setSelectedService={setSelectedService}
      />
    </Box>
  );
}

const mergeServices = (localServices, WPServices) => {
  if (!Array.isArray(WPServices)) {
    return localServices;
  }
  return localServices.map((service) => {
    const WPService = WPServices.find(
      (wps) => wps.servicemain.typeservice === service.typeservice
    );

    console.log({ WPService });

    if (!WPService) {
      return service;
    }

    return {
      ...service,
      title: WPService.servicemain.name || service.title,
      chipArray:
        (WPService.servicemain.result &&
          Array.isArray(WPService.servicemain.result.split(";")) &&
          WPService.servicemain.result
            .split(";")
            .map((chip) => ({ text: chip }))) ||
        service.chipArray,
      content:
        (WPService.servicemain.serviceOmposition &&
          Array.isArray(WPService.servicemain.serviceOmposition.split(";")) &&
          WPService.servicemain.serviceOmposition.split(";")) ||
        service.content,
      contentShort:
        (WPService.servicemain.shortdescription &&
          Array.isArray(WPService.servicemain.shortdescription.split(";")) &&
          WPService.servicemain.shortdescription.split(";")) ||
        service.contentShort,
    };
  });
};
