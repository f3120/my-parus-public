import React from "react";
import Slider from "react-slick";
import ReviewItem from "../ReviewItem/ReviewItem";
import "./ReviewSlider.css";
import { useMediaQuery } from "@mui/material";
import { mainTheme } from "../../../config/MUI/mainTheme";

export default function ReviewSlider({ reviewsData = [] }) {
  const upMd = useMediaQuery(mainTheme.breakpoints.up("md"));
  const upLg = useMediaQuery(mainTheme.breakpoints.up("lg"));
  const upXl = useMediaQuery(mainTheme.breakpoints.up("xl"));

  const [selectedSlide, setSelectedSlide] = React.useState(0);

  const settings = {
    dots: false,
    infinite: reviewsData.length === 1 ? false : true,
    speed: 500,
    afterChange: (currentSlide) => setSelectedSlide(currentSlide),
    slidesToShow: 1,
    slidesToScroll: 1,
    className: "review-slider",
    arrows: true,
    centerMode: true,
    centerPadding: upXl ? "25%" : upLg ? "20%" : upMd ? "40px" : "20px",
    // adaptiveHeight: true,
  };

  return (
    <Slider {...settings}>
      {reviewsData.length > 0 &&
        reviewsData.map((item) => (
          <ReviewItem reviewItemData={item} selectedSlide={selectedSlide} />
        ))}
    </Slider>
  );
}
