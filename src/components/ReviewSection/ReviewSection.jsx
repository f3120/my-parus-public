import { Box, Container, Stack, Typography } from "@mui/material";
import React from "react";
import ReviewSlider from "./ReviewSlider/ReviewSlider";
import reviewImageOlga from "../../media/images/ReviewOlga.png";
import reviewImageViktor from "../../media/images/ReviewViktor.png";
import reviewImageAndrey from "../../media/images/ReviewAndrey.png";
import { gql, useQuery } from "@apollo/client";

const reviewsDataDefault = [
  {
    id: 586,
    name: "Ольга К",
    rating: 5,
    text: (
      <>
        Прекрасное место для детей и взрослых. Клуб, где дети узнают о курсах
        ветра, становятся самостоятельными и преодолевают себя, взрослея за
        неделю почти на год, а то и больше. Место, где взрослые могут открыть
        для себя парус, влюбиться в него и определить все свои следующие
        отпуска, связав их с большой водой. Это место, где уже влюбленные в
        парус могут попробовать новое, снова и снова пересматривая видео с
        тренировок, улыбаясь широко-широко от эмоций.
        <br />
        My Sail in mria - лучшее место для яхтинга в Имеретинке и во всем Сочи.
        Теплое, уютное, милое, настоящее... Провела активную часть отпуска на
        лазере, лучший отдых из возможных. Лодки, паруса, тренеры. Даже в слабый
        ветер составляют программу на тренировку и учат новому.
        <br />
        От души и сердца рекомендую столь прекрасное место. Ребятам процветания,
        а вам - новых открытий на воде!
      </>
    ),
    image: reviewImageOlga,
  },
  {
    id: 587,
    name: "Андрей",
    rating: 5,
    text: (
      <>
        Товарищи! сложившаяся структура организации позволяет выполнять важные
        задания по разработке соответствующий условий активизации. Задача
        организации, в особенности же начало повседневной работы по формированию
        позиции обеспечивает широкому кругу (специалистов) участие в
        формировании форм развития. Таким образом реализация намеченных плановых
        заданий способствует подготовки и реализации дальнейших направлений
        развития.
      </>
    ),
    image: reviewImageAndrey,
  },
  {
    id: 588,
    name: "Виктор Жук",
    rating: 5,
    text: (
      <>
        Если вашим детям от 6 лет и более, то лучшего места для них в Сочи вы не
        найдете. Настолько интересно организованы занятия и настолько дружная и
        теплая атмосфера в коллективе, что дети готовы проводить в клубе все
        свободное время. Но прелесть в том, что они постигают полезные знания и
        навыки, умение управления маломерными судами и морскую терминологию. Это
        реально классный клуб, где дети набираются и здоровья и знаний, а
        главное это очень увлекательно и интересно!
      </>
    ),
    image: reviewImageViktor,
  },
];

const APOLLO_QUERY = gql`
  query Rew($categoryName: String!) {
    posts(where: { categoryName: $categoryName }, first: 10) {
      nodes {
        fields_reviews {
          point
          textReview
          nameReviewer
          photoReviewer {
            sourceUrl
          }
        }
      }
    }
  }
`;

export default function ReviewSection() {
  const { data, loading } = useQuery(APOLLO_QUERY, {
    variables: { categoryName: "review" },
  });

  const reviewsData = data?.posts?.nodes.map((node, index) => ({
    id: index,
    rating: node?.fields_reviews?.point,
    name: node?.fields_reviews?.nameReviewer,
    text: node?.fields_reviews?.textReview,
    image: node?.fields_reviews?.photoReviewer?.sourceUrl,
  }));

  return (
    <Stack
      direction={"column"}
      bgcolor={"secondary.main"}
      padding={{ xs: "10px 0", xl: "30px 0" }}
      borderRadius={"30px"}
    >
      <Typography
        sx={{ textAlign: "center" }}
        marginLeft={{ xl: "30px" }}
        variant="h3"
      >
        Отзывы о клубе
      </Typography>
      <Box mt={{ xs: "10px", md: "20px" }}>
        <ReviewSlider reviewsData={reviewsData} />
      </Box>
    </Stack>
  );
}
