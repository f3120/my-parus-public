import styled from "@emotion/styled";
import { Box, Rating, Stack, Typography, useMediaQuery } from "@mui/material";
import React, { useEffect, useRef } from "react";
import { mainTheme } from "../../../config/MUI/mainTheme";
import parse from "html-react-parser";

const StyledRating = styled(Rating)({
  "& .MuiRating-iconFilled": {
    "> svg": {
      fill: "#faaf00",
    },
  },
  "& .MuiRating-iconEmpty": {
    "> svg": {
      fill: "#FFF",
    },
  },
});

export default function ReviewItem({ selectedSlide, reviewItemData = {} }) {
  const [open, setOpen] = React.useState(false);
  const [showBtn, setShowBtn] = React.useState(false);
  const paragraphRef = useRef(null);
  const upSm = useMediaQuery(mainTheme.breakpoints.up("sm"));
  const upMd = useMediaQuery(mainTheme.breakpoints.up("md"));

  useEffect(() => {
    const paragraph = paragraphRef.current;
    const lineHeight =
      typeof window !== "undefined"
        ? parseInt(window.getComputedStyle(paragraph).lineHeight, 10)
        : 0;
    const height = paragraph?.offsetHeight;
    const lineCount = Math.ceil(height / lineHeight);
    setShowBtn(lineCount > (upMd ? 8 : upSm ? 9 : 11));
  }, []);

  useEffect(() => {
    setOpen(false);
  }, [selectedSlide]);

  return (
    <Stack
      sx={{
        bgcolor: "primary.white",
        padding: {
          xs: "10px",
          sm: "20px",
          lg: "30px",
        },
        borderRadius: { xs: "15px", sm: "20px", lg: "30px" },
        minHeight: "320px",
        maxHeight: open ? "700px" : "320px",
        overflow: "hidden",
        transition: "max-height 0.5s ease-in-out",
      }}
      direction={"column"}
      justifyContent={"flex-start"}
      gap={{ xs: "10px", sm: "15px" }}
    >
      <Stack
        direction={"row"}
        gap={{ xs: "10px", sm: "15px" }}
        alignItems={"center"}
        bgcolor={"secondary.main"}
        p={"5px 15px 5px 5px"}
        borderRadius={"100px"}
        width={"fit-content"}
      >
        <Box
          sx={{
            width: "50px",
            height: "50px",
            borderRadius: "100%",
            backgroundImage: `url(${reviewItemData?.image})`,
            backgroundSize: "cover",
          }}
        ></Box>
        <Stack
          direction={"column"}
          justifyContent={"space-between"}
          height={"100%"}
        >
          <Typography variant="body1" color="primary.main">
            {reviewItemData?.name}
          </Typography>
          <StyledRating
            value={reviewItemData?.rating || 0}
            readOnly
            sx={{
              mt: "5px",
            }}
          />
        </Stack>
      </Stack>
      <Typography
        ref={paragraphRef}
        variant="body1"
        color="primary.main"
        sx={{
          overflow: "hidden",
          textOverflow: "clip",
          display: "-webkit-box",
          WebkitLineClamp:
            showBtn && (open ? "-1" : { xs: "10", sm: "8", md: "6" }),
          WebkitBoxOrient: "vertical",
          transition: "all 0.5s ease-in-out",
        }}
      >
        {parse(reviewItemData?.text)}
      </Typography>
      <Typography
        variant="caption"
        fontWeight={700}
        color="primary.main"
        sx={{ cursor: "pointer", padding: "10px 5px" }}
        textAlign={"right"}
        display={showBtn ? "block" : "none"}
        onClick={() => setOpen(!open)}
      >
        {open ? "Скрыть" : "Читать дальше"}
      </Typography>
    </Stack>
  );
}
