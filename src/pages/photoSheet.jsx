import { Container } from "@mui/material";
import React from "react";
import { SEO } from "../components/SEO/SEO";
import Layout from "../components/Layout/Layout";

import GalleryPageTitle from "../components/GalleryPageSection/GalleryPageTitle/GalleryPageTitle";
import GalleryPageList from "../components/GalleryPageSection/GalleryPageList/GalleryPageList";
import { gql, useQuery } from "@apollo/client";
import { convertSrcSet } from "../tools/tools";

export const APOLLO_QUERY = gql`
  query OneGallery($id: ID!) {
    post(id: $id) {
      id
      title
      galleryfield {
        dategallery
        fieldGroupName
        allphoto {
          id
          sourceUrl
          srcSet
        }
      }
    }
  }
`;

export default function GallerySheet() {
  const location = typeof window !== "undefined" && window.location;
  const id = new URLSearchParams(location?.search).get("ID");

  const { loading, error, data } = useQuery(APOLLO_QUERY, {
    variables: { id: id || "" },
  });

  return (
    <Layout>
      <Container>
        <GalleryPageTitle title={data?.post?.title} />
        <GalleryPageList
          loading={loading}
          photo={data?.post?.galleryfield?.allphoto?.map((photo) =>
            convertSrcSet(photo.srcSet)
          )}
        />
      </Container>
    </Layout>
  );
}
export const Head = () => <SEO />;
