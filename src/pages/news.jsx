import React from "react";
import Layout from "../components/Layout/Layout";
import { SEO } from "../components/SEO/SEO";
import NewsTitle from "../components/News/NewsTitle/NewsTitle";
import NewsList from "../components/News/NewsList/NewsList";
import { CircularProgress, Container } from "@mui/material";
import { gql, useQuery } from "@apollo/client";
import Loader from "../components/Loader/Loader";
import { parse } from "date-fns";
import { ru } from "date-fns/locale";
import dayjs from "dayjs";

const APOLLO_QUERY = gql`
  query QueryPosts($categoryName: String!) {
    posts(where: { categoryName: $categoryName }, first: 1000) {
      edges {
        node {
          id
          title
          postfield {
            fieldGroupName
            postauthor
            shortdescriptionpost
            postoptionalfield
            postlinkoutservice
            postdata
            postimage {
              sourceUrl
            }
          }
          clubfield {
            clubname
          }
          content
        }
      }
    }
  }
`;

const news = [
  {
    title: "Почему каждый ребенок должен это попробовать",
    date: "21.03.2023",
    description:
      "Все знают о необходимости разностороннего сбалансированного развития человека...",
  },
  {
    title: "Почему каждый ребенок должен это попробовать",
    date: "21.03.2023",
    description:
      "Все знают о необходимости разностороннего сбалансированного развития человека...ь",
  },
  {
    title: "Почему каждый ребенок должен это попробовать",
    date: "21.03.2023",
    description:
      "Все знают о необходимости разностороннего сбалансированного развития человека...",
  },
];

export default function NewsPage() {
  const { data, loading } = useQuery(APOLLO_QUERY, {
    variables: { categoryName: "post" },
  });
  console.log({ data });
  const news = data?.posts?.edges
    .map((node) => ({
      id: node?.node?.id,
      title: node?.node?.title,
      date: node?.node?.postfield?.postdata,
      description: node?.node?.postfield?.shortdescriptionpost,
      background: node?.node?.postfield?.postimage?.sourceUrl,
      check: node?.node?.postfield?.postoptionalfield,
      link: node?.node?.postfield?.postlinkoutservice,
    }))
    .sort((a, b) => {
      const aDate = parse(a.date, "dd.MM.yyyy", new Date(), {
        locale: ru,
      });
      const bDate = parse(b.date, "dd.MM.yyyy", new Date(), {
        locale: ru,
      });

      return dayjs(bDate).unix() - dayjs(aDate).unix();
    });

  if (loading) {
    return <Loader />;
  }

  console.log({ news });
  return (
    <Layout>
      <Container>
        <NewsTitle />
        {loading ? (
          <CircularProgress
            size={70}
            sx={{ margin: "100px auto", display: "block" }}
          />
        ) : (
          <NewsList news={news} />
        )}
      </Container>
    </Layout>
  );
}
export const Head = () => <SEO />;
