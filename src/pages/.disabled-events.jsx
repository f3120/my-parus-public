import { gql, useQuery } from "@apollo/client";
import Layout from "../components/Layout/Layout";
import { CircularProgress, Container } from "@mui/material";
import EventsTitle from "../components/Events/EventsTitle/EventsTitle";
import { SEO } from "../components/SEO/SEO";
import EventsList from "../components/Events/EventsList/EventsList";
import React, { useEffect, useMemo, useState } from "react";
import EventsFilter from "../components/Events/EventsFilter/EventsFilter";
import { parse } from "date-fns";
import { ru } from "date-fns/locale";
import { isBefore, isAfter, isEqual } from "date-fns";
import Loader from "../components/Loader/Loader";
const APOLLO_QUERY = gql`
  query ManyEvents {
    posts(where: { categoryName: "event" }, first: 1000) {
      edges {
        node {
          id
          clubfield {
            clubname
          }
          eventsfields {
            eventstart
            eventend
            eventtittle
            statusevent
            eventage
            eventphoto {
              sourceUrl
            }
          }
        }
      }
    }
  }
`;

export default function EventsPage() {
  const { data, loading } = useQuery(APOLLO_QUERY, {
    variables: { categoryName: "event" },
  });
  console.log({ data });
  const event = useMemo(() => {
    return data?.posts?.edges.map((node) => ({
      id: node?.node?.id,
      title: node?.node?.eventsfields?.eventtittle,
      dateStart: node?.node?.eventsfields?.eventstart,
      dateEnd: node?.node?.eventsfields?.eventend,
      description: node?.node?.eventsfields?.shortdescriptionpost,
      background: node?.node?.eventsfields?.eventphoto?.sourceUrl,
      check: node?.node?.eventsfields?.postoptionalfield,
      link: node?.node?.eventsfields?.postlinkoutservice,
      age: node?.node?.eventsfields?.eventage,
      status: node?.node?.eventsfields?.statusevent,
      region: node?.node?.clubfield?.clubname,
    }));
  }, [data]);
  console.log({ event });

  const [clubRegion, setClubRegionState] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  useEffect(() => {
    setFilteredData(
      event?.filter((item) => {
        const itemDateStart = parse(item.dateStart, "dd.MM.yyyy", new Date(), {
          locale: ru,
        });
        const itemDateEnd = parse(item.dateEnd, "dd.MM.yyyy", new Date(), {
          locale: ru,
        });

        return (
          (!clubRegion || item.region === clubRegion) &&
          (!startDate ||
            !endDate ||
            // // первая дата вне события, а вторая внутри
            // (isAfter(itemDateStart, startDate) &&
            //   isAfter(itemDateEnd, endDate) &&
            //   isBefore(itemDateStart, endDate)) ||
            //   // первая дата внутри события а вторая вне
            // (isBefore(itemDateStart, startDate) &&
            //   isBefore(itemDateEnd, endDate) &&
            //   isAfter(itemDateEnd, startDate)) ||
            //   // первая и вторая дата вне события
            // (isAfter(itemDateStart, startDate) &&
            //   isBefore(itemDateEnd, endDate)) ||
            //   //первая и вторая дата внутри события
            // (isBefore(itemDateStart, startDate) &&
            //   isAfter(itemDateEnd, endDate)) ||
            //   // обе даты совпадают с датами события
            // (isEqual(itemDateStart, startDate) &&
            //   isEqual(itemDateEnd, endDate))
            (isBefore(itemDateStart, endDate) &&
              (isAfter(itemDateEnd, startDate) ||
                isEqual(itemDateEnd, startDate))))
        );
      })
    );
  }, [clubRegion, startDate, endDate, event]);

  return (
    <Layout>
      <Container>
        <EventsTitle />
        <EventsFilter
          setStartDate={setStartDate}
          setEndDate={setEndDate}
          setClubRegionState={setClubRegionState}
        />
        {loading ? (
          <CircularProgress
            size={70}
            sx={{ margin: "100px auto", display: "block" }}
          />
        ) : (
          <EventsList filteredData={filteredData} event={event} />
        )}
      </Container>
    </Layout>
  );
}
export const Head = () => <SEO />;
