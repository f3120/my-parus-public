import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout/Layout";
import { SEO } from "../../components/SEO/SEO";
import { Script } from "gatsby";
import {
  Box,
  Button,
  Container,
  Dialog,
  Stack,
  Typography,
} from "@mui/material";
import closeIcon, {
  ReactComponent as CloseIcon,
} from "../../media/icons/CloseIcon.svg";
import { mainTheme } from "../../config/MUI/mainTheme";
import dog from "../../media/images/Dog.svg";

const MoscowSchedulePage = () => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.id = "mobifitness_personal_widget_script_7ip";
    script.src = "//mobifitness.ru/personal-widget/js/code.js";
    script.dataset.div = "mf_shop_widget_cont_7ip";
    script.dataset.test = "0";
    script.dataset.debug = "0";
    script.dataset.domain = "mobifitness.ru";
    script.dataset.code = "916414";
    script.dataset.version = "v6";
    script.dataset.type = "shop";
    script.dataset.language = "";
    script.dataset.club = "6177";

    open && document.body.appendChild(script);
  }, [open]);

  return (
    <>
      <Layout sectionGap={{ xs: "10px" }}>
        <Container>
          <Stack
            direction={"column"}
            sx={{ width: "fit-content", margin: "0 auto" }}
            gap={"20px"}
          >
            <Typography variant="h4" textAlign={"center"} mt={"20px"}>
              Расписание на занятия в "Мой парус в Москве, Строгино"
            </Typography>
            <Stack
              direction={"row"}
              sx={{ width: "100%" }}
              justifyContent={"flex-start"}
              gap={"10px"}
            >
              <img
                src={dog}
                alt="dog"
                style={{
                  width: "80px",
                  height: "auto",
                }}
              />
              <Stack direction={"column"} maxWidth={"600px"} gap={"10px"}>
                <Typography
                  variant="subtitle1"
                  sx={{ fontWeight: "700 !important" }}
                  color={"primary.main"}
                >
                  Для бронирования места не забудьте оплатить занятие здесь или
                  в мобильном приложении
                </Typography>
                <Button
                  sx={{ width: { xs: "200px", sm: "300px" } }}
                  variant="contained"
                  onClick={() => setOpen((open) => !open)}
                >
                  Оплатить
                </Button>
              </Stack>
            </Stack>
          </Stack>
        </Container>

        <div id="mf_schedule_widget_cont_wdy"></div>
      </Layout>
      <Dialog
        PaperProps={{
          sx: {
            borderRadius: "15px",
            overflow: "hidden",
            width: { xs: "100vw", lg: "70vw" },
            maxWidth: { xs: "100vw", lg: "70vw" },
            overflowY: "auto",
          },
        }}
        open={open}
        onClose={() => setOpen(false)}
      >
        <Button
          sx={{
            position: "absolute",
            top: "5px",
            right: 0,
          }}
          onClick={() => setOpen(false)}
        >
          <CloseIcon
            width={30}
            height={30}
            stroke={"#FFFFFF"}
            style={{
              padding: "5px",
              borderRadius: "100%",
              backgroundColor: mainTheme.palette.primary.main,
            }}
          />
        </Button>
        <Box>
          <div id="mf_shop_widget_cont_7ip"></div>
        </Box>
      </Dialog>
    </>
  );
};

export default MoscowSchedulePage;
export const Head = () => (
  <SEO>
    <script
      type="text/javascript"
      async="async"
      id="mobifitness_personal_widget_script_wdy"
      src="//mobifitness.ru/personal-widget/js/code.js"
      data-div="mf_schedule_widget_cont_wdy"
      data-test="0"
      data-debug="0"
      data-domain="mobifitness.ru"
      data-code="916414"
      data-version="v6"
      data-type="schedule"
      data-language=""
      data-club="6177"
    ></script>
  </SEO>
);
