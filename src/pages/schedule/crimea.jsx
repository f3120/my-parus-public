import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout/Layout";
import { SEO } from "../../components/SEO/SEO";
import { Script } from "gatsby";
import { Typography } from "@mui/material";

const mriaSchedulePage = () => {
  return (
    <>
      <Layout sectionGap={{ xs: "10px" }}>
        <Typography variant="h4" textAlign={"center"} mt={"20px"}>
          Расписание на занятия в "Мой парус в Крыму, Mriya Resort & SPA"
        </Typography>
        <div id="mf_schedule_widget_cont_x53"></div>
      </Layout>
    </>
  );
};

export default mriaSchedulePage;
export const Head = () => (
  <SEO>
    <script
      type="text/javascript"
      async="async"
      id="mobifitness_personal_widget_script_x53"
      src="//mobifitness.ru/personal-widget/js/code.js"
      data-div="mf_schedule_widget_cont_x53"
      data-test="0"
      data-debug="0"
      data-domain="mobifitness.ru"
      data-code="916414"
      data-version="v6"
      data-type="schedule"
      data-language=""
      data-club="7313"
    ></script>
  </SEO>
);
