import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout/Layout";
import { SEO } from "../../components/SEO/SEO";
import { Script } from "gatsby";
import { Typography } from "@mui/material";

const SochiSchedulePage = () => {
  return (
    <>
      <Layout sectionGap={{ xs: "10px" }}>
        <Typography variant="h4" textAlign={"center"} mt={"20px"}>
        Расписание на занятия в "Сочи"
        </Typography>
        {/* <div id="mf_schedule_widget_cont_yj5"></div> */}
        <Typography variant="h6" textAlign={"center"} mt={"100px"}>
          Расписание скоро будет доступно...
        </Typography>
      </Layout>
    </>
  );
};

export default SochiSchedulePage;
export const Head = () => (
  <SEO>
    <script
      type="text/javascript"
      async="async"
      id="mobifitness_personal_widget_script_yj5"
      src="//mobifitness.ru/personal-widget/js/code.js"
      data-div="mf_schedule_widget_cont_yj5"
      data-test="0"
      data-debug="0"
      data-domain="mobifitness.ru"
      data-code="916414"
      data-version="v6"
      data-type="schedule"
      data-language=""
      data-club="6177"
    ></script>
  </SEO>
);
