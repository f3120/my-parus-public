import React from "react";
import Layout from "../components/Layout/Layout";
import ClubServicesSection from "../components/ClubServicesSection/ClubServicesSection";
import ClubMainSection from "../components/ClubMainSection/ClubMainSection";
import InstructorsSection from "../components/InstructorsSection/InstructorsSection";
import ClubMapSection from "../components/ClubMapSection/ClubMapSection";
import { SEO } from "../components/SEO/SEO";

export default function SochiPage() {
  const clubId = "sochi";
  return (
    <Layout>
      <ClubMainSection clubId={clubId} />
      {/* <ClubServicesSection clubId={clubId} /> */}
      {/* <ClubNewsSection clubId={clubId} /> */}
      <InstructorsSection clubId={clubId} />
      <ClubMapSection clubId={clubId} />
    </Layout>
  );
}
export const Head = () => <SEO />;
