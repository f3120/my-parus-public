import React from "react";
import Layout from "../components/Layout/Layout";
import ServicesSection from "../components/ServicesSection/ServicesSection";
import MapSelectorSection from "../components/MapSelectorSection/MapSelectorSection";
import MainSection from "../components/MainSection/MainSection";
import NewsSection from "../components/NewsSection/NewsSection";
import ReviewSection from "../components/ReviewSection/ReviewSection";
import { SEO } from "../components/SEO/SEO";
import DevelopeCardModal from "../components/DevelopeCardModal/DevelopeCardModal";
import ScheduleModal from "../components/ScheduleModal/ScheduleModal";
import FeedBackModal from "../components/FeedBackModal/FeedBackModal";

const IndexPage = () => {
  const servicesBlockRef = React.useRef();
  return (
    <Layout>
      <MainSection servicesBlockRef={servicesBlockRef} />
      <MapSelectorSection />
      <ServicesSection servicesBlockRef={servicesBlockRef} />
      <NewsSection />
      <ReviewSection />
    </Layout>
  );
};

export default IndexPage;
export const Head = () => <SEO />;
