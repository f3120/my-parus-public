import React from "react";
import Layout from "../components/Layout/Layout";
import GalleryMainSection from "../components/GallerySection/GalleryMainSection";
import { SEO } from "../components/SEO/SEO";

export default function GallaryPage() {
  return (
    <Layout>
      <GalleryMainSection />
    </Layout>
  );
}
export const Head = () => <SEO />;
