import { gql, useQuery } from "@apollo/client";
import React, { useContext } from "react";
import Layout from "../components/Layout/Layout";
import { SEO } from "../components/SEO/SEO";
import EventFeedbackModal from "../components/Event/EventFeedbackModal/EventFeedbackModal";
import EventMainSection from "../components/Event/EventMainSection/EventMainSection";
import EventWinners from "../components/Event/EventWinners/EventWinners";
import EventGallery from "../components/Event/EventGallery/EventGallery";
import EventPartners from "../components/Event/EventPartners/EventPartners";
import Loader from "../components/Loader/Loader";
import { Button } from "@mui/material";
import { ModalContext } from "../context/ModalContext";
import { citiesData } from "../config/data/citiesData";

export const APOLLO_QUERY = gql`
  query OneEvent($id: ID!) {
    post(id: $id) {
      id
      eventsfields {
        eventage
        eventend
        eventstart
        eventtittle
        eventdescriptions
        eventphoto {
          sourceUrl
        }
        eventgallery {
          id
          sourceUrl
        }
        eventpresentation {
          sourceUrl
        }
        eventreglament {
          sourceUrl
        }
        fieldGroupName
        statusevent
        victoryuser {
          ... on Post {
            id
            victoryusereventfield {
              descriptionvictorydate
              eventresultuser
              fieldGroupName
              victoryusername
              photovictoryuser {
                sourceUrl
              }
            }
          }
        }
        eventspartners {
          ... on Post {
            id
            partnersfields {
              link
              photo {
                sourceUrl
              }
            }
          }
        }
      }
      clubfield {
        clubname
      }
    }
  }
`;

export default function EventPage({ location }) {
  const { setModalActive } = useContext(ModalContext);
  const id = new URLSearchParams(location.search).get("ID");
  const { loading, error, data } = useQuery(APOLLO_QUERY, {
    variables: { id: id || "" },
  });

  const post = data?.post?.eventsfields || {};

  const {
    eventtittle,
    eventdescriptions,
    eventage,
    eventstart,
    eventend,
    statusevent,
    victoryuser,
    eventspartners,
    eventphoto,
    eventgallery,
    eventpresentation,
    eventreglament,
  } = post || {};

  if (loading) {
    return <Loader />;
  }

  return (
    <Layout>
      <EventMainSection
        postData={post}
        clubName={citiesData.find(
          (city) => city.id === data?.post?.clubfield?.clubname
        )}
      />
      <EventWinners eventWinners={victoryuser || []} />
      <EventGallery eventGallery={eventgallery || []} />
      <EventPartners eventsPartners={eventspartners || []} />
      <EventFeedbackModal eventName={eventtittle} />
    </Layout>
  );
}

export const Head = () => <SEO />;
