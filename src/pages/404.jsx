import React from "react";
import Layout from "../components/Layout/Layout";
import { Stack, Typography } from "@mui/material";
import { SEO } from "../components/SEO/SEO";

export default function PageNotFound() {
  return (
    <Layout>
      <Typography variant="h4" textAlign={"center"} mt={"70px"}>
        Упс...
        <br />
        Такой страницы не существует.
        <br />
        Как вы сюда попали?
      </Typography>
    </Layout>
  );
}

export const Head = () => <SEO></SEO>;
