import React, { useContext, useEffect } from "react";
import Layout from "../../components/Layout/Layout";
import { SEO } from "../../components/SEO/SEO";
import ShopMainBanner from "../../components/Shop/ShopMainBanner/ShopMainBanner";
import ShopFilterSortBlock from "../../components/Shop/ShopFilterSortBlock/ShopFilterSortBlock";
import ShopItemList from "../../components/Shop/ShopItemList/ShopItemList";
import { gql, useQuery } from "@apollo/client";
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import ShopBreadcrumbs from "../../components/Shop/ShopBreadcrumbs/ShopBreadcrumbs";
import client from "../../gatsby-plugin-apollo/client";
import ImagesBlock from "../../components/Product/ImagesBlock/ImagesBlock";
import ProductDataBlock from "../../components/Product/ProductDataBlock/ProductDataBlock";
import SizeSelector from "../../components/Product/SizeSelector/SizeSelector";
import { ReactComponent as ShoppingCart } from "../../media/icons/shopping-cart.svg";
import AdditionalProductsSlider from "../../components/Product/AdditionalProductsSlider/AdditionalProductsSlider";
import { navigate } from "gatsby";
import AddToCartModal from "../../components/Product/AddToCartModal/AddToCartModal";
import { ModalContext } from "../../context/ModalContext";
import { CartContext } from "../../context/CartContext";

export const APOLLO_QUERY = gql`
  query ($id: ID!) {
    product(id: $id) {
      type
      id
      name
      onSale
      description
      productTags {
        nodes {
          name
        }
      }
      galleryImages {
        nodes {
          sourceUrl
        }
      }
      ... on SimpleProduct {
        description
        price(format: RAW)
        productId
        regularPrice(format: RAW)
        stockStatus
        stockQuantity
        weight
        width
        height
        length
      }
      ... on VariableProduct {
        price(format: RAW)
        productId
        regularPrice(format: RAW)
        stockStatus
        stockQuantity
        variations {
          nodes {
            name
            id
            databaseId
            stockStatus
            stockQuantity
          }
        }
        id
        weight
        width
        height
        length
      }
      related {
        edges {
          node {
            id
            name
            image {
              sourceUrl
            }
            ... on SimpleProduct {
              enclosure
              price(format: RAW)
              regularPrice(format: RAW)
              salePrice
              id
              productId
              onSale
              shippingRequired
              weight
              width
              height
              length
            }
            ... on VariableProduct {
              price(format: RAW)
              onSale
              regularPrice(format: RAW)
              productId
              weight
              width
              height
              length
            }
          }
        }
      }
      attributes {
        nodes {
          attributeId
          id
          label
          name
          options
          position
          scope
          variation
          visible
        }
      }
      image {
        sourceUrl
      }
      productCategories {
        nodes {
          slug
        }
      }
    }
  }
`;

export default function ProductPage({ location }) {
  const id = new URLSearchParams(location.search).get("ID");

  const { setModalActive } = React.useContext(ModalContext);

  const locationState = location.state;

  const { loading, error, data } = useQuery(APOLLO_QUERY, {
    variables: { id: id || "" },
  });

  const [selectedSize, setSelectedSize] = React.useState();

  const product = data?.product;

  const { getCartItem, addCartItem } = useContext(CartContext);

  useEffect(() => {
    setSelectedSize(undefined);
  }, [location]);

  const {
    name,
    onSale,
    price,
    regularPrice,
    description,
    galleryImages,
    productTags,
    type,
    //type = "SIMPLE" || "VARIABLE"
    related,
    variations,
    productCategories,
    stockStatus,
    // stockStatus = "IN_STOCK" || "OUT_OF_STOCK"
  } = product || {};

  const cartItem = getCartItem(id, type, selectedSize?.databaseId);

  console.log({ cartItem, id, type, variation: selectedSize?.databaseId });
  return (
    <Layout displayShopHeader>
      <Container>
        <ShopBreadcrumbs
          productName={name || locationState?.name}
          productCategory={
            productCategories?.nodes[0]?.slug || locationState?.productCategory
          }
        />
        {loading ? (
          <CircularProgress
            size={70}
            sx={{ margin: "100px auto", display: "block" }}
          />
        ) : (
          <>
            <Stack
              width={"100%"}
              direction={{ xs: "column", md: "row" }}
              justifyContent={"space-between"}
              alignItems={{ xs: "center", md: "normal" }}
              gap={"25px"}
            >
              <Box width={{ xs: "100%", md: "40%" }}>
                <ImagesBlock galleryImages={galleryImages?.nodes} />
              </Box>

              <Stack
                width={{ xs: "100%", md: "60%" }}
                direction={"column"}
                justifyContent={"space-between"}
                alignItems={"center"}
                gap={"15px"}
              >
                <ProductDataBlock
                  productData={{
                    name,
                    onSale,
                    price,
                    regularPrice,
                    description,
                    productTags,
                    stockStatus,
                  }}
                />
                <Stack
                  direction={"column"}
                  justifyContent={"flex-end"}
                  alignItems={"flex-start"}
                  width={"100%"}
                  gap={"10px"}
                >
                  {type === "VARIABLE" && (
                    <SizeSelector
                      variations={variations}
                      selectedSize={selectedSize}
                      setSelectedSize={setSelectedSize}
                    />
                  )}

                  <Button
                    sx={{
                      width: { xs: "100%", sm: "auto" },
                      padding: "10px 45px",
                      ...((type === "VARIABLE" && cartItem?.quantity) ||
                      (type === "SIMPLE" && cartItem)
                        ? {
                            "&:hover": {
                              backgroundColor: "secondary.main",
                              boxShadow: "0px 2px 10px 0px rgba(0,0,0,0.2)",
                            },
                            bgcolor: "secondary.main",
                            color: "primary.main",
                          }
                        : {}),
                    }}
                    disabled={
                      stockStatus !== "IN_STOCK" ||
                      (type === "VARIABLE" && !selectedSize)
                    }
                    variant="contained"
                    onClick={() => {
                      if (
                        (type === "VARIABLE" && cartItem?.quantity) ||
                        (type === "SIMPLE" && cartItem)
                      ) {
                        navigate("/cart");
                      } else {
                        addCartItem({ id, selectedSize, quantity: 1 });
                        setModalActive("addToCart");
                      }
                    }}
                  >
                    {(type === "VARIABLE" && cartItem?.quantity) ||
                    (type === "SIMPLE" && cartItem) ? (
                      <>Перейти в корзину</>
                    ) : (
                      <>
                        <ShoppingCart
                          width={"18px"}
                          height={"18px"}
                          stroke={
                            stockStatus !== "IN_STOCK" ||
                            (type === "VARIABLE" && !selectedSize)
                              ? "grey"
                              : "#FFFFFF"
                          }
                          cursor={"pointer"}
                          style={{ marginRight: "5px", transition: "0.3s" }}
                        />
                        Добавить в корзину
                      </>
                    )}
                  </Button>
                </Stack>
              </Stack>
            </Stack>
          </>
        )}
      </Container>
      {related?.edges && <AdditionalProductsSlider related={related?.edges} />}
      <AddToCartModal productName={name} />
    </Layout>
  );
}

// export const query = client.query({
//   query: APOLLO_QUERY,
// });

export const Head = () => <SEO />;
