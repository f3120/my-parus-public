import React from "react";
import ShopPage from "..";
import { SEO } from "../../../components/SEO/SEO";

export default function ClothesPage() {
  return <ShopPage category="clothes" />;
}
export const Head = () => <SEO />;
