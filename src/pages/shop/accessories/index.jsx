import React from "react";
import ShopPage from "..";
import { SEO } from "../../../components/SEO/SEO";

export default function AccessoriesPage() {
  return <ShopPage category="accessories" />;
}
export const Head = () => <SEO />;
