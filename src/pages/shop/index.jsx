import React from "react";
import Layout from "../../components/Layout/Layout";
import { SEO } from "../../components/SEO/SEO";
import ShopMainBanner from "../../components/Shop/ShopMainBanner/ShopMainBanner";
import ShopFilterSortBlock from "../../components/Shop/ShopFilterSortBlock/ShopFilterSortBlock";
import ShopItemList from "../../components/Shop/ShopItemList/ShopItemList";
import { gql, useQuery } from "@apollo/client";
import { CircularProgress } from "@mui/material";
import { correctingPrice } from "../../components/Shop/ShopItemList/ShopItem/ShopItem";

const APOLLO_QUERY = gql`
  query Products($category: String!) {
    products(where: { category: $category }, first: 1000) {
      found
      nodes {
        databaseId
        image {
          sourceUrl
        }
        name
        id
        type
        ... on SimpleProduct {
          enclosure
          price(format: RAW)
          regularPrice(format: RAW)
          salePrice
          id
          productId
          productCategories {
            nodes {
              slug
            }
          }
          onSale
        }
        ... on VariableProduct {
          price(format: RAW)
          onSale
          regularPrice(format: RAW)
          productCategories {
            nodes {
              slug
            }
          }
          productId
        }
      }
    }
  }
`;

export default function ShopPage({ category }) {
  const [products, setProducts] = React.useState([]);
  const [sortedProducts, setSortedProducts] = React.useState([]);

  const [sortParam, setSortParam] = React.useState("default");

  const { loading, error, data } = useQuery(APOLLO_QUERY, {
    variables: { category: category || "" },
  });

  React.useEffect(() => {
    data && setProducts(data?.products?.nodes);
  }, [data]);

  React.useEffect(() => {
    if (sortParam === "default") {
      setSortedProducts(products);
    } else if (sortParam === "asc" && products) {
      setSortedProducts(
        [...products].sort(
          (a, b) => correctingPrice(a.price) - correctingPrice(b.price)
        )
      );
    } else if (sortParam === "desc" && products) {
      setSortedProducts(
        [...products].sort(
          (a, b) => correctingPrice(b.price) - correctingPrice(a.price)
        )
      );
    }
  }, [sortParam, products]);

  return (
    <Layout displayShopHeader>
      <ShopMainBanner />
      <ShopFilterSortBlock sortParam={sortParam} setSortParam={setSortParam} />
      {loading ? (
        <CircularProgress size={70} sx={{ margin: "100px auto" }} />
      ) : (
        <ShopItemList itemList={sortedProducts} />
      )}
    </Layout>
  );
}
export const Head = () => <SEO />;
