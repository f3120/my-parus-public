import React, { useContext, useEffect, useState } from "react";
import Layout from "../../components/Layout/Layout";
import { SEO } from "../../components/SEO/SEO";
import CartItemsSection from "../../components/Cart/CartItemsSection/CartItemsSection";
import CartSummarySection from "../../components/Cart/CartSummarySection/CartSummarySection";
import { Container, Stack, Typography } from "@mui/material";
import { mainTheme } from "../../config/MUI/mainTheme";
import { CartContext } from "../../context/CartContext";

export default function CartPage() {
  const { cartItems, updateCartItem, deleteCartItem } = useContext(CartContext);

  return (
    <Layout displayShopHeader hideCart>
      <Container>
        <Typography variant="h4">Корзина</Typography>
      </Container>

      <Stack
        margin={"0 auto"}
        direction={{ xs: "column", lg: "row" }}
        justifyContent={"space-between"}
        alignItems={{ xs: "center", lg: "flex-start" }}
        width={"100%"}
        gap={{ xs: "20px", md: "30px" }}
        padding={{ xs: "0", sm: "0 20px", md: "0 40px" }}
        maxWidth={{
          lg: mainTheme.breakpoints.values.lg,
          xl: mainTheme.breakpoints.values.xl,
        }}
      >
        <CartItemsSection
          cartItems={cartItems}
          updateCartItem={updateCartItem}
          deleteCartItem={deleteCartItem}
        />
        <CartSummarySection cartItems={cartItems} />
      </Stack>
    </Layout>
  );
}
export const Head = () => <SEO />;
