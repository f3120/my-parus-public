import React, { useContext, useEffect, useState } from "react";
import Layout from "../../components/Layout/Layout";
import { SEO } from "../../components/SEO/SEO";
import { Container, Stack, Typography } from "@mui/material";
import OrderDataSection from "../../components/Cart/Order/OrderDataSection/OrderDataSection";
import OrderContactsSection from "../../components/Cart/Order/OrderContactsSection/OrderContactsSection";
import { mainTheme } from "../../config/MUI/mainTheme";
import { CartContext } from "../../context/CartContext";
import OrderCdekModal from "../../components/Cart/Order/OrderCdekModal/OrderCdekModal";
import { ModalContext } from "../../context/ModalContext";

export default function CartOrderPage() {
  const { cartItems, updateCartItem, deleteCartItem } = useContext(CartContext);
  const [cdekDekiveryData, setCdekDekiveryData] = useState();

  return (
    <Layout displayShopHeader hideCart>
      <Container>
        <Typography variant="h4">Оформление</Typography>
      </Container>
      <Stack
        margin={"0 auto"}
        direction={{ xs: "column", lg: "row" }}
        justifyContent={"space-between"}
        alignItems={{ xs: "center", lg: "flex-start" }}
        width={"100%"}
        gap={{ xs: "20px", md: "30px" }}
        padding={{ xs: "0", sm: "0 20px", md: "0 40px" }}
        maxWidth={{
          lg: mainTheme.breakpoints.values.lg,
          xl: mainTheme.breakpoints.values.xl,
        }}
      >
        <OrderDataSection
          cartItems={cartItems}
          cdekDekiveryData={cdekDekiveryData}
          updateCartItem={updateCartItem}
          deleteCartItem={deleteCartItem}
        />
        <OrderContactsSection
          cartItems={cartItems}
          cdekDekiveryData={cdekDekiveryData}
          setCdekDekiveryData={setCdekDekiveryData}
        />
      </Stack>
      {/* <OrderCdekModal /> */}
    </Layout>
  );
}
export const Head = () => <SEO></SEO>;
