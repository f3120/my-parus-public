import { Container } from "@mui/material";
import React from "react";
import { SEO } from "../components/SEO/SEO";
import Layout from "../components/Layout/Layout";

import GalleryPageTitle from "../components/GalleryPageSection/GalleryPageTitle/GalleryPageTitle";
import GalleryPageList from "../components/GalleryPageSection/GalleryPageList/GalleryPageList";
import { gql, useQuery } from "@apollo/client";
import NewsMainContent from "../components/NewsMore/NewsMainContent/NewsMainContent";
import NewsContent from "../components/NewsMore/NewsContent/NewsContent";

export const APOLLO_QUERY = gql`
  query OnePost($id: ID!) {
    post(id: $id) {
      id
      title
      content
      postfield {
        fieldGroupName
        shortdescriptionpost
        postauthor
        postdata
        postimage {
          sourceUrl
        }
      }
    }
  }
`;

export default function NewsMore() {
  const location = typeof window !== "undefined" && window.location;
  const id = new URLSearchParams(location.search).get("ID");

  const { loading, error, data } = useQuery(APOLLO_QUERY, {
    variables: { id: id || "" },
  });

  return (
    <Layout>
      <Container>
        <NewsMainContent
          author={data?.post?.postfield?.postauthor}
          date={data?.post?.postfield?.postdata}
          title={data?.post?.title}
          background={data?.post?.postfield?.postimage?.sourceUrl}
        />
        <NewsContent content={data?.post?.content} />
      </Container>
    </Layout>
  );
}
export const Head = () => <SEO />;
